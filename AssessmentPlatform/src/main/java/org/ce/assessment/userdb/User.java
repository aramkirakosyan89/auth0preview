package org.ce.assessment.userdb;

import org.ce.assessment.platform.Communities;
import org.ce.assessment.platform.Community;
import org.mindrot.jbcrypt.BCrypt;

import java.util.List;

public class User {
	private String id;
	private String email;	
	private String name;
	private String lastName = "";
	private String password;
	private String jobTitle;
	private String organizationId;
    private String organizationName;
	private String phone;

	private boolean isAdmin;
    private boolean isOrganizationAdmin = false;
	private boolean unKnown = true;

    public User() {
    }

	public User(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public User setId(String id) {
        this.id = id;
        return this;
    }
	
	public String getEmail() {
		return email;
	}

	public User setEmail(String email) {
		this.email = email.toLowerCase();
		return this;
	}

	public String getName() {
		return name;
	}

	public User setName(String name) {
		this.name = name;
		return this;
	}

    public String getLastName() {
        return lastName;
    }

    public User setLastName(String lastName) {
	    this.lastName = (lastName == null)?"":lastName;
	    return this;
    }

    public String getFullName() {
	    return name + ((lastName.isEmpty())?"":" "+lastName);
    }


	public User setIsAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
		return this;
	}
	
	public boolean isAdmin() {
		return isAdmin;
	}

	public boolean isCommunityAdmin() {
    	// TODO: weird logic
    	return !isAdmin && (new Communities().getByAdminOrganizationId(organizationId) != null);
	}

    public User setIsOrganizationAdmin(boolean isOrganizationAdmin) {
        this.isOrganizationAdmin = isOrganizationAdmin;
        return this;
    }

    public boolean isOrganizationAdmin() {
	    return isOrganizationAdmin;
    }

    public boolean canAdminOrganization() {
		return isAdmin || isOrganizationAdmin;
	}


	private boolean isCommunityAdminForOrganization(Organization organization) {
		final List<Community> communities = new Communities().getWithOrganization(organization.id);
		if (communities == null) {
			throw new RuntimeException("Internal Server Error");
		}
		for (Community c : communities) {
			if ((c.getAdminOrganization().id).equals(organizationId)) {
				return true;
			}
		}
		return false;
	}


	public boolean canAdminOrganization(Organization organization) {
        return isAdmin
				|| (isOrganizationAdmin && this.organizationId.equals(organization.id))
				|| isCommunityAdminForOrganization(organization);
    }

    public boolean canSeeOrganization(Organization organization) {
		return  isAdmin
				|| organizationId.equals(organization.id)
				|| isCommunityAdminForOrganization(organization);  // TODO: this is not really safe if one org is in multiple orgs
	}


    public String getPassword() {
		return password;
	}
	
	/**
	 * To be used when loading user from DB
	 */
	public User setEncryptedPassword(String password) {
		this.password = password;
		return this;
	}

	/**
	 * Will encrypt password and store it
	 * @param password - un-encrypted password from the registration form
	 */
	public User setPassword(String password) {
		this.password = BCrypt.hashpw(password, BCrypt.gensalt());
		return this;
	}
	
	/**
	 * @param candidate - un-encrypted password candidate
	 */
	public boolean checkPassword(String candidate) {
		return BCrypt.checkpw(candidate, password);
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public User setJobTitle(String jobTitle) {
		this.jobTitle = nonull(jobTitle);
		return this;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public User setOrganizationId(String organizationId) {
        this.organizationId = nonull(organizationId);
        return this;
	}

	public String getOrganizationName() {
	    return organizationName;
    }

    public User setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
        return this;
    }

    public String getPhone() {
		return phone;
	}
	
	public User setPhone(String phone) {
		this.phone = nonull(phone);
		return this;
	}

	public boolean isUnKnown() {
		return unKnown;
	}

	public void setUnKnown(boolean unKnown) {
		this.unKnown = unKnown;
	}

	private String nonull(String string) {
		return ("null".equals(string)?"":string);
	}

}
