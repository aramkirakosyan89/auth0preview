package org.ce.assessment.userdb;

import org.ce.assessment.platform.DbQuery;
import org.ce.assessment.platform.DbTable;
import org.ce.assessment.platform.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.SQLException;

public class Admins extends DbTable {

    private final static Logger log = LoggerFactory.getLogger(Admins.class);

    private final static String NAME = "admins";

    public Admins(Platform platform) {
        super(platform);
    }

    public boolean addAdmin(User user) {
        String sql = "insert into admins values (" + user.getId() + ");";
        try (DbQuery query = query(sql)) {
            query.execute();
        } catch (SQLException | NamingException e) {
            log.error("Could not add admin " + user.getEmail() + ". " + e.toString());
            return false;
        }
        log.info("Added admin " + user.getEmail());
        return true;
    }
}
