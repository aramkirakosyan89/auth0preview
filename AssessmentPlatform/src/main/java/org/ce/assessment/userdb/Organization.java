package org.ce.assessment.userdb;

import org.ce.assessment.platform.Assessment;
import org.ce.assessment.platform.Assessments;
import org.ce.assessment.platform.Community;
import org.ce.assessment.platform.CommunityAssessment;
import org.ce.assessment.platform.invites.Invite;
import org.ce.assessment.platform.invites.Invites;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Organization {

    private final static Logger log = LoggerFactory.getLogger(Organization.class);

    public final String id;
    private String name;
    private String profileToken;


    public Organization(String id, String name, String profileToken) {
        this.id = id;
        this.name = name;
        if ("null".equals(profileToken)) {
            this.profileToken = "";
        } else {
            this.profileToken = profileToken;
        }
    }

    public static final Organization NONE = new Organization("null", "none", "none");

    public Organization(Organization o) {
        this.id = o.id;
        this.name = o.name;
        this.profileToken = o.profileToken;
    }

    @Override
    public String toString() {
        return "organization \"" + name + "\" (id= " + id + ")";
    }

    public String getName() {
        return name;
    }

    public Organization setName(String name) {
        this.name = name;
        return this;
    }

    public OrganizationProfile getProfile() {
        if (profileToken == null || profileToken.isEmpty()) {
            Organizations organizations = new Organizations();
            profileToken = organizations.createProfileToken(name);
            if (profileToken == null) {
                return null;
            }
            organizations.update(this);
        }
        return new OrganizationProfile(this, profileToken);
    }

    public List<User> getUsers() {
        return new Users().getByOrganizationId(id);
    }

    public List<Invite> getInvites() {
        return  new Invites().getUserInvitesForOrganization(this);
    }

    public Assessment createAssessment(CommunityAssessment communityAssessment) {
        return new Assessments().create(this, communityAssessment);
    }

    public boolean removeCommunityAssessments(Community community) {
        boolean success = true;
        for (Assessment assessment : getAssessments()) {
            if (community.id.equals(assessment.getCommunityAssessment().getCommunity().id)) {
                if (!new Assessments().delete(assessment.getId())) {
                    log.error("Could not delete community(" + community.id + ") assessment for organization (" + id);
                    success = false;
                }
            }
        }
        return success;
    }

    public List<Assessment> getAssessments() {
        return new Assessments().getForOrganization(id);
    }

    public String getProfileToken() {
        return profileToken;
    }
}
