package org.ce.assessment.userdb;

import org.ce.assessment.platform.DataException;
import org.ce.assessment.platform.DbQuery;
import org.ce.assessment.platform.DbTable;
import org.ce.assessment.platform.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Users extends DbTable {
    
    private final static Logger log = LoggerFactory.getLogger(Users.class);
    
    protected final static String USERS = "users";
    protected final static  String USERS_VIEW = "users_view";
    private final static String SQL =  /* update */
            "CREATE TABLE users (" +
            "   email character varying," +
            "   password character varying," +
            "   clientid integer," +
            "   jobtitle character varying," +
            "   phone character varying," +
            "   id serial NOT NULL," +
            "   name character varying," +
            "   lastname character varying," +
            "   CONSTRAINT id PRIMARY KEY (id)," +
            "   CONSTRAINT \"users_clientId_fkey\" FOREIGN KEY (clientid)" +
            "   REFERENCES clients (id) MATCH SIMPLE" +
            "   ON UPDATE NO ACTION ON DELETE NO ACTION," +
            "   CONSTRAINT users_email_key UNIQUE (email)" +
            ") WITH (OIDS=FALSE );";
    
    private final static String VIEW_SQL =
            "CREATE OR REPLACE VIEW " + USERS_VIEW + " AS \n" +
            " SELECT u.id,\n" +
            "    u.name,\n" +
            "    u.organization_id,\n" +
            "    u.date_created,\n" +
            "    u.date_joined,\n" +
            "    u.job_title,\n" +
            "    u.password,\n" +
            "    u.email,\n" +
            "    u.last_name,\n" +
            "    u.phone,\n" +
            "        CASE a.admin_user_id\n" +
            "            WHEN NULL::integer THEN false\n" +
            "            ELSE true\n" +
            "        END AS is_admin\n" +
            "   FROM users u\n" +
            "     LEFT JOIN admins a ON u.id = a.admin_user_id;" +
            "ALTER TABLE public.users_view OWNER TO webapp;";

    public Users() {
        super(Platform.instance());
    }

    public static boolean createDbTable(String dataSourceName) {
        return createDbTable(USERS, SQL, dataSourceName, log) && createDbTable(USERS_VIEW, VIEW_SQL, dataSourceName, log);
    }

    public Users(Platform platform) {
        super(platform);
    }

    public User createInDb(User user) {
        String sql = "insert into " + USERS + " (email, organization_id) values ('"
                + user.getEmail() + "', " + user.getOrganizationId()+ ") returning id;";

        try (DbQuery insertQuery = query(sql);
             ResultSet rs = insertQuery.getResults())
        {
            if (rs.next()) {
                user.setId(rs.getString("id"));
            } else {
                log.error("did not get id of the new user " + user.getEmail());
                return null;
            }
        }
        catch (SQLException | NamingException e) {
            log.error("Could not create new user with email " + user.getEmail());
            e.printStackTrace();
            return null;
        }

        if (!update(user)) {
            log.error("Could not create new user with email " + user.getEmail());
            delete(user);
            return null;
        }

        return user;
    }


    public User createNew(String email, String organizationId) {
        email = email.toLowerCase();
        User user = getByEmail(email);
        if (user != null) {
            log.error("User with email " + email + " already exists");
            return null;
        }

        String sql = "insert into " + USERS + " (email, organization_id) values ('" + email + "', " + organizationId + ");";

        try (DbQuery insertQuery = query(sql)) {
            insertQuery.execute();
            log.info("Created new user " + email);
        }
        catch (SQLException | NamingException e) {
            log.error("Could not create new user with email " + email);
            e.printStackTrace();
            return null;
        }
        return getByEmail(email);
    }

    public User getByEmail(String email) { // TODO: throw exception
        email = email.toLowerCase();

        try (DbQuery selectQuery = query("select * from " + USERS_VIEW + " where email = '" + email + "';")) {
            ResultSet res = selectQuery.getResults();
            if (res.next()) {
                return fromDbRow(res);
            }
        }
        catch (SQLException | NamingException e) {
            log.error("Could not get user by email (" + email + "). " + e.toString());
            e.printStackTrace();
        }
        return null;
    }

    public User getById(String id) {
        try (DbQuery selectQuery = query("select * from " + USERS_VIEW + " where id = " + id + ";")) {
            ResultSet res = selectQuery.getResults();
            if (res.next()) {
                return fromDbRow(res);
            }
        }
        catch (SQLException | NamingException e) {
            log.error("Could not get user by id (" + id + "). " + e.toString());
            e.printStackTrace();
        }
        return null;
    }
    
    public List<User> getByOrganizationId(String organizationId) {
        String sql = "select * from " + USERS_VIEW + " where organization_id = " + organizationId + ";";
        try {
            return getUsersWithQuery(sql);
        } catch (DataException e) {
            log.error("Could not get users by organization id (" + organizationId + "). " + e.toString());
            e.printStackTrace();
        }
        return null;
    }

    protected List<User> getUsersWithQuery(String sql) throws DataException {
        List<User> users = new ArrayList<>();
        try (DbQuery selectQuery = query(sql);
             ResultSet res = selectQuery.getResults())
        {
            while (res.next()) {
                User user = fromDbRow(res);
                users.add(user);
            }
        }
        catch (SQLException | NamingException e) {
            throw new DataException(e);
        }
        return users;        
    }
    

    private User fromDbRow(ResultSet dbRow) throws SQLException {
        User user = new User(dbRow.getString("id"));
        user.setEncryptedPassword(dbRow.getString("password"))
                .setEmail(dbRow.getString("email"))
                .setName(dbRow.getString("name"))
                .setLastName(dbRow.getString("last_name"))
                .setOrganizationId(dbRow.getString("organization_id"))
                .setJobTitle(dbRow.getString("job_title"))
                .setPhone(dbRow.getString("phone"))
                .setIsAdmin(dbRow.getBoolean("is_admin"))
                .setIsOrganizationAdmin(dbRow.getBoolean("is_organization_admin"))
        ;
        return user;
    }

    public boolean update(User user) {
        String sql = "update " + USERS + " set" +
                "   password='" + user.getPassword() + "'" +
                "   , name='" + user.getName() + "'" +
                "   , last_name='" + user.getLastName() + "'" +
                "   , job_title='" + user.getJobTitle() + "'" +
                "   , email='" + user.getEmail() + "'" +
                "   , phone='" + user.getPhone() + "'" +
                " where id = " + user.getId() + ";";

        try (DbQuery updateQuery = query(sql)) {
            updateQuery.execute();
        }
        catch (SQLException | NamingException e) {
            log.error("Could not update user (" + user.getId() + ", " + user.getEmail() + "). " + e.toString());
            return false;
        }
        log.info("Updated user " + user.getFullName() + " - " + user.getEmail());
        return true;
    }

    public boolean delete(User user) {
        String sql = "delete from users where id="  + user.getId();
        try (DbQuery q = query(sql)) {
            q.execute();
        } catch (SQLException | NamingException e) {
            log.error("Could not delete user " + user.getId());
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
