package org.ce.assessment.userdb;

import org.ce.assessment.limeapi.AddParticipants;
import org.ce.assessment.limeapi.ParticipantData;
import org.ce.assessment.limeapi.Survey;
import org.ce.assessment.platform.DataException;
import org.ce.assessment.platform.DbQuery;
import org.ce.assessment.platform.DbTable;
import org.ce.assessment.platform.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Organizations extends DbTable {

    private final static Logger log = LoggerFactory.getLogger(Organizations.class);

    private final static String NAME = "organizations";

    // TODO: update SQL
    private final static String SQL = "CREATE TABLE " + NAME + " (" +
            "id serial NOT NULL," +
            "name character varying," +
            "token character varying(32)," +
            "CONSTRAINT "+NAME+"_pkey PRIMARY KEY (id)" +
            ")" +
            "WITH ( OIDS=FALSE );";

    public static boolean createDbTable(String dataSourceName) {
        return createDbTable(NAME, SQL, dataSourceName, log);
    }

    public Organizations(Platform platform) {
        super(platform);
    }

    public Organizations() {
        super(Platform.instance());
    }


    protected String getSelectQuery() {
        return getSelectQuery("order by name asc");
    }

    private String getSelectQuery(String args) {
        return "select * from " + NAME + " " + args + ";";
    }

    protected Organization fromDbRow(ResultSet dbRow) throws SQLException {
        return new Organization(dbRow.getString("id"),
                                dbRow.getString("name"),
                                dbRow.getString("profile_token"));
    }

    private List<Organization> getList(String sql) {
        List<Organization> result = new ArrayList<>();

        try (DbQuery query = query(sql)) {
            ResultSet res = query.getResults();
            while (res.next()) {
                Organization organization = fromDbRow(res);
                result.add(organization);
            }
        } catch (SQLException | NamingException e) {
            log.error("Could not load organizations. " + e.toString());
            return null;
        }

        return result;
    }
    public List<Organization> getAll() {
        return getList(getSelectQuery());
    }

    public List<Organization> getLike(String query) {
        if (query.isEmpty()) {
            return new ArrayList<>();
        }
        return getList(getSelectQuery(" where name ilike '%" + query +  "%'"));
    }

    private Organization getBy(String args) {
        try (DbQuery query = query(getSelectQuery("where " + args))) {
            ResultSet res = query.getResults();
            if (res.next()) {
                return fromDbRow(res);
            } else {
                log.error("Could not find an organization with " + args);
            }

        } catch (SQLException | NamingException e) {
            log.error("Could not load an organization with " + args + ". " + e.toString());
            e.printStackTrace();
        }
        return null;
    }

    public Organization getById(String id) {
        return getBy("id=" + id);
    }

    public Organization createNew(String name) {
        String profileToken = createProfileToken(name);
        if (profileToken == null) {
            log.error("Could not create organization (" + name + "). Could not create profile token.");
            return null;
        }

        String sql =
                "insert into " + NAME + " (name, profile_token) " +
                " values (" + quotesAndComma(name) + quotes(profileToken) + ") " +
                " returning id;";
        try (DbQuery insertQuery = query(sql)) {
            ResultSet res = insertQuery.getResults();

            if (res.next()) {
                return getById(res.getString("id"));
            }
        } catch (SQLException | NamingException e) {
            log.error("Could not create new organization");
            e.printStackTrace();
        }
        return null;
    }
//
//
//    private String getSurveyId() {
//        try {
//            InitialContext cxt = new InitialContext();
//            return (String) ((Context) cxt.lookup("java:/comp/env")).lookup("org.ce.assessment.orgProfileSurveyId");
//        } catch (NamingException e) {
//            log.error("Could not find organization profile limesurvey id (should be specified in WEB-INF/web.xml. ");
//            e.printStackTrace();
//            return null;
//        }
//    }

    String createProfileToken(String orgName) {
        String surveyId = Platform.instance().configuration.profileSurveyId;
        if (surveyId == null) {
            log.error("Could not create new ogranization profile token");
            return null;
        }
        Survey survey = new Survey(surveyId);
        String token = new AddParticipants(survey).addParticipant(new ParticipantData().setFirstName(orgName));

        if (token == null) {
            log.error("Could not create new ogranization profile token");
            return null;
        }
        return token;
    }

    public boolean update(Organization organization) {
        String sql = "update " + NAME + " set " +
                " name=" + quotes(organization.getName()) +
                ", date_edited=current_timestamp " +
                ", profile_token=" + quotes(organization.getProfileToken()) +
                " where id = " + organization.id + ";";

        try (DbQuery updateQuery = query(sql)) {
            updateQuery.execute();
        } catch (SQLException | NamingException e) {
            log.error("Could not update organizations (" + organization.id + ", " + organization.getName() + "). " + e.toString());
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public int getCount() {
        String sql = "select count(*) as count from organizations where is_community=false;";
        try (DbQuery query = query(sql);
             ResultSet rs = query.getResults())
        {
            if (rs.next()) {
                return rs.getInt("count");
            } else {
                log.error("Could not get organizations count.");
            }
        } catch (SQLException | NamingException e) {
            log.error("Could not get organizations count." + e.toString());
        }
        return -1;
    }

    public List<Organization> getUnattached() {
        return getAll();
        // TODO
    }

    public boolean delete(Organization organization) {
        return delete(organization.id);
    }

    public boolean delete(String id) {
        try {
            execute("delete from " + NAME + " where id=" + id);
        } catch (DataException e) {
            e.printStackTrace();
            log.error("Could not delete an organization(" + id + "). " + e.toString() );
            return false;
        }
        return true;
    }

}



