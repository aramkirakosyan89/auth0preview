package org.ce.assessment.fashiontool;

import org.ce.assessment.limeapi.*;
import org.ce.assessment.platform.DbQuery;
import org.ce.assessment.platform.DbTable;
import org.ce.assessment.platform.Platform;
import org.ce.assessment.platform.report.Report;
import org.ce.assessment.platform.report.Sector;
import org.ce.assessment.userdb.Organization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;


public class WasteMapping extends Report {

    private final static Logger log = LoggerFactory.getLogger(WasteMapping.class);
    private static final String SURVEY_ID = "643663";

    private final String token;
    private final Organization organization;
    private final WasteMappingTable wasteMappingTable = new WasteMappingTable();

    public WasteMapping(Organization organization, String token) {
        super(new Survey(SURVEY_ID));
        this.organization = organization;
        this.token = token;

        withSector(new Sector("Post-Consumer Waste").withDefinition(f.q("PostConTotal")));
        withSector(new Sector("Post-Consumer Collected Waste").withDefinition(f.q("PostCollectionTotal")));
        withSector(new Sector("Pre-Consumer Waste").withDefinition(f.q("PreConTotal")));
        withSector(new Sector("Post-Industrial Waste").withDefinition(f.q("IndWasteTotal")));

        try {
            loadForToken(token);
        } catch (LimeCallException e) {
            e.printStackTrace();
        }
    }


    public String getSurveyLink(boolean newSurvey) {
        if (token == null) {
            log.error("getting organization ("+organization.id+") profile survey link with NULL token");
            return "javascript: alert('Something went wrong. Cannot get the link to the survey')'";
        }
        return LimeClient.LIME_URL_SSL + "/index.php/survey/index"
                + "/sid/" + SURVEY_ID
                + "/token/" + token
                + ((newSurvey)?"/lang/en/newtest/Y":"");
    }

    public static WasteMapping getForOrganization(Organization organization) {
        String token = new WasteMappingTable().getOrganizationToken(organization);
        if (token != null) {
            return new WasteMapping(organization, token);
        } else {
            log.error("Could not get WasteMapping for organization " + organization.toString());
            return null;
        }

    }


    private static class WasteMappingTable extends DbTable {

        private final String TABLE = "waste_mapping";

        public WasteMappingTable() {
            super(Platform.instance());
        }

        public String getOrganizationToken(Organization organization) {
            String sql = "select token from " + TABLE + " where organization_id=" + organization.id;

            try (DbQuery query = query(sql)) {
                ResultSet res = query.getResults();
                if (res.next()) {
                    return res.getString("token");
                } else {
                    return createNewToken(organization);
                }
            }
            catch (SQLException | NamingException e) {
                log.error("Could not load organizations. " + e.toString());
                return null;
            }
        }

        private String createNewToken(Organization organization) {

            Survey survey = new Survey(SURVEY_ID);
            String token = new AddParticipants(survey).addParticipant(new ParticipantData().setFirstName(organization.getName()));

            if (token == null) {
                log.error("Could not create new organization waste mapping token");
                return null;
            }

            String sql = "insert into " + TABLE + " values (" + organization.id + ", '" + token + "');";
            try (DbQuery query = query(sql)) {
                query.execute();
            } catch (SQLException | NamingException e) {
                e.printStackTrace();
                log.error("Could not create waste mappping token for organization " + organization.toString());
                return null;
            }

            return token;
        }
    }


}
