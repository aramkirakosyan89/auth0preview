package org.ce.assessment.platform.report;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tair on 11/22/2016.
 */
public class TemplateReplacer {

    private Map<String, String> values = new HashMap<>();

    public TemplateReplacer withValue(String key, String value) {
        values.put(key, value);
        return this;
    }

    public String applyTo(String string) {
        for (Map.Entry<String, String> value : values.entrySet()) {
            string = string.replaceAll(value.getKey(), value.getValue());
        }
        return string;
    }

}
