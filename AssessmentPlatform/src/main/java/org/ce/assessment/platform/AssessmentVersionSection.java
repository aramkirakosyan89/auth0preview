package org.ce.assessment.platform;

import org.ce.assessment.platform.report.Report;

public abstract class AssessmentVersionSection {
    private String id;
    private String assessmentVersionId;
    private String name;
    private String kind;
    private String limeSurveyId;

    public String getId() {
        return id;
    }

    public AssessmentVersionSection setId(String id) {
        this.id = id;
        return this;
    }

    public String getAssessmentVersionId() {
        return assessmentVersionId;
    }

    public AssessmentVersionSection setAssessmentVersionId(String assessmentVersionId) {
        this.assessmentVersionId = assessmentVersionId;
        return this;
    }

    public String getName() {
        return name;
    }

    public AssessmentVersionSection setName(String name) {
        this.name = name;
        return this;
    }

    public String getKind() {
        return kind;
    }

    public AssessmentVersionSection setKind(String kind) {
        this.kind = kind;
        return this;
    }

    public String getLimeSurveyId() {
        return limeSurveyId;
    }

    public AssessmentVersionSection setLimeSurveyId(String limeSurveyId) {
        this.limeSurveyId = limeSurveyId;
        return this;
    }

    public AssessmentVersion getAssessmentVersion() {
        return Platform.instance().getAssessmentVersions().getById(assessmentVersionId);
    }

    public abstract Report prepareReport();


    public String getNextId() {
        return getAssessmentVersion().getNextSectionId(id);
    }

    public String getPrevId() {
        return getAssessmentVersion().getPrevSectionId(id);
    }

}
