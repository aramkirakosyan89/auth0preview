package org.ce.assessment.platform.dashboard;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.ce.assessment.limeapi.answeroptions.CheckedAnswerOption;
import org.ce.assessment.platform.*;
import org.ce.assessment.platform.report.Section;
import org.ce.assessment.platform.report.SevenElementsReport;
import org.ce.assessment.platform.report.SevenElementsSection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Challenges extends DbTable {

    private final static Logger log = LoggerFactory.getLogger(Challenges.class);
    public static final String TABLE = "challenges";

    public Challenges() {
        super(Platform.instance());
    }

    private Challenge fromDbRow(ResultSet rs) throws SQLException {
        return new Challenge(
                rs.getString("challenge"),
                rs.getString("tier2_strategy"),
                rs.getString("element"),
                rs.getString("element_id"),
                rs.getInt("frequency"));
    }

    void updateAssessmentSection(AssessmentSection assessmentSection) {

        Map<String, List<String>> challenges = getChallengesFromReport((SevenElementsReport) assessmentSection.getReport());

        String sectionId = assessmentSection.getId();
        String element = assessmentSection.getName();
        String elementId = assessmentSection.getAssessmentVersionSection().getId();

        try(DbQuery query = query("delete from " + TABLE + " where assessment_section_id = " + sectionId)) {
            query.execute();
        } catch (SQLException | NamingException e) {
            log.error("Could not delete challenges for assessment section " + sectionId);
            return;
        }

        String insertSql = "insert into " + TABLE + " " +
                "(assessment_section_id, tier2_strategy, challenge, element, element_id) values (?,?,?,?,?);";

        try (DbQuery query = query(insertSql)) {
            PreparedStatement st = query.getPreparedStatement();

            for (Map.Entry<String, List<String>> t2Challenges : challenges.entrySet()) {
                String tier2Strategy = t2Challenges.getKey();
                for (String challenge : t2Challenges.getValue()) {
                    st.setInt(1, Integer.valueOf(sectionId));
                    st.setString(2, tier2Strategy);
                    st.setString(3, challenge);
                    st.setString(4, element);
                    st.setInt(5, Integer.valueOf(elementId));
                    st.execute();
                }
            }
        } catch (SQLException |NamingException e) {
            log.error("Could not update challenges for assessment section " + assessmentSection.getId() + ". " + e.toString());
            e.printStackTrace();
        }
    }

    private Map<String, List<String>> getChallengesFromReport(SevenElementsReport report) {
        Map<String, List<String>> challenges = new HashMap<>();
        for (Section section : report.sections.values()) {
            SevenElementsSection t2 = (SevenElementsSection) section;
            List<CheckedAnswerOption> selectedChallenges = t2.getKeyChallenges().getSelectedOptions();
            List<String> t2Challenges = new ArrayList<>();
            for (CheckedAnswerOption selectedChallenge : selectedChallenges) {
                t2Challenges.add(selectedChallenge.getText());
            }
            challenges.put(t2.getName(), t2Challenges);
        }
        return challenges;
    }

    public String getForCommunity(String communityAssessmentId) {
        AssessmentVersion assessmentVersion = new CommunityAssessments().getById(communityAssessmentId).getAssessmentVersion();
        Map<String, List<String>> tiers = assessmentVersion.getTiers();

        Map<String, Map<String, Map<String, Challenge>>> challenges = new LinkedHashMap<>();
        for (Map.Entry<String, List<String>> tier1 : tiers.entrySet()) {
            Map<String, Map<String, Challenge>> t2s = new LinkedHashMap<>();
            for (String tier2 : tier1.getValue()) {
                t2s.put(tier2, new TreeMap<>(Comparator.reverseOrder()));
            }
            challenges.put(tier1.getKey(), t2s);
        }

        String sql = "select\n" +
                "       challenge, tier2_strategy, element, element_id,\n" +
                "       count(*) as frequency\n" +
                "from\n" +
                "    " + TABLE + " c\n" +
                "    join " + AssessmentSections.TABLE + " s on c.assessment_section_id = s.id\n" +
                "    join " + Assessments.TABLE + " a on s.assessment_id = a.id\n" +
                "where\n" +
                "    a.community_assessment_id = " + communityAssessmentId + "\n" +
                "    and a.excluded_from_dashboard is not true\n" +
                "group by\n" +
                "         challenge, tier2_strategy, element, element_id\n" +
                ";";

        Set<String> challengeNames = new TreeSet<>(Comparator.reverseOrder());

        try (DbQuery query =  query(sql);
             ResultSet rs = query.getResults()) {

            while (rs.next()) {
                Challenge challenge = fromDbRow(rs);
                Map<String, Map<String, Challenge>> t1 = challenges.get(challenge.element);
                if (t1 == null) {
                    log.error("Unknown tier1 element '" + challenge.element + "' " +
                            "for assessment version " + assessmentVersion.getId());
                    continue;
                }

                Map<String, Challenge> t2 = t1.get(challenge.tier2Strategy);
                if (t2 == null) {
                    log.error("Unknown tier 2 strategy '" + challenge.tier2Strategy+ "' " +
                            "for assessment version " + assessmentVersion.getId());
                    continue;
                }

                t2.put(challenge.name, challenge);
                challengeNames.add(challenge.name);  // TODO:  bad - may not capture all challenges
            }

        } catch (SQLException | NamingException e) {
            log.error("Could not get challenges for community assessment id = " +communityAssessmentId + ". " + e.toString());
            e.printStackTrace();
        }

        return toJson(challenges, challengeNames);
    }


    private String toJson(Map<String, Map<String, Map<String, Challenge>>> challenges,
                          Set<String> challengeNames)
    {

        JsonObject root = new JsonObject();

        JsonObject t1 = new JsonObject();
        t1.addProperty("name", "");
        t1.addProperty("drilldown", true);

        JsonArray t1Names = new JsonArray();
        for (String t1Name : challenges.keySet()) {
            t1Names.add(t1Name);
        }
        t1.add("categories", t1Names);
        
        {
            JsonArray t1Series = new JsonArray();
            for (String challengeName : challengeNames) {
                JsonObject s = new JsonObject();
                s.addProperty("name", challengeName);
                JsonArray d = new JsonArray();
                for (Map<String, Map<String, Challenge>> cT1 : challenges.values()) {
                    int frequency = 0;
                    for (Map<String, Challenge> cT2 : cT1.values()) {
                        Challenge challenge = cT2.get(challengeName);
                        if (challenge != null) {
                            frequency += challenge.frequency;
                        }
                    }
                    d.add(frequency);
                }
                s.add("data", d);
                t1Series.add(s);
            }
            t1.add("series", t1Series);
        }

        root.add("t1", t1);


        // by Element
        JsonArray byElement = new JsonArray();

        for (Map.Entry<String, Map<String, Map<String, Challenge>>> mapEntry : challenges.entrySet()) {
            String t1Name = mapEntry.getKey();
            Map<String, Map<String, Challenge>> cT1 = mapEntry.getValue();

            JsonObject t1Element = new JsonObject();
            t1Element.addProperty("name", t1Name);

            JsonArray t2Series = new JsonArray();
            for (String name : challengeNames) {
                JsonObject s = new JsonObject();
                s.addProperty("name", name);
                JsonArray d = new JsonArray();
                for (Map<String, Challenge> cT2 : cT1.values()) {
                    Challenge challenge = cT2.get(name);
                    d.add((challenge != null)?challenge.frequency:0);
                }
                s.add("data", d);
                t2Series.add(s);
            }
            t1Element.add("series", t2Series);


            JsonArray t2Categories = new JsonArray();
            for (String t2Category : cT1.keySet()) {
                t2Categories.add(t2Category);
            }
            t1Element.add("categories", t2Categories);

            byElement.add(t1Element);
        }


        root.add("byElement", byElement);


        return root.toString();

    }



}
