package org.ce.assessment.platform.invites;

import org.ce.assessment.platform.Community;
import org.ce.assessment.platform.Platform;
import org.ce.assessment.userdb.OrganizationAdmins;
import org.ce.assessment.userdb.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CommunityMemberExistingOrgInvite extends CommunityMemberInvite {


    private final static Logger log = LoggerFactory.getLogger(CommunityMemberExistingOrgInvite.class);

    public CommunityMemberExistingOrgInvite(String token, String communityId, String organizationId) {
        super(token, Invites.InviteType.COMMUNITY_BY_ORG, communityId);
        setOrganizationId(organizationId);
        setAsAdmin(false);
    }

    @Override
    public boolean sendInvitationEmails(String host) {

        Community community = getCommunity();
        if (community == null) {
            getLogger().error("Community("+getCommunityId()+") from invite(" + token + ") does not exist");
            return false;
        }

        // TODO: use button in the email to join the community
        String emailSubject = "Join " + community.getName() + " on Circle Assessment";

        String emailMsg =
                "Your organization is invited to join " + community.getName()
                        + " on the Circle Assessment platform<br>"
                        + "<a href=\"" + host + "\">"
                        + "Login to accept invitation</a>";

        List<User> admins = new OrganizationAdmins().getForOrganization(getOrganizationId());
        for (User admin : admins) {
            if (!Platform.instance().getMailer().sendMsg(admin.getEmail(), emailSubject, emailMsg)) {
                log.error("Could not send community invitation email to : " + admin.getEmail() + ". ");
            }
        }
        return true;
    }

    @Override
    protected Logger getLogger() {
        return log;
    }
}
