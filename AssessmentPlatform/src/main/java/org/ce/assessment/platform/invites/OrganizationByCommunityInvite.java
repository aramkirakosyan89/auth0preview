package org.ce.assessment.platform.invites;

import org.ce.assessment.platform.Community;
import org.ce.assessment.userdb.Organization;
import org.ce.assessment.userdb.Organizations;
import org.ce.assessment.userdb.User;

import static org.ce.assessment.platform.api.AbstractApi.P_INVITE_TOKEN;

public class OrganizationByCommunityInvite extends OrganizationMemberInvite {

    public OrganizationByCommunityInvite(String token, Community community, Organization organization, String email) {
        super(token, Invites.InviteType.ORGANIZATION_BY_COMMUNITY, organization, email, true);
        setCommunity(community);
    }

    public OrganizationByCommunityInvite(String token, String communityId, String organizationId, String email) {
        super(token, Invites.InviteType.ORGANIZATION_BY_COMMUNITY, organizationId, email, true);
        setCommunityId(communityId);
    }

    @Override
    public boolean sendInvitationEmails(String host) {
        String orgName = getOrganization().getName();
        String communityName = getCommunity().getName();

        String emailSubject = "Join " + communityName + " on Circle Assessment";
        String emailMsg =
                "Your organization " + orgName + " is invited to join " + communityName
                        + " on the Circle Assessment tool<br>"
                        + "<a href=\"" + host + "register.jsp?"
                        + param(P_INVITE_TOKEN, token)
                        + "\">Register here</a>";

        return sendEmail(getEmail(), emailSubject, emailMsg);
    }

    @Override
    public boolean accept() {
        User user = getRegisteredUser();
        String newOrgName = user.getOrganizationName();
        if (newOrgName != null && !getOrganization().getName().equals(newOrgName)) {
            getOrganization().setName(newOrgName);
            new Organizations().update(getOrganization());
        }

        return super.accept();
    }
}
