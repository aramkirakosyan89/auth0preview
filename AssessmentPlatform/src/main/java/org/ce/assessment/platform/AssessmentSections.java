package org.ce.assessment.platform;

import org.ce.assessment.limeapi.AddParticipants;
import org.ce.assessment.limeapi.ParticipantData;
import org.ce.assessment.limeapi.Survey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class AssessmentSections extends DbTableWithId<AssessmentSection> {

    private final static Logger log = LoggerFactory.getLogger(AssessmentSections.class);
    public static final String TABLE = "assessment_sections";

    public AssessmentSections() {
        super(Platform.instance());
    }

    @Override
    protected AssessmentSection fromDbRow(ResultSet rs) throws SQLException {
        return new AssessmentSection(
                rs.getString("id"),
                rs.getString("assessment_id"),
                rs.getString("assessment_version_section_id"),
                rs.getString("token"),
                id -> new Assessments().getById(id),
                null
                //id -> new AssessmentVersionSectionsDb().getById(id)
        )
            .setCompleted(rs.getBoolean("completed"))
            .setScore(rs.getInt("score"));
    }

    protected String table() {
        return TABLE;
    }

    @Override
    protected String entity() {
        return "assessment section";
    }

    @Override
    protected Logger log() {
        return log;
    }

    /** Should be always called during creation of an assessment */
    AssessmentSection create(Assessment assessment, AssessmentVersionSection versionSection) {

        Survey survey = new Survey(versionSection.getLimeSurveyId());
        String token = new AddParticipants(survey).addParticipant(
                new ParticipantData()
                        .setFirstName(assessment.getOrganization().getName())
                        .setLastName(assessment.getId() + ". " + assessment.getName())
                );

        if (token == null) {
            log.error("Failed to create a token for assessment(" + assessment.getId() + "), section (" + versionSection.getId() + ")");
            return null;
        }

        String sql = "insert into " + table() + "(assessment_id, assessment_version_section_id, token) values " +
                "(" + assessment.getId() + ", " + versionSection.getId() + ", '" + token + "') returning id";
        return createWithSql(sql);
    }

    List<AssessmentSection> getForAssessment(String assessmentId) {
        return getAll(" where assessment_id = " + assessmentId);
    }

    public void update(AssessmentSection assessmentSection) {
        String sql = "update " + table() + " set completed=" + assessmentSection.isCompleted() + ", date_completed=now(), "
                + " score=" + assessmentSection.getScore() + " where id=" + assessmentSection.getId();
        try (DbQuery query = query(sql)) {
            query.execute();
        } catch (SQLException | NamingException e) {
            log.error("Could not update assessment(" + assessmentSection.getAssessmentId() +
                    " section(" + assessmentSection.getId() + "). " + e.toString());
            e.printStackTrace();
        }
    }

    public String getAssessmentSectionIdByVersionSectionId(String assessmentId, String versionSectionId) {
        String url = "select id from assessment_sections where assessment_id = " + assessmentId +
                " and assessment_version_section_id = " + versionSectionId;
        try (DbQuery query = query(url);
            ResultSet rs = query.getResults())
        {
            if (rs.next()) {
                return rs.getString("id");
            } else {
                log.error("Unknown version section id(" + versionSectionId + ") for assessment(" + assessmentId  +")");
            }
        } catch (SQLException | NamingException e) {
            log.error("Could not get assessment section id by version section id "  + versionSectionId + ") for assessment(" + assessmentId  +"). "+ e.toString());
            e.printStackTrace();
        }
        return null;
    }

}
