package org.ce.assessment.platform.report;

import org.ce.assessment.limeapi.Survey;
import org.ce.assessment.limeapi.question.ReportFactory;
import org.ce.assessment.limeapi.question.TextQuestion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/*
Section has a name, a scoreQuestion and sectors inside of it
 */
public class Section extends ConditionalElement<Section> {

    private final static Logger log = LoggerFactory.getLogger(Section.class);

    protected transient final Survey survey;
    protected final ReportFactory f;
    private final String name;
    private String icon;

    private TextQuestion scoreQuestion;
    private TextQuestion definition = TextQuestion.DUMMY;

    public final Map<String, Section> sections = new LinkedHashMap<>();
    public final List<SectorInterface> sectors = new ArrayList<>();

    Section(String name, Survey survey) {
        this.name = name;
        this.survey = survey;
        this.f = survey.factory;
    }


    /**
     * @param name of the new section
     * @param scoreQuestion question with the score value
     * @return newly added sub-section
     */
    public Section addSection(String name, TextQuestion scoreQuestion) {
        Section section = new Section(name, survey).withScoreQuestion(scoreQuestion);
        sections.put(name, section);
        return section;
    }

    public Section withSector(SectorInterface sector) {
        sectors.add(sector);
        return this;
    }

    public String getName() {
        return name;
    }

    public Section withScoreQuestion(TextQuestion scoreQuestion) {
        this.scoreQuestion = scoreQuestion;
        return this;
    }

    public int getScore() {
        if (scoreQuestion != null) {
            String score = scoreQuestion.value();
            if (score.isEmpty()) {
                log.error("Could not get score for survey (" + this.survey.getId() + ") section(" + this.name + ")");
                throw new RuntimeException("Survey is not yet loaded");
            } else if ("NAN".equals(score)) {  //case when no strategies are applicable, max score for the section is 0
                return LetterScore.NOT_APPLICABLE;
            }
            return (int) Float.valueOf(score).floatValue();
        }
        throw new UnsupportedOperationException("This report section (" + name + ") does not have a scoreQuestion");
    }

    public Section setDefinition(TextQuestion definition) {
        this.definition = definition;
        return this;
    }

    public TextQuestion getDefinition() {
        return definition;
    }

    public Section setIcon(String icon) {
        this.icon = icon;
        return this;
    }

    public String getIcon() {
        return icon;
    }
}
