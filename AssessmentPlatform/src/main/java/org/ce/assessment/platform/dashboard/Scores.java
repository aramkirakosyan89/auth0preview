package org.ce.assessment.platform.dashboard;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.ce.assessment.platform.*;
import org.ce.assessment.platform.report.Section;
import org.ce.assessment.platform.report.SevenElementsReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Scores extends DbTable {

    public Scores() {
        super(Platform.instance());
    }

    public final static String TABLE = "scores";
    public final static String TIER1_TOTAL = "Tier1 Total";

    private final static Logger log = LoggerFactory.getLogger(Scores.class);

    private Map<String, Integer> getScoresFromReport(SevenElementsReport report) {
        Map<String, Integer> scores = new HashMap<>();
        for (Section section : report.sections.values()) {
            scores.put(section.getName(), section.getScore());
        }
        scores.put(TIER1_TOTAL, report.getScore());
        return scores;
    }

    void updateAssessmentSection(AssessmentSection assessmentSection) {
        Map<String, Integer> scores = getScoresFromReport((SevenElementsReport) assessmentSection.getReport());

        String sectionId = assessmentSection.getId();
        String element = assessmentSection.getName();
        String elementId = assessmentSection.getAssessmentVersionSection().getId();

        try(DbQuery query = query("delete from " + TABLE + " where assessment_section_id = " + sectionId)) {
            query.execute();
        } catch (SQLException | NamingException e) {
            log.error("Could not delete tier2 scores for assessment section " + sectionId);
            return;
        }

        String insertSql = "insert into " + TABLE + " " +
                "(assessment_section_id, tier2_strategy, score, element, element_id) values (?,?,?,?,?);";

        try (DbQuery query = query(insertSql)) {
            PreparedStatement st = query.getPreparedStatement();

            for (Map.Entry<String, Integer> score: scores.entrySet()) {
                String tier2Strategy = score.getKey();
                st.setInt(1, Integer.valueOf(sectionId));
                st.setString(2, tier2Strategy);
                st.setInt(3, score.getValue());
                st.setString(4, element);
                st.setInt(5, Integer.valueOf(elementId));
                st.execute();
            }
        } catch (SQLException |NamingException e) {
            log.error("Could not update scores for assessment section " +
                    assessmentSection.getId() + ". " + e.toString());
            e.printStackTrace();
        }
    }


    public String getTotalsForCommunityAssessment(String communityAssessmentId) {
        return getTotalsForCommunityAssessment(communityAssessmentId, null);
    }

    /**
     * @param communityAssessmentId
     * @param ownAssessmentId - optional, assessment id to overlay own scores over the aggregates. If not needed - should be passed as null
     * @return
     */
    public String getTotalsForCommunityAssessment(String communityAssessmentId, String ownAssessmentId) {

        boolean getOwnScores = ownAssessmentId != null;

        AssessmentVersion assessmentVersion =
                new CommunityAssessments().getById(communityAssessmentId).getAssessmentVersion();

        Map<String, Map<String, Map<String, Integer>>> scores =
                loadTotalsForCommunityAssessment(communityAssessmentId, assessmentVersion);
        if (scores == null) {
            log.error("Could not load aggregate scores for community assessment id=" + communityAssessmentId);
            return "{}";
        }

        Map<String, Map<String, Integer>> ownScores = null;
        if (getOwnScores) {
            ownScores = loadAssessmentScores(ownAssessmentId, assessmentVersion);
            if (ownScores == null) {
                log.error("Could not get scores for own assessment id = " + ownAssessmentId);
                return "{}";
            }
        }

        JsonObject root = new JsonObject();

        JsonObject t1Scores = new JsonObject();

        JsonArray labels_json = new JsonArray();
        JsonArray min_json = new JsonArray();
        JsonArray max_json = new JsonArray();
        JsonArray avg_json = new JsonArray();
        JsonArray own_json = new JsonArray();


        for (Map.Entry<String, Map<String, Map<String, Integer>>> t1Entry : scores.entrySet()) {
            labels_json.add(t1Entry.getKey());
            Map<String, Integer> score = t1Entry.getValue().get(TIER1_TOTAL);
            min_json.add(score.get("min"));
            max_json.add(score.get("max"));
            avg_json.add(score.get("avg"));

            if (getOwnScores) {
                own_json.add(ownScores.get(t1Entry.getKey()).get(TIER1_TOTAL));
            }

        }
        t1Scores.add("labels", labels_json);
        t1Scores.add("min", min_json);
        t1Scores.add("max", max_json);
        t1Scores.add("avg", avg_json);
        if (getOwnScores) {
            t1Scores.add("own", own_json);
        }

        t1Scores.addProperty("drilldown", true);
        t1Scores.addProperty("tickSize", 51.43);

        root.add("t1", t1Scores);

        JsonArray t2s = new JsonArray();
        for (Map.Entry<String, Map<String, Map<String, Integer>>> t1Entry : scores.entrySet()) {
            JsonObject t2scores = new JsonObject();
            t2scores.addProperty("name", t1Entry.getKey());

            labels_json = new JsonArray();
            min_json = new JsonArray();
            max_json = new JsonArray();
            avg_json = new JsonArray();
            own_json = new JsonArray();

            for (Map.Entry<String, Map<String, Integer>> t2Entry : t1Entry.getValue().entrySet()) {
                if (TIER1_TOTAL.equals(t2Entry.getKey())) {
                    continue;
                }
                labels_json.add(t2Entry.getKey());
                Map<String, Integer> score = t2Entry.getValue();
                min_json.add(score.get("min"));
                max_json.add(score.get("max"));
                avg_json.add(score.get("avg"));
                if (getOwnScores) {
                    int ownScore = ownScores.get(t1Entry.getKey()).get(t2Entry.getKey());
                    own_json.add(ownScore);
                }
            }
            t2scores.add("labels", labels_json);
            t2scores.add("min", min_json);
            t2scores.add("max", max_json);
            t2scores.add("avg", avg_json);
            if (getOwnScores) {
                t2scores.add("own", own_json);
            }

            t2scores.addProperty("tickSize", 360.0f / (t1Entry.getValue().size()-1) );


            t2s.add(t2scores);
        }

        root.add("t2s", t2s);
        return root.toString();

    }

    private Map<String, Map<String, Integer>> loadAssessmentScores(String assessmentId, AssessmentVersion assessmentVersion) {
        Map<String, Map<String, Integer>> scores = new LinkedHashMap<>();

        Map<String, List<String>> tiers = assessmentVersion.getTiers();
        for (Map.Entry<String, List<String>> t1 : tiers.entrySet()) {
            Map<String, Integer> t2Scores = new LinkedHashMap<>();

            for (String t2 : t1.getValue()) {
                t2Scores.put(t2, -1);
            }
            t2Scores.put(TIER1_TOTAL, -1);
            scores.put(t1.getKey(), t2Scores);
        }

        String sql = "" +
                " select\n" +
                "   s.element,\n" +
                "   s.tier2_strategy,\n" +
                "   s.score\n" +
                " from\n" +
                "   scores s\n" +
                "   join assessment_sections a on s.assessment_section_id = a.id\n" +
                " where a.assessment_id = " + assessmentId;
        try (DbQuery query = query(sql);
             ResultSet rs = query.getResults())
        {
            while (rs.next()) {
                scores.get(rs.getString("element"))
                        .put(rs.getString("tier2_strategy"), rs.getInt("score"));
            }

        } catch (SQLException | NamingException e) {
            e.printStackTrace();
        }


        return scores;
    }

    private Map<String, Map<String, Map<String, Integer>>> loadTotalsForCommunityAssessment(String communityAssessmentId,
                AssessmentVersion assessmentVersion) {

        Map<String, Map<String, Map<String, Integer>>> scores = new LinkedHashMap<>();

        Map<String, Integer> empty = new HashMap<>();
        empty.put("min", 0);
        empty.put("max", 0);
        empty.put("avg", 0);

        Map<String, List<String>> tiers = assessmentVersion.getTiers();
        for (Map.Entry<String, List<String>> t1 : tiers.entrySet()) {
            Map<String, Map<String, Integer>> t2Scores = new LinkedHashMap<>();
            for (String t2 : t1.getValue()) {
                t2Scores.put(t2, empty);
            }
            t2Scores.put(TIER1_TOTAL, empty);
            scores.put(t1.getKey(), t2Scores);
        }

        String sql = "select\n" +
                "    t.element,\n" +
                "    t.tier2_strategy,\n" +
                "    max(t.score) as max,\n" +
                "    min(t.score) as min,\n" +
                "    avg(t.score) as avg\n" +
                "from\n" +
                "    " + TABLE + " t\n" +
                "    join " + AssessmentSections.TABLE + " s on t.assessment_section_id = s.id\n" +
                "    join " + Assessments.TABLE + " a on s.assessment_id = a.id\n" +
                "where\n" +
                "    a.community_assessment_id = " + communityAssessmentId + "\n" +
                "    and t.score >= 0\n" +
                "    and a.excluded_from_dashboard is not true\n" +
                "group by\n" +
                "    t.tier2_strategy,\n" +
                "    t.element";

        try (DbQuery query = query(sql);
             ResultSet rs = query.getResults())
        {
            while (rs.next()) {
                Map<String, Integer> score = new HashMap<>();
                score.put("min", rs.getInt("min"));
                score.put("max", rs.getInt("max"));
                score.put("avg", rs.getInt("avg"));

                String t2Strategy = rs.getString("tier2_strategy");
                String t1Element = rs.getString("element");
                Map<String, Map<String, Integer>> t1Scores = scores.get(t1Element);
                if (t1Scores == null) {
                    log.error("No tier 1 element '" + t1Element + "' in assessment version " + assessmentVersion.getId());
                    return null;
                }
                if (!t1Scores.containsKey(t2Strategy)) {
                    log.error("No tier 2 strategy '" + t2Strategy + "' in assessment version " + assessmentVersion.getId());
                    return null;
                }

                t1Scores.put(t2Strategy, score);
            }
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
        }
        return scores;
    }

}
