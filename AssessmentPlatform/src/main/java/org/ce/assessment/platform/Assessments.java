package org.ce.assessment.platform;

import org.ce.assessment.platform.assessmentVersions.AssessmentVersionsCoded;
import org.ce.assessment.userdb.Organization;
import org.ce.assessment.userdb.Organizations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Assessments extends DbTableWithId<Assessment> {

    private final static Logger log = LoggerFactory.getLogger(Assessments.class);
    public final static String TABLE = "assessments";

    public Assessments() {
        super(Platform.instance());
    }

    @Override
    protected String table() {
        return TABLE;
    }

    @Override
    protected String entity() {
        return "assessment";
    }

    @Override
    protected String entities() {
        return "assessments";
    }

    @Override
    protected Logger log() {
        return log;
    }

    @Override
    protected Assessment fromDbRow(ResultSet rs) throws SQLException {
        return new Assessment(
                        id -> new Organizations().getById(id),
                        id -> new AssessmentSections().getForAssessment(id)
                )
                .setId(rs.getString("id"))
                .setAssessmentVersionId(rs.getString("assessment_version_id"))
                .setOrganizationId(rs.getString("organization_id"))
                .setName(rs.getString("name"))
                .setCommunityAssessmentId(rs.getString("community_assessment_id"))
                .setIsExcludedFromDashboard(rs.getBoolean("excluded_from_dashboard"))
                ;
    }

    public Assessment create(String assessmentVersionId, String organizationId) {
        Organization organization = new Organizations().getById(organizationId);
        if (organization == null) {
            log.error("Unknown organization id = " + organizationId + "/n");
            return null;
        }

        return create(assessmentVersionId, organization, null);
    }

    public Assessment create(Organization organization, CommunityAssessment communityAssessment) {
        return create(communityAssessment.getAssessmentVersionId(), organization, communityAssessment);
    }

    public Assessment create(String assessmentVersionId, Organization organization, CommunityAssessment communityAssessment) {

        AssessmentVersion assessmentVersion = new AssessmentVersionsCoded().getById(assessmentVersionId);
        if (assessmentVersion == null) {
            log.error("Unknown assessment version id = " + assessmentVersionId);
            return null;
        }

        String name = (communityAssessment == null)
                ? assessmentVersion.getName()
                : communityAssessment.getName();

        String sql = "insert into " + table() +
                " (assessment_version_id, organization_id, name, community_assessment_id) values (" +
                assessmentVersionId +
                comma(organization.id)+
                comma(quotes(name)) +
                comma(communityAssessment.getId()) +
                ") returning id;";
        Assessment assessment = createWithSql(sql);
        if (assessment == null) {
            return null;
        }

        // create sections by adding participants to respected lime surveys and getting new tokens
        AssessmentSections sections = new AssessmentSections();
        for (AssessmentVersionSection versionSection : assessmentVersion.getSectionList()) {
            AssessmentSection section = sections.create(assessment, versionSection);
            if (section == null) {
                log().error("Could not create all assessment sections");
                delete(assessment.getId());
                return null;
            }
        }

        log.info("Created new assessment " + assessment.getName());
        return assessment;
    }

    public boolean setExcludedFromDashboard(String assessmentId, boolean setExcluded) {
        String sql = "update " + TABLE + " set excluded_from_dashboard=" + setExcluded + " where id=" + assessmentId + ";";
        try (DbQuery query = query(sql)) {
            query.execute();
        } catch (SQLException | NamingException e) {
            log.error("Could not set excluded " + setExcluded + " for assessment id (" + assessmentId + "). " + e.toString());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public List<Assessment> getForOrganization(String organizationId) {
        return getAll(" where organization_id = " + organizationId);
    }
}
