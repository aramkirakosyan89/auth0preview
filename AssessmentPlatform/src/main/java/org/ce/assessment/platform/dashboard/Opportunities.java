package org.ce.assessment.platform.dashboard;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.ce.assessment.platform.*;
import org.ce.assessment.platform.report.Section;
import org.ce.assessment.platform.report.SevenElementsReport;
import org.ce.assessment.platform.report.SevenElementsSection;
import org.ce.assessment.platform.report.Strategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Opportunities extends DbTable {

    private final static Logger log = LoggerFactory.getLogger(Opportunities.class);
    private final String TABLE = "opportunities";

    public Opportunities() {
        super(Platform.instance());
    }

    private Opportunity fromDbRow(ResultSet rs) throws SQLException {
        return new Opportunity(
                rs.getString("name"),
                rs.getString("element"),
                rs.getString("element_id"),
                rs.getInt("frequency"));
    }


    private List<String> getOpportunitiesFromReport(SevenElementsReport report) {
        List<String> opportunities = new ArrayList<>();
        for (Section s : report.sections.values())  {
            for (Strategy t : ((SevenElementsSection) s).getStrategies()) {
                if (t.isOpportunity()) {
                    opportunities.add(t.getName());
                }
            }
        }
        return opportunities;
    }

    void updateAssessmentSection(AssessmentSection assessmentSection) {
        List<String> opportunities = getOpportunitiesFromReport((SevenElementsReport) assessmentSection.getReport());

        String sectionId = assessmentSection.getId();
        String element = assessmentSection.getName();
        String elementId = assessmentSection.getAssessmentVersionSection().getId();



        try(DbQuery query = query("delete from " + TABLE + " where assessment_section_id = " + sectionId)) {
            query.execute();
        } catch (SQLException |NamingException e) {
            log.error("Could not delete opportunities for assessment section " + sectionId);
            return;
        }

        String insertSql = "insert into " + TABLE + " " +
                "(assessment_section_id, strategy, element, element_id) values (?, ?, ?, ?); ";

        try (DbQuery query = query(insertSql)) {
            PreparedStatement st = query.getPreparedStatement();

            for (String opportunity : opportunities) {
                st.setInt(1, Integer.valueOf(sectionId));
                st.setString(2, opportunity);
                st.setString(3, element);
                st.setInt(4, Integer.valueOf(elementId));
                st.execute();
            }
        }
        catch (SQLException |NamingException e) {
            log.error("Could not update opportunities for assessment section " + assessmentSection.getId() + ". " + e.toString());
            e.printStackTrace();
        }
    }

    public boolean captureForCommunity(String communityAssessmentId) {
        String sql = "select s.id\n" +
                "from\n" +
                "     assessments a\n" +
                "     join assessment_sections s on a.id = s.assessment_id\n" +
                "where a.community_assessment_id = " + communityAssessmentId + " and s.completed = true ;";
        try (DbQuery query = query(sql);
            ResultSet rs = query.getResults()) {
            AssessmentSections assessmentSections = new AssessmentSections();
            int i = 0;
            while (rs.next()) {
                String sectionId = rs.getString("id");
                AssessmentSection section = assessmentSections.getById(sectionId);
                section.getReport();
                log.debug("completed " + ++i);
            }
        } catch (SQLException | NamingException e) {
            log.error(e.toString());
            e.printStackTrace();
            return false;
        }
        return true;
    }



    private String toJson(Map<String, Set<Opportunity>> opportunities) {

        JsonObject root = new JsonObject();

        JsonArray t1Totals = new JsonArray();
        for (Map.Entry<String, Set<Opportunity>> t1 : opportunities.entrySet()) {
            JsonObject t1_json = new JsonObject();
            t1_json.addProperty("name", t1.getKey());
            t1_json.addProperty("drilldown", t1.getKey());
            int frequency = 0;
            for (Opportunity opportunity : t1.getValue()) {
                frequency += opportunity.frequency;
            }
            t1_json.addProperty("y", frequency);
            t1Totals.add(t1_json);
        }
        root.add("t1", t1Totals);

        JsonArray t1DrillDown = new JsonArray();
        for (Map.Entry<String, Set<Opportunity>> t1 : opportunities.entrySet()) {
            JsonObject t1_json = new JsonObject();
            t1_json.addProperty("name", t1.getKey());
            t1_json.addProperty("id", t1.getKey());

            JsonArray t1Data = new JsonArray();
            for (Opportunity opportunity : t1.getValue()) {
                JsonArray opportunity_json = new JsonArray();
                opportunity_json.add(opportunity.name);
                opportunity_json.add(opportunity.frequency);
                t1Data.add(opportunity_json);
            }
            t1_json.add("data", t1Data);
            t1DrillDown.add(t1_json);
        }

        root.add("drilldown", t1DrillDown);


        return root.toString();
    }

    public String getForCommunity(String communityAssessmentId) {

        AssessmentVersion assessmentVersion = new CommunityAssessments().getById(communityAssessmentId).getAssessmentVersion();
        Map<String, List<String>> tiers = assessmentVersion.getTiers();

        Map<String, Set<Opportunity>> opportunities = new LinkedHashMap<>();
        for (String tier1 : tiers.keySet()) {
            opportunities.put(tier1, new TreeSet<Opportunity>((o1, o2) -> {return o2.frequency - o1.frequency;}));
        }

        String sql = "select\n" +
                "       o.strategy as name,\n" +
                "       o.element as element,\n" +
                "       o.element_id as element_id,\n" +
                "       count(*) as frequency\n" +
                "from\n" +
                    "     " + TABLE + " o \n" +
                "         join " + AssessmentSections.TABLE + " s on o.assessment_section_id = s.id\n" +
                "         join " + Assessments.TABLE + " a on s.assessment_id = a.id\n" +
                "where\n" +
                "    a.community_assessment_id = " + communityAssessmentId +"\n" +
                "    and a.excluded_from_dashboard is not true\n" +
                "group by\n" +
                "         o.strategy, o.element, o.element_id\n" +
                "order by \n" +
                "         count(*) DESC \n" +
                ";";

        try (DbQuery query =  query(sql);
             ResultSet rs = query.getResults()) {

            int i = 0;
            while (rs.next()) {
                Opportunity opportunity = fromDbRow(rs);
                Set<Opportunity> opportunitiesForElement = opportunities.get(opportunity.element);
                if (opportunitiesForElement == null) {
                    log.error("Unknown tier1 element '" + opportunity.element + "' " +
                            "for assessment version " + assessmentVersion.getId());
                    continue;
                }
                opportunitiesForElement.add(opportunity);
            }
        } catch (SQLException | NamingException e) {
            log.error("Could not get opportunities for community assessment id = " +communityAssessmentId + ". " + e.toString());
            e.printStackTrace();
        }

        return toJson(opportunities);

    }


    public Map<String, List<String>> getForAssessment(String assessmentId) throws DataException {

        Map<String, List<String>> opportunities = new LinkedHashMap<>();

        String sql = "select\n" +
                "      element,\n" +
                "      strategy\n" +
                "from\n" +
                "      opportunities o\n" +
                "      join assessment_sections a on o.assessment_section_id = a.id\n" +
                "where a.assessment_id = " + assessmentId + "\n" +
                "order by element, strategy";

        try (DbQuery query = query(sql);
            ResultSet rs = query.getResults())
        {
            while (rs.next()) {
                String element = rs.getString("element");
                String strategy = rs.getString("strategy");

                List<String> strategies = opportunities.get(element);
                if (strategies == null) {
                    strategies = new ArrayList<>();
                    opportunities.put(element, strategies);
                }
                strategies.add(strategy);
            }
        } catch (SQLException | NamingException e) {
            log.error("Could not get opportunities for assessment id = " + assessmentId + ". " + e.toString());
            throw new DataException(e);
        }
        return opportunities;
    }
}

