package org.ce.assessment.platform;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class CommunityAssessments extends DbTableWithId<CommunityAssessment> {

    private final static Logger log = LoggerFactory.getLogger(CommunityAssessments.class);
    private static final String COMMUNITY_ASSESSMENTS = "community_assessments";

    public CommunityAssessments() {
        super(Platform.instance());
    }

    public CommunityAssessment create(String communityId, String assessmentVersionId, String name) {
        String sql = "insert into " + table() + " (community_id, assessment_version_id, name) values (" +
                quotesAndComma(communityId) +
                quotesAndComma(assessmentVersionId) +
                quotes(name) +
                ") returning id;";
        return createWithSql(sql);
    }

    @Override
    protected CommunityAssessment fromDbRow(ResultSet rs) throws SQLException {
        return new CommunityAssessment()
                .setId(rs.getString("id"))
                .setName(rs.getString("name"))
                .setCommunityId(rs.getString("community_id"))
                .setAssessmentVersionId(rs.getString("assessment_version_id"))
                .setActivated(rs.getBoolean("activated"))
        ;
    }

    @Override
    protected String table() {
        return COMMUNITY_ASSESSMENTS;
    }

    @Override
    protected String entity() {
        return "community assessment";
    }

    @Override
    protected Logger log() {
        return log;
    }

    public List<CommunityAssessment> getForCommunity(Community community) {
        return getAll("where community_id = " + community.id);
    }

    /**
     *
     * @param communityAssessmentId
     * @return -1 if case of an error
     */
    public int getNOrganizationInDashboard(String communityAssessmentId) {
        String sql = "select count(distinct  a.organization_id) as cnt\n" +
                "from\n" +
                "     assessments a\n" +
                "where\n" +
                "    a.excluded_from_dashboard is not true\n" +
                "    and a.community_assessment_id = " + communityAssessmentId;
        try (   DbQuery query = query(sql);
                ResultSet rs = query.getResults()) {
            if (rs.next()) {
                return rs.getInt("cnt");
            } else {
                log.error("Could not get count from database for community assessment id(" + communityAssessmentId + ")");
            }
        } catch (SQLException | NamingException e) {
            log.error("Could not get count from database for community assessment id(" + communityAssessmentId + "). "
                    + e.toString());
            e.printStackTrace();
        }
        return -1;
    }
}
