package org.ce.assessment.platform.invites;

import org.ce.assessment.userdb.Organization;
import org.ce.assessment.userdb.OrganizationAdmins;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import static org.ce.assessment.platform.api.AbstractApi.P_INVITE_TOKEN;

public class OrganizationMemberInvite extends Invite {

    private final static Logger log = LoggerFactory.getLogger(OrganizationMemberInvite.class);

    public OrganizationMemberInvite(String token, String organizationId, String email, boolean asAdmin) {
        super(token, Invites.InviteType.USER);
        setOrganizationId(organizationId);
        setEmail(email);
        setAsAdmin(asAdmin);
    }
    public OrganizationMemberInvite(String token, Organization organization, String email, boolean asAdmin) {
        super(token, Invites.InviteType.USER);
        setOrganization(organization);
        setEmail(email);
        setAsAdmin(asAdmin);
    }

    protected OrganizationMemberInvite(String token,
                                       Invites.InviteType type,
                                       Organization organization,
                                       String email,
                                       boolean asAdmin)
    {
        super(token, type);
        setOrganization(organization);
        setEmail(email);
        setAsAdmin(asAdmin);
    }

    protected OrganizationMemberInvite(String token,
                                       Invites.InviteType type,
                                       String organizationId,
                                       String email,
                                       boolean asAdmin)
    {
        super(token, type);
        setOrganizationId(organizationId);
        setEmail(email);
        setAsAdmin(asAdmin);
    }


    @Context
    private HttpServletRequest request;



    private String getHost() {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + "/";
    }

    @Override
    public boolean sendInvitationEmails(String host) {
        String orgName = getOrganization().getName();
        String emailSubject = "Join " + orgName + " on Circle Assessment";
        String emailMsg =
                "You are invited to join " + orgName
                        + " on the Circle Assessment tool<br>"
                        + "<a href=\"" + host + "register.jsp?"
                        + param(P_INVITE_TOKEN, token)
                        + "\">Register here</a>";

        return sendEmail(getEmail(), emailSubject, emailMsg);
    }

    @Override
    public boolean accept() {
        if (asAdmin()) {
            new OrganizationAdmins().add(getOrganizationId(), getRegisteredUser().getId());
        }
        return super.accept();
    }

    @Override
    protected Logger getLogger() {
        return log;
    }
}
