package org.ce.assessment.platform.api;

import org.ce.assessment.platform.AuthManager;
import org.ce.assessment.platform.invites.Invite;
import org.ce.assessment.platform.invites.Invites;
import org.ce.assessment.userdb.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

import static org.ce.assessment.platform.api.ViewPath.HOME_PAGE;
import static org.ce.assessment.platform.api.ViewPath.ORG_HOME;

@Path("users")
public class UsersApi extends AbstractApi {

    private final static Logger log = LoggerFactory.getLogger(UsersApi.class);

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse servletResponse;

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("/save")
    public Response save(@FormParam("name") String name) {
        String redirectUrl;
        User userInSession = (User) request.getSession().getAttribute("userObject");

        Users users = new Users();
        User userData = new User();
        userData.setEmail(userInSession.getEmail());
        userData.setName(name);
        if(!userInSession.isUnKnown()) {
            userData.setOrganizationId(userInSession.getOrganizationId());
            redirectUrl = ORG_HOME + userInSession.getOrganizationId();
        } else {
            redirectUrl = HOME_PAGE;
        }
        // create user
        User user = users.createInDb(userData);
        if (user == null) {
            log.error("failed to create new user");
            // TODO redirect to Error page.
            return Response.serverError().entity("failed to create user").build();
        }

        log.info("Registered new user " + userData.getEmail());
        // Update user in session.
        request.getSession().setAttribute("userObject", userData);
        return Response.status(HttpServletResponse.SC_OK).entity(redirectUrl).build();
    }

    @POST
    @Path("{id}/password")
    public Response changePassword(@PathParam("id") String id,
                                    @FormParam("currentPassword") String currentPw,
                                    @FormParam("password") String newPw1,
                                    @FormParam("password2") String newPw2) {
        // TODO: check auth

        Users users = new Users();
        User user = users.getById(id);

        if (!user.checkPassword(currentPw)) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Wrong password").build();
        } else if (newPw1 == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("New password cannot be empty").build();
        } else if (!newPw1.equals(newPw2)) {
            return Response.status(Response.Status.BAD_REQUEST).entity("New passwords do not match").build();
        } else {
            user.setPassword(newPw1);
            if (!users.update(user)) {
                Response.serverError().entity("Could not set new password. Server error").build();
            }
        }
        return Response.ok().build();
    }


    @POST @Path("register")
    public Response registerByInvitation(   // TODO: move to invites API
            @FormParam("inviteToken") String inviteToken,
            @FormParam("email") String email,
            @FormParam("name") String name,
            @FormParam("orgName") String organizationName,
            @FormParam("password") String password,
            @FormParam("password2") String password2,
            @DefaultValue ("") @FormParam("jobTitle") String jobTitle,
            @DefaultValue ("") @FormParam("phone") String phone,
            @DefaultValue("false") @FormParam("okJoin") String okJoin
    ) throws ServletException, IOException {
        log.info("Registering " + email);

        if (inviteToken == null) {
            log.error("registering by invitation without invite token");
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("registering by invitation without invite token").build();
        }

        Invite invite = new Invites().getByToken(inviteToken);
        if (invite == null) {
            String msg = "unknown invitation token " + inviteToken;
            log.error(msg);
            return Response.status(Response.Status.BAD_REQUEST).entity(msg).build();
        }
        if (invite.getStatus() != Invites.InviteStatus.PENDING) {
            String msg = "Trying to register using " + invite.getStatus().toString()
                    + " invitation. Token " + inviteToken;
            log.warn(msg);
            return Response.status(Response.Status.FORBIDDEN).entity(msg).build();
        }

        if (!password.equals(password2)) {
            log.info("Register: passwords do not match");
            return Response.status(Response.Status.BAD_REQUEST).entity("passwords do not match").build();
        }

        User userData = new User("")
                .setEmail(email)
                .setOrganizationName(organizationName)
                .setPassword(password)
                .setName(name)
                .setJobTitle(jobTitle)
                .setPhone(phone)
                .setIsOrganizationAdmin(invite.asAdmin())
                ;

        Response response;
        switch (invite.type) {

            case USER:
            case ORGANIZATION_BY_COMMUNITY:
                userData.setOrganizationId(invite.getOrganizationId());
                response = createUser(userData);
                break;
            case COMMUNITY_BY_EMAIL:
                response = registerAndJoinCommunity(invite, userData, okJoin);
                break;
            default:
                return Response.serverError().entity("unexpected invite type").build();
        }
        if (response.getStatus() == Response.Status.CREATED.getStatusCode()) {
            invite.setRegisteredUser((User) response.getEntity());
            invite.accept();
            Response.ResponseBuilder successResponse = Response.status(Response.Status.CREATED);
            AuthManager authManager = new AuthManager();
            if (authManager.signIn(request, email, password) == AuthManager.Result.SIGNED_IN) {
                successResponse.entity(servletResponse.encodeURL(authManager.getUserHome(request)));
            } else {
                successResponse.entity("index.jsp");
            }
            return successResponse.build();
        }

        return Response.status(response.getStatus()).build();
    }

    Response registerAndJoinCommunity(Invite invite, User userData, String okJoin) {
        if (!invite.asAdmin()) {
            if (!"true".equals(okJoin)) {
                return  Response.status(Response.Status.EXPECTATION_FAILED)
                        .entity("did not accept to join community").build();
            }
        }

        Response response = register(userData);
        invite.setOrganizationId(((User) response.getEntity()).getOrganizationId());

        return response;
    }

    Response register(User userData) {

        Organizations organizations = new Organizations();

        // create org
        Organization organization = organizations.createNew(userData.getOrganizationName());
        if (organization == null) {
            return Response.serverError().entity("could not create organization").build();
        }
        userData.setOrganizationId(organization.id);

        // create user
        Response response = createUser(userData);
        if (response.getStatus() != Response.Status.CREATED.getStatusCode()) {
            organizations.delete(organization);
            log.info("Failed to register user " + userData.getEmail());
        }

        if (userData.isOrganizationAdmin()) {
            new OrganizationAdmins().add(organization.id, ((User) response.getEntity()).getId());
        }
        return response;
    }

    Response createUser(User userData) {
        if (userData.getEmail() == null) {
            log.error("unknown email");
            return Response.status(Response.Status.BAD_REQUEST).entity("missing email address").build();
        }

        Users users = new Users();
        if (null != users.getByEmail(userData.getEmail())) {
            log.error("email " + userData.getEmail() + " already registered");
            return Response.status(Response.Status.CONFLICT).entity("email already registered").build();
        }

        // create user
        User user = users.createInDb(userData);
        if (user == null) {
            log.error("failed to create new user");
            return Response.serverError().entity("failed to create user").build();
        }

        log.info("Registered new user " + userData.getEmail());
        return Response.status(HttpServletResponse.SC_CREATED).entity(user).build();
    }


    @Override
    protected Logger getLogger() {
        return log;
    }
}
