package org.ce.assessment.platform.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.ce.assessment.platform.*;
import org.ce.assessment.platform.dashboard.Opportunities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

@Path("assessments/{id}")
public class AssessmentsApi {

    private final static Logger log = LoggerFactory.getLogger(AssessmentsApi.class);
    private final Assessments A = new Assessments();

    private final String assessmentId;

    public AssessmentsApi(@PathParam("id") String assessmentId) {
        this.assessmentId = assessmentId;
    }


    @POST
    @Path("sections/{sectionId}/load-report")
    public Response loadReportForSection(@PathParam("sectionId") String sectionId)
    {
        AssessmentSection section = new AssessmentSections().getById(sectionId);
        if (!section.getReport().isLoaded()) {
            log.error("Could not load report for assessment section(" + section.getId());
            return Response.serverError().build();
        }

        return Response.ok().build();
    }

    @POST
    @Path("setExcludedFromDashboard")
    public Response setExcludedFromDashboard(@FormParam("setExcluded") boolean setExcluded) {
        if (!A.setExcludedFromDashboard(assessmentId, setExcluded)) {
            return Response.serverError().build();
        }
        return Response.ok().build();
    }

    @GET
    @Path("scores")
    public Response getScores() {
        Assessment assessment = new Assessments().getById(assessmentId);
        if (assessment == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        JsonArray scores = new JsonArray();

        for (AssessmentSection section : assessment.getSections()) {
            int score = section.getScore();
            scores.add(score);
        }
        return Response.ok(scores.toString()).build();
    }

    @GET
    @Path("opportunities")
    public Response getOpportunities() {

        try {
            Map<String, List<String>> opportunities = new Opportunities().getForAssessment(assessmentId);

            JsonArray j_opportunities = new JsonArray();
            for (Map.Entry<String, List<String>> element : opportunities.entrySet()) {
                JsonObject j_element = new JsonObject();
                j_element.addProperty("name", element.getKey());
                JsonArray j_strategies = new JsonArray();
                j_element.add("strategies", j_strategies);

                for (String strategy : element.getValue()) {
                    j_strategies.add(strategy);
                }

                j_opportunities.add(j_element);
            }

            return Response.ok().entity(j_opportunities.toString()).build();
        } catch (DataException e) {
            return Response.serverError().entity(e.getCause().toString()).build();
        }
    }
}
