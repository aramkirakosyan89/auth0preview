package org.ce.assessment.platform.api;

public class ViewPath {

    static final String HOME_PAGE = "/index.jsp";
    static final String ORG_HOME = "/organization.jsp?id=";
    static final String USER_INFO = "/userinfo.jsp";

}
