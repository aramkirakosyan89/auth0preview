package org.ce.assessment.platform.report;

import org.ce.assessment.limeapi.question.Question;
import org.ce.assessment.limeapi.question.TextQuestion;


/***
 * An element of a report
 * It is based on a single survey question
 */
public class Sector extends ConditionalElement<Sector> implements SectorInterface {
	public final String name;

	private Question definition = TextQuestion.DUMMY;
	private TextQuestion comment;

	public Sector(String name) {
	    this.name = name;
    }

	public Sector withDefinition(Question definition) {
	    this.definition = definition;
	    return this;
	}

	public Sector withComment(TextQuestion comment) {
		this.comment = comment;
		return this;
	}

	public TextQuestion getComment() {
		return comment;
	}

	public String getDefinition() {
		return definition.value();
	}

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String value() {
        return getDefinition();
    }
}
