package org.ce.assessment.platform.api;

import org.ce.assessment.platform.AuthManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("auth")
public class AuthApi extends AbstractApi {

    private final static Logger log = LoggerFactory.getLogger(AuthApi.class);
    @Context private HttpServletRequest request;
    private final AuthManager authManager = new AuthManager();

    @POST @Path("signout")
    public void singOut() {
        authManager.signOut(request);
    }

    @POST @Path("signin") //TODO: make use from index.jsp
    public Response signIn(@FormParam("email") String email, @FormParam("password") String password) throws ServletException, IOException {

        AuthManager.Result result = authManager.signIn(request, email, password);
        switch (result) {
            case SIGNED_IN:
                return Response.ok(response.encodeURL(authManager.getUserHome(request))).build();
            case WRONG_PASSWORD:
                return Response.status(Response.Status.UNAUTHORIZED).build();
            case UNKNOWN_EMAIL:
                return Response.status(Response.Status.BAD_REQUEST).build();
//                return Response.status(Response.Status.UNAUTHORIZED).entity(result.msg).build();
            default:
                return Response.serverError().entity(result.msg).build();
        }

    }

    @POST @Path("reqest-password-reset")
    public Response requestPasswordReset(@FormParam("email") String email) {
        AuthManager.Result result = new AuthManager().sendPasswordReset(email);
        if (result == AuthManager.Result.UNKNOWN_EMAIL) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Invalid email").build();
        } else if (result == AuthManager.Result.COULDNT_EMAIL) {
            return Response.serverError().build();
        } else if (result == AuthManager.Result.RESET_SENT) {
            return Response.ok().build();
        }
         return Response.serverError().entity("Unknown error").build();
    }

    @POST @Path("reset-password")
    public Response resetPassword(@FormParam("token") String token, @FormParam("password") String password) {
        AuthManager.Result result = new AuthManager().resetPassword(token, password);

        switch (result) {
            case PASSWORD_CHANGED:
                return Response.ok().build();
            case UNKNOWN_EMAIL:
            case INVALID_PASSWORD_RESET_REQUEST:
                return Response.status(Response.Status.BAD_REQUEST).entity(result.msg).build();
            default:
                return Response.serverError().build();
        }
    }


    @Override
    protected Logger getLogger() {
        return log;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }
}
