package org.ce.assessment.platform;

import org.ce.assessment.platform.report.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class AssessmentVersionSectionsDb extends DbTableWithId<AssessmentVersionSection> {

    private final static Logger log = LoggerFactory.getLogger(AssessmentVersionSectionsDb.class);

    @Override
    protected String table() {
        return "assessment_version_sections";
    }

    @Override
    protected String entity() {
        return "assessment version section";
    }

    @Override
    protected String entities() {
        return "assessment version sections";
    }

    @Override
    protected Logger log() {
        return log;
    }

    public AssessmentVersionSectionsDb() {
        super(Platform.instance());
    }

    public AssessmentVersionSection fromDbRow(ResultSet rs) throws SQLException {
        AssessmentVersionSection section = new AssessmentVersionSection() {
            @Override
            public Report prepareReport() {
                throw new NotImplementedException();
                //TODO: something
            }
        }
                .setAssessmentVersionId(rs.getString("assessment_version_id"))
                .setId(rs.getString("id"))
                .setKind(rs.getString("kind"))
                .setName(rs.getString("name"))
                .setLimeSurveyId(rs.getString("lime_survey_id"))
                ;

        return section;
    }

    public AssessmentVersionSection create(String assessmentVersionId,
                                           String kind,
                                           String name,
                                           String limeSurveyId)
    {
        String sql = "insert into " + table() + " (assessment_version_id, kind, name, lime_survey_id) " +
                "values (" + assessmentVersionId + ", " + kind + ", '" + name + "', '" + limeSurveyId + "') returning id;";
        return createWithSql(sql);
    }

    public List<AssessmentVersionSection> getForAssessmentVersion(String assessmentVersionId) {
        return getAll(" where assessment_version_id=" + assessmentVersionId);
    }
}