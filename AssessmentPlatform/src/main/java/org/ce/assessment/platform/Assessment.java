package org.ce.assessment.platform;

import org.ce.assessment.userdb.Organization;

import java.util.List;
import java.util.function.Function;



/**
 * Represents an assessment of an organization.
 * Consists of {@link AssessmentSection}s, each representing a separate survey
 */
public class Assessment {

    private String id;
    private String assessmentVersionId;
    private String organizationId;
    private String name;
    private String communityAssessmentId;
    private CommunityAssessment communityAssessment;
    private boolean isExcludedFromDashboard;

    private final Function<String, Organization> getOrganizationFunction;
    private final Function<String, List<AssessmentSection>> getSectionsFunction;

    public Assessment(Function<String, Organization> getOrganizationFunction,
                      Function<String, List<AssessmentSection>> getSectionsFunction)
    {
        this.getOrganizationFunction = getOrganizationFunction;
        this.getSectionsFunction = getSectionsFunction;
    }

    public Organization getOrganization() {
        return getOrganizationFunction.apply(getOrganizationId());
    }

    public List<AssessmentSection> getSections() {
        return getSectionsFunction.apply(id);
    }

    public String getCommunityAssessmentId() {
        return communityAssessmentId;
    }

    public Assessment setCommunityAssessmentId(String communityAssessmentId) {
        this.communityAssessmentId = communityAssessmentId;
        return this;
    }

    public CommunityAssessment getCommunityAssessment() {
        if (communityAssessment == null) {
            if (null != communityAssessmentId && !communityAssessmentId.isEmpty())
            communityAssessment = new CommunityAssessments().getById(communityAssessmentId);
        }
        return communityAssessment;
    }

    public Assessment setCommunityAssessment(CommunityAssessment communityAssessment) {
        this.communityAssessment = communityAssessment;
        return this;
    }

    public String getId() {
        return id;
    }

    public Assessment setId(String id) {
        this.id = id;
        return this;
    }

    public String getAssessmentVersionId() {
        return assessmentVersionId;
    }

    public Assessment setAssessmentVersionId(String assessmentVersionId) {
        this.assessmentVersionId = assessmentVersionId;
        return this;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public Assessment setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
        return this;
    }

    public String getName() {
        return name;
    }

    public Assessment setName(String name) {
        this.name = name;
        return this;
    }

    public Assessment setIsExcludedFromDashboard(boolean isExcluded) {
        this.isExcludedFromDashboard = isExcluded;
        return this;
    }

    public boolean isExcludedFromDashboard() {
        return isExcludedFromDashboard;
    }

}
