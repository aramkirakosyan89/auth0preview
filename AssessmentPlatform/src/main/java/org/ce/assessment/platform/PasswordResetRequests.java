package org.ce.assessment.platform;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class PasswordResetRequests extends DbTable {



    public static class PasswordResetRequest {
        public final String token;
        public final String email;
        private final boolean expired;
        private final Calendar expiresAt;
        
        PasswordResetRequest(String token, String email, boolean expired, Date timestamp) {
            this.token = token;
            this.email = email;
            this.expired = expired;
            this.expiresAt = Calendar.getInstance();
            this.expiresAt.setTime(timestamp);
            this.expiresAt.add(Calendar.MINUTE, 30);
        }
    }
    
    private final static Logger log = LoggerFactory.getLogger(PasswordResetRequests.class);

    private final static String TABLE = "pw_resets";
    private final static String NAME = "password resets";

    private final static String SQL = "CREATE TABLE " + TABLE + "(" +
            "token character varying NOT NULL," +
            "\"timestamp\" timestamp without time zone DEFAULT now()," +
            "expired boolean DEFAULT false," +
            "email character varying," +
            "CONSTRAINT pwresets_pkey PRIMARY KEY (token)" +
            ") WITH ( OIDS=FALSE );";

    static boolean createDbTable(String dataSourceName) {
        return createDbTable(NAME, SQL, dataSourceName, log);
    }

    public PasswordResetRequests() {
        super(Platform.instance());
    }
    
    public String createNew(String email) {
        email = email.toLowerCase();
        String token = UUID.randomUUID().toString().replace("-","");        

        try (DbQuery query = query("INSERT INTO " + TABLE + " (token, email) VALUES ('" + token + "', '" + email + "');")) {
            query.execute();
        } 
        catch (SQLException | NamingException e) {
            log.error("Could not create a new password reset request for email " + email + ". " + e.toString());
            e.printStackTrace();
            return null;
        }
        return token;
    }
    
    public PasswordResetRequest getByToken(String token) {
        try (DbQuery query = query("select * from " + TABLE + " where token='" + token + "';")) {
            ResultSet results = query.getResults();
            if (results.next()) {
                return new PasswordResetRequest(
                        results.getString("token")
                        , results.getString("email")
                        , results.getBoolean("expired")
                        , results.getTimestamp("timestamp"));
            }            
        }
        catch (SQLException | NamingException e) {
            log.error("Could not retrieve token");
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public boolean isValid(String token) {
        PasswordResetRequest request = getByToken(token);
        if (request == null || request.expired) {
            return false;
        }
                                
        boolean expired = (request.expiresAt.compareTo(Calendar.getInstance()) < 0);
        if (expired) {
            expire(request);
        }
        return !expired;
    }

    public void expire(PasswordResetRequest request) {
        try (DbQuery query = query("UPDATE " + TABLE + " SET expired=true where token = '" + request.token + "';")) {
            query.execute();
        }
        catch (SQLException | NamingException e) {
            log.error("Could not expire token " + e.toString());
            e.printStackTrace();
        }
    }

}
