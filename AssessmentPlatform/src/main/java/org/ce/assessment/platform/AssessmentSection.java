package org.ce.assessment.platform;

import org.ce.assessment.limeapi.LimeCallException;
import org.ce.assessment.limeapi.LimeClient;
import org.ce.assessment.platform.dashboard.Dashboard;
import org.ce.assessment.platform.report.LetterScore;
import org.ce.assessment.platform.report.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class AssessmentSection {

    private final static Logger log = LoggerFactory.getLogger(AssessmentSection.class);

    private final String id;
    private final String assessmentId;
    private final String assessmentVersionSectionId;
    private final String token;
    private int score;
    private boolean isCompleted = false;

    private Assessment assessment;
    private final Function<String, Assessment> getAssessmentFunction;

    private AssessmentVersionSection versionSection;
    private final Function<String, AssessmentVersionSection> getAssessmentVersionSectionFunction;

    private Report report = null;

    public AssessmentSection(String id,
                             String assessmentId,
                             String assessmentVersionSectionId,
                             String token,
                             Function<String, Assessment> getAssessmentFunction,
                             Function<String, AssessmentVersionSection> getAssessmentVersionSectionFunction
                            )
    {
        this.id = id;
        this.assessmentId = assessmentId;
        this.assessmentVersionSectionId = assessmentVersionSectionId;
        this.token = token;
        this.getAssessmentVersionSectionFunction = getAssessmentVersionSectionFunction;
        this.getAssessmentFunction = getAssessmentFunction;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return getAssessmentVersionSection().getName();
    }

    public String getSurveyId() {
        return getAssessmentVersionSection().getLimeSurveyId();
    }

    public String getToken() {
        return token;
    }

    public String getAssessmentId() {
        return assessmentId;
    }


    public int getScore() {
        if (!isCompleted()) {
            return LetterScore.NOT_COMPLETED;
        }
        return score;
    }

    public AssessmentSection setScore(int score) {
        this.score = score;
        return this;
    }

    // TODO: reconsider lazy init
    public AssessmentVersionSection getAssessmentVersionSection() {
        if (versionSection == null) {
            Assessment assessment = getAssessment();
            AssessmentVersion assessmentVersion =
                    Platform.instance().getAssessmentVersions().getById(assessment.getAssessmentVersionId());
            if (assessmentVersion != null) {
                versionSection = assessmentVersion.getSections().get(assessmentVersionSectionId);
            }
        }
        if (versionSection == null) {
            log.error("could not get for " + this.assessmentVersionSectionId);
        }
        return versionSection;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public AssessmentSection setCompleted(boolean completed) {
        this.isCompleted = completed;
        return this;
    }



    public String getSurveyLink(boolean newAssessment) {
        if (token == null) {
            log.error("getting survey (" + getSurveyId() + ") link for assessment(" + id + ") with NULL token");
            return "javascript: alert('Something went wrong. Cannot get the link to the survey')'";
        }
        return LimeClient.LIME_URL + "/index.php/survey/index/sid/" + getSurveyId() + "/token/" + token
                + ((newAssessment)?"/lang/en/newtest/Y":"");
    }

    public void loadReport() {
        report = getAssessmentVersionSection().prepareReport();
        try {
            report.loadForToken(token);
        } catch (LimeCallException e) {
            log.error("Could not load report for assessment section " + this.id + ". " + e.toString() );
            return;
        }

        if (report.isSurveyCompleted()) {
            isCompleted = true;
            score = report.getScore();
            new AssessmentSections().update(this);
            new Dashboard().updateAssessmentSection(this);
        }
    }

    public Report getReport() {
        if (report == null) {
            loadReport();
        }
        return report;
    }

    public AssessmentSection setAssessment(Assessment assessment) {
        this.assessment = assessment;
        return this;
    }

    public Assessment getAssessment() {
        if (assessment == null) {
            assessment = getAssessmentFunction.apply(assessmentId);
        }
        return assessment;
    }

    public String getNextId() {
        String vId = getAssessmentVersionSection().getNextId();
        return new AssessmentSections().getAssessmentSectionIdByVersionSectionId(assessmentId, vId);
    }

    public String getPrevId() {
        String vId = getAssessmentVersionSection().getPrevId();
        return new AssessmentSections().getAssessmentSectionIdByVersionSectionId(assessmentId, vId);
    }

}
