package org.ce.assessment.platform;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class PlatformConfiguration {

    private final static Logger log = LoggerFactory.getLogger(PlatformConfiguration.class);



    private String dataSourceName;
    private MailerConfiguration mailerConfiguration;

    private static final String LIME_URL_PARAM = "org.ce.assessment.limeUrl";
    public final String limeUrl;

    private static final String LIME_USER_PARAM = "org.ce.assessment.limeUser";
    public final String limeUser;

    private static final String LIME_PASSWORD_PARAM = "org.ce.assessment.limePassword";
    public final String limePassword;

    private static final String PROFILE_SURVEY_ID_PARAM = "org.ce.assessment.orgProfileSurveyId";
    public final String profileSurveyId;

    private static final String HOST_PARAM = "org.ce.assessment.host";
    public final String host;

    private boolean isAuthEnabled = true;


    private PlatformConfiguration() {
        profileSurveyId = getContextParam(PROFILE_SURVEY_ID_PARAM);
        limeUrl = getContextParam(LIME_URL_PARAM);
        limeUser = getContextParam(LIME_USER_PARAM);
        limePassword = getContextParam(LIME_PASSWORD_PARAM);
        host = getContextParam(HOST_PARAM);
    }

    public String getDataSourceName() {
        return dataSourceName;
    }

    public boolean isAuthEnabled() {
        return isAuthEnabled;
    }


    public MailerConfiguration getMailerConfiguration() {
        return mailerConfiguration;
    }

    private String getContextParam(String paramName) {
        try {
            InitialContext cxt = new InitialContext();
            return (String) ((Context) cxt.lookup("java:/comp/env")).lookup(paramName);
        } catch (NamingException e) {
            log.error("Could not get environment entry " + paramName + "(should be specified in WEB-INF/web.xml.");
            e.printStackTrace();
            return null;
        }
    }


    public static class Builder {
        private final PlatformConfiguration configuration = new PlatformConfiguration();

        public Builder withDataSourceName(String dataSourceName) {
            configuration.dataSourceName = dataSourceName;
            return this;
        }

        public Builder withMailerConfiguration(MailerConfiguration mailerConfiguration) {
            configuration.mailerConfiguration = mailerConfiguration;
            return this;
        }

        public Builder setAuthEnabled(boolean isAuthEnabled) {
            configuration.isAuthEnabled = isAuthEnabled;
            return this;
        }

        public PlatformConfiguration build() {
            // TODO: validate configuration

            if (configuration.mailerConfiguration == null) {
                log.warn("no mailer configuration found - will not be able to send email");
            }

            return configuration;
        }


    }

}
