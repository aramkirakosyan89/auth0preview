package org.ce.assessment.platform.invites;

import org.ce.assessment.platform.Community;
import org.ce.assessment.userdb.Organization;

public abstract class CommunityMemberInvite extends Invite {

    public CommunityMemberInvite(String token, Invites.InviteType type, String communityId) {
        super(token, type);
        setCommunityId(communityId);
    }

    @Override
    public boolean accept() {
        if (getStatus() != Invites.InviteStatus.PENDING)
        {
            getLogger().warn("Trying to accept an invite which is not pending. Token: " + token);
            return false;
        }

        Community community = getCommunity();
        if (community == null) {
            getLogger().error("Could not get community " + getCommunityId() + " for invite " + token);
            return false;
        }

        Organization organization = getOrganization();
        if (organization == null) {
            getLogger().error("Could not get organization " + getOrganizationId() + " for invite " + token);
        }

        if (!community.members.join(organization)) {
            getLogger().error("Failed to accept invite " + token + ". Server error");
            return false;
        }

        return super.accept();
    }

}
