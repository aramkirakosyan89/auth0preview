package org.ce.assessment.platform;

public class CommunityAssessment {

    private String id;
    private String name;
    private String communityId;
    private Community community;
    private String assessmentVersionId;
    private AssessmentVersion assessmentVersion;
    private boolean isActivated;


    public String getId() {
        return (id == null) ? "null": id;
    }

    public CommunityAssessment setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CommunityAssessment setName(String name) {
        this.name = name;
        return this;
    }

    public String getCommunityId() {
        return communityId;
    }

    public CommunityAssessment setCommunityId(String communityId) {
        this.communityId = communityId;
        return this;
    }

    public Community getCommunity() {
        if (community == null) {
            community = new Communities().getById(communityId);
        }
        return community;
    }

    public CommunityAssessment setCommunity(Community community) {
        this.community = community;
        return this;
    }

    public String getAssessmentVersionId() {
        return assessmentVersionId;
    }

    public CommunityAssessment setAssessmentVersionId(String assessmentVersionId) {
        this.assessmentVersionId = assessmentVersionId;
        return this;
    }

    public AssessmentVersion getAssessmentVersion() {
        if (assessmentVersion == null) {
            assessmentVersion = Platform.instance().getAssessmentVersions().getById(assessmentVersionId);
        }
        return assessmentVersion;
    }

    public CommunityAssessment setAssessmentVersion(AssessmentVersion assessmentVersion) {
        this.assessmentVersion = assessmentVersion;
        return this;
    }

    public boolean isActivated() {
        return isActivated;
    }

    public CommunityAssessment setActivated(boolean activated) {
        isActivated = activated;
        return this;
    }
}
