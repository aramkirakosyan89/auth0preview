package org.ce.assessment.platform.report;

import org.ce.assessment.limeapi.Survey;

import java.util.ArrayList;
import java.util.List;

public class SevenElementsSection extends Section {

    private final String questionCode;

    private final List<Strategy> strategies = new ArrayList<>();
    private MultipleChoiceSector keyChallenges;
    private Sector keyOpportunities;
    private Sector notes;
    private Sector quant;

    SevenElementsSection(String name, String questionCode, Survey survey) {
        super(name, survey);
        this.questionCode = questionCode;
    }

    public SevenElementsSection withStrategy(String name, int n) {
        Strategy strategy = (Strategy) f.strategy(name, questionCode, n);
        strategies.add(strategy);
        return (SevenElementsSection) withSector(strategy);
    }

    public SevenElementsSection withKeyChallenges() {
        keyChallenges = new MultipleChoiceSector("Key Challenges", f.mcq(questionCode + "Ch"))
                .withExplanation(f.q(questionCode + "ChExp"))
                .hideUnselected();

        return (SevenElementsSection) withSector(keyChallenges);
    }

    public SevenElementsSection withKeyOpportunities() {
        keyOpportunities = new Sector("Key Opportunities").withDefinition(f.q(questionCode+"Opp"));
        return (SevenElementsSection) withSector(keyOpportunities);
    }

    public SevenElementsSection withNotes() {
        notes = new Sector("Notes").withDefinition(f.q(questionCode+"Notes"));
        return (SevenElementsSection) withSector(notes);
    }

    public SevenElementsSection withQuant() {
        quant = new Sector("% of products affected").withDefinition(f.radio(questionCode+"Quant"));
        return (SevenElementsSection) withSector(quant);
    }

    public List<Strategy> getStrategies() {
        return strategies;
    }

    public MultipleChoiceSector getKeyChallenges() {
        return keyChallenges;
    }

    public Sector getKeyOpportunities() {
        return keyOpportunities;
    }

    public Sector getNotes() {
        return notes;
    }

    public Sector getQuant() {
        return quant;
    }


}
