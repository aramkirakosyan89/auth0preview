package org.ce.assessment.platform;

import org.ce.assessment.userdb.Organization;
import org.ce.assessment.userdb.Organizations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class CommunityMembers extends Organizations {

    private final static Logger log = LoggerFactory.getLogger(CommunityMembers.class);

    private final static String NAME = "community_members";
    private final static String SQL =
            "CREATE TABLE public." + NAME + "\n" +
            "(\n" +
            "  community_id integer NOT NULL,\n" +
            "  organization_id integer NOT NULL,\n" +
            "  CONSTRAINT community_members_communities_id_fk FOREIGN KEY (community_id)\n" +
            "      REFERENCES public.communities (id) MATCH SIMPLE\n" +
            "      ON UPDATE NO ACTION ON DELETE NO ACTION,\n" +
            "  CONSTRAINT community_members_organizations_id_fk FOREIGN KEY (organization_id)\n" +
            "      REFERENCES public.organizations (id) MATCH SIMPLE\n" +
            "      ON UPDATE NO ACTION ON DELETE NO ACTION\n" +
            ")\n" +
            "WITH (\n" +
            "  OIDS=FALSE\n" +
            ");";

    public static boolean createDbTable(String dataSourceName) {
        return createDbTable(NAME, SQL, dataSourceName, log);
    }

    private final String communityId;
    private Community community;

    public CommunityMembers(Platform platform, String communityId) {
        super(platform);
        this.communityId = communityId;
    }

    public CommunityMembers(Community community) {
        super(Platform.instance());
        this.communityId = community.id;
        this.community = community;
    }

    public Community getCommunity() {
        if (community == null) {
            community = new Communities().getById(communityId);
        }
        return community;
    }

    protected String getSelectQuery() {
        return "select O.*" +
                "from " +
                "   community_members M " +
                "   inner join organizations O on M.organization_id = O.id" +
                " where M.community_id=" + communityId + "" +
                " order by O.name ;";
    }

    public List<Organization> getAll() {
        List<Organization> result = new ArrayList<>();

        try (DbQuery query = query(getSelectQuery())) {
            ResultSet res = query.getResults();
            while (res.next()) {
                Organization organization = fromDbRow(res);
                result.add(organization);
            }
        } catch (SQLException | NamingException e) {
            log.error("Could not load community members. " + e.toString());
            return null;
        }

        return result;
    }

    public boolean join(Organization organization) {
        String sql = "INSERT INTO " + NAME + " (community_id, organization_id) " +
                "VALUES (" + communityId + ", " + organization.id + ");";

        try (DbQuery q = query(sql)) {
            q.execute();
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error("Could not add " + organization.toString() + " to community id=" + communityId);
            return false;
        }

        // create community assessments for the new member
        for (CommunityAssessment communityAssessment : getCommunity().getAssessments()) {
            if (organization.createAssessment(communityAssessment) == null) {
                log.error("Could not create assessment (" + communityAssessment.getId() + ") for " +
                        "org("  + organization.id + ") upon joining community(" + communityId +")");
            };
        }


        return true;
    }

    public boolean leave(Organization organization) {
        boolean deletedCommunityAssessment = organization.removeCommunityAssessments(getCommunity());
        if (!deletedCommunityAssessment) {
            log.error("Could not delete all community assessments for org. So org not leaving the community");
            return false;
        }

        String sql = "delete from " + NAME +
                " where community_id = " + communityId + " and organization_id = " + organization.id;
        try(DbQuery query = query(sql)) {
            query.execute();
        } catch (SQLException | NamingException e) {
            log.error("Could not remove member organization from the community. " + e.toString());
            return false;
        }

        return true;
    }
}
