package org.ce.assessment.platform;

public class DuplicateAssessmentException extends Exception {

	public DuplicateAssessmentException(String message, Exception cause) {
		super(message, cause);
	}
	
	public DuplicateAssessmentException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2381521138419407414L;

}
