package org.ce.assessment.platform.invites;

import org.ce.assessment.platform.Communities;
import org.ce.assessment.platform.Community;
import org.ce.assessment.platform.Platform;
import org.ce.assessment.userdb.Organization;
import org.ce.assessment.userdb.Organizations;
import org.ce.assessment.userdb.User;
import org.slf4j.Logger;

// TODO: enable lazy loading for Organization and Community

public abstract class Invite {

    public final String token;
    public final Invites.InviteType type;
    private boolean asAdmin = false;
    private Invites.InviteStatus status = Invites.InviteStatus.PENDING;
    private String email;


    private Organization organization;
    private String organizationId;

    private Community community;
    private String communityId;

    private User registeredUser;

    public Invite(String token, Invites.InviteType type) {
        this.token = token;
        this.type = type;
    }

    public boolean asAdmin() {
        return asAdmin;
    }

    public Invite setAsAdmin(boolean asAdmin) {
        this.asAdmin = asAdmin;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Invite setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getCommunityId() {
        return communityId;
    }

    public String getCommunityIdString() {
        return  (communityId == null)?"null":communityId;
    }

    public Invite setCommunityId(String communityId) {
        this.communityId = communityId;
        return this;
    }

    public Invite setCommunity(Community community) {
        this.community = community;
        this.communityId = community.id;
        return this;
    }

    public Community getCommunity() {
        if (community == null) {
            if (communityId == null) {
                return null;
            }
            community = new Communities().getById(communityId);
        }
        return community;
    }

    public User getRegisteredUser() {
        return registeredUser;
    }


    public Invite setRegisteredUser(User user) {
        this.registeredUser = user;
        return this;
    }

    public Invite setStatus(Invites.InviteStatus status) {
        this.status = status;
        return this;
    }

    public Invites.InviteStatus getStatus() {
        return status;
    }

    public Organization getOrganization() {
        if (organization == null) {
            if (organizationId == null) {
                return null;
            }
            organization = new Organizations().getById(organizationId);
        }
        return organization;
    }

    public Invite setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
        return this;
    }

    public Invite setOrganization(Organization organization) {
        this.organizationId = organization.id;
        this.organization = organization;
        return this;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public String getOrganizationIdString() {
        return (organizationId == null)?"null":organizationId;
    }

    public String getInvitee() {
        if (type == Invites.InviteType.COMMUNITY_BY_ORG) {
            return getOrganization().getName();
        } else {
            return email;
        }
    }


    public boolean accept() {
        setStatus(Invites.InviteStatus.ACCEPTED);
        return new Invites().setInviteStatus(token, Invites.InviteStatus.ACCEPTED);
    };

    public boolean reject() {
        return new Invites().setInviteStatus(token, Invites.InviteStatus.REJECTED);
    }

    protected abstract Logger getLogger();

    public abstract boolean sendInvitationEmails(String host);

    protected boolean sendEmail(String email, String subject, String msg) {
        if (!Platform.instance().getMailer().sendMsg(email, subject, msg)) {
            getLogger().error("Could not send invitation email to " + email);
            return false;
        }
        return true;
    }

    protected String param(String param, String value) {
        return param + "="  + value + "&";
    }
}