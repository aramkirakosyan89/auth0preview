package org.ce.assessment.platform.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class LetterScore {

    private final static Logger log = LoggerFactory.getLogger(LetterScore.class);

    public final static int NOT_APPLICABLE = -1;
    public final static int NOT_COMPLETED = -2;


    private final int rangeFrom;
    private final int rangeTo;
    public final String letter;
    public final String color;
    public final String cssClass;
    private int score = NOT_COMPLETED;


    private LetterScore(int rangeFrom, int rangeTo, String letter, String color, String cssClass) {
        this.rangeFrom = rangeFrom;
        this.rangeTo = rangeTo;
        this.letter = letter;
        this.color = color;
        this.cssClass = cssClass;
    }

    private boolean appliesToScore(int score) {
        return score >= rangeFrom && score < rangeTo;
    }

    public static final List<LetterScore> scoreLetters = init();

    private static final List<LetterScore> init() {
        List<LetterScore> scores = new ArrayList<>();
        scores.add(new LetterScore(NOT_COMPLETED, NOT_APPLICABLE, "", "#ddd", "score-none"));
        scores.add(new LetterScore(NOT_APPLICABLE, 0, "N/A", "#ddd", "score-NA"));
        scores.add(new LetterScore(0, 10, "E", "red", "score-E"));
        scores.add(new LetterScore(10, 30, "D", "red", "score-D"));
        scores.add(new LetterScore(30, 50, "C", "red", "score-C"));
        scores.add(new LetterScore(50, 70, "B", "red", "score-B"));
        scores.add(new LetterScore(70, 90, "A", "red", "score-A"));
        scores.add(new LetterScore(90, 101, "A+", "red", "score-Aplus"));
        return scores;
    }

    public static LetterScore getForScore(int score) {
        for (LetterScore ls : scoreLetters)  {
            if (ls.appliesToScore(score)) {
                ls.score = score;
                return ls;
            }
        }
        log.error("did not find a letter for score " + score + ". Returning 'unknown'");
        return new LetterScore(NOT_COMPLETED, NOT_APPLICABLE, "unknown", "#ddd", "score-NA");
    }

    public int getScore() {
        return score;
    }

    public String getScoreTitle() {
        if (score == NOT_APPLICABLE) {
            return "not applicable";
        } else if (score == NOT_COMPLETED) {
            return "no score";
        }
        return "" + score + "%";
    }
}
