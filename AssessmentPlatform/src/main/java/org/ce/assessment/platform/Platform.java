package org.ce.assessment.platform;

import org.ce.assessment.platform.assessmentVersions.AssessmentVersionsCoded;
import org.ce.assessment.platform.invites.Invites;
import org.ce.assessment.userdb.UserDb;

public abstract class Platform {

    public static final String LIME_DATA_SOURCE_NAME = "jdbc/limedb";

    public final PlatformConfiguration configuration;
    private Invites invites = null;

    private AssessmentVersions assessmentVersions = null;


    protected AuthManager authManager = null;
    private Mailer mailer = null;

    private UserDb userDb = null;

    protected Platform(PlatformConfiguration configuration) {
        this.configuration = configuration;
    }

    public PlatformConfiguration getConfiguration() {
        return configuration;
    }

    public AssessmentVersions getAssessmentVersions() {
        if (assessmentVersions == null) {
            assessmentVersions = new AssessmentVersionsCoded();
        }
        return assessmentVersions;
    }

    public UserDb getUserDb() {
        if (userDb == null) {
            userDb = new UserDb(this);
        }
        return userDb;
    }
    public abstract AuthManager getAuthManager();

    public Mailer getMailer() {
        if (mailer == null) {
            mailer = new Mailer(configuration.getMailerConfiguration());
        }
        return mailer;
    }

    public Invites getInvites() {
        if (invites == null) {
            invites = new Invites(this);
        }
        return invites;
    }


    private static Platform platform;
    public static Platform instance() {
        if (platform == null) {
            final PlatformConfiguration CONFIGURATION = new PlatformConfiguration.Builder()
                    .withDataSourceName("jdbc/cloud")
                    .withMailerConfiguration(
                            MailerConfiguration.getSparkPostBuilder()
                                    .withFromAddress("no-reply@circle-assessment.com")
                                    .withFromName("Circle Assessment")
                                    .build()
                    )
                    .build();

            platform = new Platform(CONFIGURATION) {
                @Override
                public AuthManager getAuthManager() {
                    return new AuthManager(this) {
                    };
                }
            };
        }
        return platform;
    }
}
