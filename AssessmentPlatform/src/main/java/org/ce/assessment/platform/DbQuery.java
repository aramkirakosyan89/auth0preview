package org.ce.assessment.platform;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.io.Closeable;
import java.sql.*;

public class DbQuery implements Closeable {

	private static final Logger log = LoggerFactory.getLogger(DbQuery.class);
	
	private Connection con;
	private Statement st;
	private final String sql;

	private final String dataSourceName;

	public DbQuery(String dataSourceName, String sql) {
		this.dataSourceName = dataSourceName;
		this.sql = sql;
		log.debug("New query: " + sql);
	}
	
	private void initStatement() throws NamingException, SQLException {
	    log.debug("trying to initialize statement");
		InitialContext cxt = new InitialContext();			
		DataSource ds = (DataSource) cxt.lookup( "java:/comp/env/" + dataSourceName);
		con = ds.getConnection();
		st = con.createStatement();
		log.debug("initialized statement");
	}
	
	public ResultSet getResults() throws SQLException, NamingException {
		initStatement();
		log.debug("trying to get results");
		return st.executeQuery(sql);
	}	
	
	public void execute() throws SQLException, NamingException {		
		initStatement();
		st.execute(sql);
	}

	public PreparedStatement getPreparedStatement() throws SQLException, NamingException {
	    initStatement();
		return con.prepareStatement(sql);
    }
	
	public void close() {
		try {
			if (st != null) {
				st.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			log.error("Could not close the query '" + sql + "'." + e.toString());
			e.printStackTrace();
		}
	}	
	
}
