package org.ce.assessment.platform;

import org.slf4j.Logger;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract  class DbTableWithId<T> extends DbTable {

    DbTableWithId(Platform platform) {
        super(platform);
    }

    protected abstract T fromDbRow(ResultSet rs) throws SQLException;
    protected abstract String table();
    protected abstract String entity();
    protected abstract Logger log();

    protected String entities() {
        return entity()+"s";
    }


    public List<T> getAll() {
        return getAll("");
    }

    public List<T> getAll(String params) {
        List<T> all = new ArrayList<>();

        String sql = "select * from " + table() + " " + params + " order by id; ";
        try (final DbQuery query = query(sql);
             final ResultSet rs = query.getResults())
        {
            while (rs.next())  {
                T t = fromDbRow(rs);
                all.add(t);
            }
        } catch (SQLException | NamingException e) {
            log().error("Could not get all " + entities() +" from the database. " + e.toString());
            e.printStackTrace();
        }
        return all;
    }

    public T getById(String id) {
        String sql = "select * from " + table() + " where id=" + id;
        try (DbQuery query = query(sql);
             ResultSet rs = query.getResults()) {
            if (rs.next()) {
                return fromDbRow(rs);
            } else {
                log().warn("No " + entity() + " version with id=" + id);
            }
        } catch (SQLException | NamingException e) {
            log().error("Could not get " + entity() + " by id=" + id + " from the database. " + e.toString());
            e.printStackTrace();
        }
        return null;
    }

    private String addReturningId(String sql) {
        if (sql.endsWith(";")) {
            sql = sql.substring(0, sql.length()-1);
        }

        if (!sql.endsWith("returning id")) {

            sql = sql + "returning id";
        }
        return sql + ";";
    }

    /***
     *
      * @param sql - sql insert query, that must have "returning id;" in the end
     * @return newly created entity, null if creation failed
     */
    T createWithSql(String sql) {

        sql = addReturningId(sql);

        try (DbQuery query = query(sql)) {
            ResultSet rs = query.getResults();
            if (rs.next()) {
                String id = rs.getString("id");
                return getById(id);
            }
        } catch (SQLException | NamingException e) {
            log().error("Could not create a new " + entity() + " in the database. " + e.toString());
            e.printStackTrace();
        }
        return null;
    }

    public boolean delete(String id) {
        String sql = "delete from " + table() + " where id=" + id;
        try (DbQuery query = query(sql)) {
            query.execute();
        } catch (SQLException | NamingException e) {
            log().error("Could not delete " + entity() + " with id=" + id + ". " + e.toString());
            e.printStackTrace();
            return false;
        }
        return true;
        // TODO: return false if not found?
    }




}
