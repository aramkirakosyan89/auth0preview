package org.ce.assessment.platform;

import com.google.gson.Gson;
import org.postgresql.util.PGobject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import static java.sql.Types.VARCHAR;


/**
 *  Database controller for notes
 */
public class AssessmentNotesTable extends DbTable {

    private final static Logger log = LoggerFactory.getLogger(AssessmentNotesTable.class);

    private final static String NAME = "notes";
    private final static String SQL =
            "CREATE TABLE public.notes ( assessmentid integer NOT NULL, notes json );" +
                    "ALTER TABLE public.notes OWNER TO webapp;";

    AssessmentNotesTable(Platform platform) {
        super(platform);
    }

    public AssessmentNotes getForAssessment(long assessmentId) {

        String sql = "select  * from notes where assessmentid=" + assessmentId;
        try (DbQuery query = query(sql)) {
            ResultSet res = query.getResults();
            if (res.next()) {
                String json = res.getString("notes");
                if (json !=null) {
                    AssessmentNotes notes = new Gson().fromJson(json, AssessmentNotes.class);
                    return notes;
                }
            }
        } catch (SQLException | NamingException e) {
            log.error("Could not get notes for assignment id=" + assessmentId + " from the database. " + e.toString());
        }
        return new AssessmentNotes().setAssessmentId(assessmentId);
    }

    public boolean update(AssessmentNotes notes) {
        String sql = "INSERT INTO notes (assessmentid, notes) VALUES (?, to_json(?::json)) " +
                "ON CONFLICT (assessmentid) DO UPDATE SET notes = excluded.notes;";

        try (DbQuery query = query(sql)) {
            String json = new Gson().toJson(notes);

            PGobject jsonObject = new PGobject();
            jsonObject.setValue(json);
            jsonObject.setType("json");

            PreparedStatement ps = query.getPreparedStatement();
            ps.setInt(1, (int) notes.getAssessmentId());
            ps.setObject(2, jsonObject, VARCHAR);
            ps.execute();
        } catch (SQLException | NamingException e) {
            log.error("Could not update notes for assessment id=" + notes.getAssessmentId() + ". " + e.toString());
            return false;
        }
        return true;
    }

    public static boolean createDbTable(String dataSourceName) {
        return createDbTable(NAME, SQL, dataSourceName, log);
    }
}
