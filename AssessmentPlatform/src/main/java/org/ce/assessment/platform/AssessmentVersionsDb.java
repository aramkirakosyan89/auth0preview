package org.ce.assessment.platform;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public class AssessmentVersionsDb extends DbTableWithId<AssessmentVersion> implements AssessmentVersions {

    private final static Logger log = LoggerFactory.getLogger(AssessmentVersionsDb.class);

    private final String ASSESSMENT_VERSIONS = "assessment_versions";

    public AssessmentVersionsDb(Platform platform) {
        super(platform);
    }

    public AssessmentVersionsDb() {
        super(Platform.instance());
    }

    @Override
    protected Logger log() {
        return log;
    }

    @Override
    protected String table() {
        return ASSESSMENT_VERSIONS;
    }

    @Override
    protected String entities() {
        return "assessment versions";
    }

    @Override
    protected String entity() {
        return "assessment version";
    }

    protected AssessmentVersion fromDbRow(ResultSet rs) throws SQLException {
        final String id = rs.getString("id");
        final String name = rs.getString("name");
        final boolean isActivated = rs.getBoolean("activated");
        List<AssessmentVersionSection> sections = new AssessmentVersionSectionsDb().getForAssessmentVersion(id);

        return new AssessmentVersion(id, name).withSections(sections).setActivated(isActivated);
    }

    public AssessmentVersion create(String name) {
        String sql = "insert into " + ASSESSMENT_VERSIONS + " (name) values ('" + name + "') returning id;";
        return createWithSql(sql);
    }

    public boolean delete(AssessmentVersion assessmentVersion) {
        return delete(assessmentVersion.id);
    }

    public boolean activate(AssessmentVersion assessmentVersion) {
        String sql = "update " + table() + " set activated = TRUE, date_activated = now() where id = " + assessmentVersion.getId();
        try (DbQuery query = query(sql)) {
            query.execute();
            assessmentVersion.setActivated(true);
        } catch (SQLException | NamingException e) {
            log.error("Could not activate assessment version " + assessmentVersion.toString());
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
