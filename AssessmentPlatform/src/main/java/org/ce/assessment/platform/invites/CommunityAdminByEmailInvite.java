package org.ce.assessment.platform.invites;

import org.ce.assessment.platform.Communities;
import org.ce.assessment.platform.Community;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.ce.assessment.platform.api.AbstractApi.P_INVITE_TOKEN;

public class CommunityAdminByEmailInvite extends Invite {

    private final static Logger log = LoggerFactory.getLogger(CommunityAdminByEmailInvite.class);

    public CommunityAdminByEmailInvite(String token, String communityId, String email) {
        super(token, Invites.InviteType.COMMUNITY_BY_EMAIL);
        setAsAdmin(true);
        setCommunityId(communityId);
        setEmail(email);
    }

    @Override
    public boolean sendInvitationEmails(String host) {
        final Community community = getCommunity();
        if (community == null) {
            getLogger().error("Community("+getCommunityId()+") from invite(" + token + ") does not exist");
            return false;
        }

        String emailSubject = "Create " + community.getName() + " on Circle Assessment";
        String emailMsg =
                "You are invited to create " + community.getName()
                        + " community on the Circle Assessment platform<br>"
                        + "<a href=\"" + host + "register.jsp?"
                        + param(P_INVITE_TOKEN, token)
                        + "\">Register here</a>";

        return sendEmail(getEmail(), emailSubject, emailMsg);
    }

    @Override
    public boolean accept() {
        boolean success = new Communities().setAdminOrganization(getCommunityId(), getOrganizationId());
        if (!success) {
            log.error("Could not accept an invite with token: " + token);
            return false;
        }

        return super.accept();
    }

    @Override
    protected Logger getLogger() {
        return log;
    }
}
