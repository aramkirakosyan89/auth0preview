package org.ce.assessment.platform.report;

import org.ce.assessment.limeapi.question.TextQuestion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.TreeMap;

/**
 * A multi-tier sector has different definitions depending on the score
 */
public class MultiTierSector extends ScoreSector {

    private final static Logger log = LoggerFactory.getLogger(MultiTierSector.class);

    private TreeMap<Integer, TextQuestion> definitions = new TreeMap<>();

    public MultiTierSector(String name, TextQuestion score) {
        super(name, score);
    }

    /**
    The highest tier must have upper limit = 100
     */
    public MultiTierSector withDefinition(int upperLimit, TextQuestion definition) {
        definitions.put(upperLimit, definition);
        return this;
    }

    @Override
    public String getDefinition() {
        int score = getScore();
        Map.Entry<Integer, TextQuestion> entry = definitions.ceilingEntry(score);
        if (entry == null) {
            log.warn("No tier found for score " + score + " in sector " + name + ". Will return default definition");
            return super.getDefinition();
        } else {
            return entry.getValue().value();
        }
    }
}

