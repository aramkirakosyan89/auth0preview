package org.ce.assessment.platform.auth0;

import com.google.gson.Gson;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.io.UnsupportedEncodingException;

public class Auth0Client {

    private Auth0Config config;

    public Auth0Client(Auth0Config config) {
        this.config = config;
    }

    /**
     * Makes http call to Auth0(/oauth/token) endpoint with code to get Access Token.
     */
    public String requestAccessToken(HttpServletRequest request, String code) throws UnsupportedEncodingException {
        /* prepare data for request */
        String domain = config.getDomain();
        String clientId = config.getClientId();
        String clientSecret = config.getClientSecret();
        String redirectUri = request.getScheme() + "://" + request.getServerName() +
                ":" + request.getServerPort() + "/api/callback/";
        String targetUri = "https://" + domain + "/oauth/token";

        /* Create request body */
        Auth0Client.AuthRequest authRequest = new Auth0Client.AuthRequest("authorization_code", clientId, clientSecret,
                code, redirectUri);
        Gson gson = new Gson();
        String json = gson.toJson(authRequest);

        /* make request to Auth0 */
        String response = ClientBuilder.newClient()
                .target(targetUri)
                .request(MediaType.APPLICATION_JSON)
                .header("content-type", "application/json")
                .post(Entity.json(json))
                .readEntity(String.class);

        /* parse response */
        Auth0Client.AuthResponse resp = gson.fromJson(response, Auth0Client.AuthResponse.class);

        return resp.access_token;
    }

    /**
     * Makes http call to Auth0(/userinfo) endpoint with access token to get user email.
     */
    public String requestUserInfo(String accessToken) {
        String targetUri = "https://" + config.getDomain() + "/userinfo";

        /* make request to Auth0 */
        String response = ClientBuilder.newClient()
                .target(targetUri)
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + accessToken)
                .get().readEntity(String.class);

        /* parse response */
        Gson gson = new Gson();
        Auth0Client.UserInfo info = gson.fromJson(response, Auth0Client.UserInfo.class);

        return info.email;
    }

    private static class UserInfo {
        String email;
    }

    private static class AuthRequest {
        String grant_type;
        String client_id;
        String client_secret;
        String code;
        String redirect_uri;

        AuthRequest(String grant_type, String client_id,
                    String client_secret, String code, String redirect_uri) {
            this.grant_type = grant_type;
            this.client_id = client_id;
            this.client_secret = client_secret;
            this.code = code;
            this.redirect_uri = redirect_uri;
        }

    }

    private static class AuthResponse {
        String access_token;
    }

}
