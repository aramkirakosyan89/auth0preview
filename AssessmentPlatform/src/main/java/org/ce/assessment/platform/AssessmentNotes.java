package org.ce.assessment.platform;

/**
 * Created by Tair on 6/1/2017.
 */

public class AssessmentNotes {
    private long assessmentId = -1;
    private String adminNotes = "";
//    private boolean isAdminNotePrivate = true;
    private String clientNotes = "";
    private String privateAdminNotes = "";

    public String getAdminNotes() {
        return adminNotes;
    }

    public AssessmentNotes setAdminNotes(String adminNotes) {
        this.adminNotes = adminNotes;
        return this;
    }

    public String getPrivateAdminNotes() {
        return privateAdminNotes;
    }

    public AssessmentNotes setPrivateAdminNotes(String privateAdminNotes) {
        this.privateAdminNotes = privateAdminNotes;
        return this;
    }

//    public boolean isAdminNotePrivate() {
//        return isAdminNotePrivate;
//    }

//    public AssessmentNotes setAdminNotePrivate(boolean adminNotePrivate) {
//        isAdminNotePrivate = adminNotePrivate;
//        return this;
//    }

    public String getClientNotes() {
        return clientNotes;
    }

    public AssessmentNotes setClientNotes(String clientNotes) {
        this.clientNotes = clientNotes;
        return this;
    }

    public long getAssessmentId() {
        return assessmentId;
    }

    public AssessmentNotes setAssessmentId(long assessmentId) {
        this.assessmentId = assessmentId;
        return this;
    }
}
