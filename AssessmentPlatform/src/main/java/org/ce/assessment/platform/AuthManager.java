package org.ce.assessment.platform;

import org.ce.assessment.userdb.User;
import org.ce.assessment.userdb.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthManager {



    public static class InputFields {
        public final String EMAIL = "email";
        public final String PASSWORD = "password";
        public final String PW1 = "password1";
        public final String PW2 = "password2";
        public final String NAME = "name";
        public final String LAST_NAME = "lastname";
        public final String JOB_TITLE = "jobtitle";
        public final String PHONE = "phone";
        public final String TOKEN = "token";
        public final String ORGANIZATION = "org";
    }
    public static final InputFields fields = new InputFields();

    public enum Result {
        SIGNED_IN("Signed In")
        , UNKNOWN_EMAIL("E-mail is not recognized")
        , WRONG_PASSWORD("Wrong password")
        , REGISTERED("Registered")
        , EMAIL_EXISTS("User with this e-mail is already registered")
        , PASSWORD_EMPTY("Password cannot be empty")
        , PASSWORDS_DO_NOT_MATCH("Passwords do not match")
        , UNINVITED("Your email has not been invited to register")
        , PASSWORD_CHANGED("Password changed")
        , RESET_SENT("Sent a link to reset password")
        , COULDNT_EMAIL("Could not send email")
        , INVITE_SENT("Invitation sent")
        , INVALID_PASSWORD_RESET_REQUEST("Invalid password reset request")
        , ERROR("Server error. Fixer monkeys have been notified. ")
        ;

        public final String msg;

        Result(String msg) {
            this.msg = msg;
        }
    }

    private final static Logger log = LoggerFactory.getLogger(AuthManager.class);

    protected final Platform platform;
    protected final Users users;
    private final PasswordResetRequests passwordResetRequests;

    private final String loginPage;
    private final String adminHome;
    private final String communityAdminHome;
    private final String orgHome;


    public AuthManager(Platform platform) {
        this.platform = platform;
        users = new Users();
        passwordResetRequests = new PasswordResetRequests();

        //TODO: move this to PlatformConfiguration
        loginPage = "index.jsp";
        adminHome = "communities.jsp";
        communityAdminHome = "community.jsp?id=";
        orgHome = "organization.jsp?id=";
    }


    public AuthManager() {
        this(Platform.instance());
    }

    public User getAuthedUser(HttpServletRequest request) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            log.debug("getAuthedUser: Session does not exist");
            return null;
        }
        User user = (User) session.getAttribute("userObject");
        if (user == null) {
            log.debug("getAuthedUser: No authenticated user");
            return null;
        }
        log.debug("getAuthedUser: Authenticated user " + user.getName() + ", " + user.getEmail());
        return user;
    }


    public User getAuthedUserWithException(HttpServletRequest request) throws ServletException, IOException, NotSignedInException {
        User user = getAuthedUser(request);
        if (user == null) {
            throw new NotSignedInException();
        }
        return user;
    }

    // TODO: return a boolean value
    public void confirmAuthForPage(HttpServletRequest request, String pageClientId)
            throws NotAuthorizedException, ServletException, IOException, NotSignedInException {
        if (!platform.getConfiguration().isAuthEnabled())
            return;

        User user = getAuthedUserWithException(request);
        if (!user.isAdmin() && !user.getOrganizationId().equals(pageClientId)) {
            log.warn("confirmAuthForPage: user " + user.getName() + " is not authorized for page of client with id "
                    + pageClientId);
            throw new NotAuthorizedException();
        }
        log.debug("confirmAuthForPage: user " + user.getName() + " is authorized for page of client with id"
                + pageClientId);
    }


    public boolean canSeeCommunity(HttpServletRequest request, Community community) throws ServletException, IOException, NotSignedInException {
        if (!platform.getConfiguration().isAuthEnabled()) {
            return true;
        }

        User user = getAuthedUserWithException(request);

        if (user.isAdmin()) {
            return true;
        }
        if ((community.getAdminOrganization().id).equals(user.getOrganizationId())) {
            return true;
        }

        return false;
    }

    // TODO: return a boolean
    public void confirmAuthAsAdmin(HttpServletRequest request)
            throws ServletException, IOException, NotSignedInException, NotAuthorizedException {
        if (!platform.getConfiguration().isAuthEnabled())
            return;

        User user = getAuthedUserWithException(request);
        if (!user.isAdmin()) {
            log.warn("confirmAuthAsAdmin: user " + user.getName() + " is not an admin and should not be ");
            throw new NotAuthorizedException();
        }
        log.debug("confirmAuthAsAdmin: user " + user.getName() + " is an admin");
    }

    public String getUserHome(HttpServletRequest request) throws ServletException, IOException {
        if (!platform.getConfiguration().isAuthEnabled())
            return getAdminHome();

        log.debug("getting user home");
        try {
            User user = getAuthedUserWithException(request);
            if (user.isAdmin()) {
                return getAdminHome();
            }
            Community community = new Communities().getByAdminOrganizationId(user.getOrganizationId());
            if (community != null) {
                return getCommunityAdminHome(community);
            }
            return getUserHome(user);
        }
        catch (NotSignedInException e) {
            log.debug("not signed in");
            return getLoginHome();
        }
    }



    protected String getAdminHome() {
        return adminHome;
    }
    protected String getUserHome(User user) {
        return orgHome + user.getOrganizationId();
    }
    private String getCommunityAdminHome(Community community) {
        return communityAdminHome + community.id;
    }
    private String getLoginHome() {
        return loginPage;
    }

    public void signOut(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            if (!session.isNew() && session.getAttribute("userObject") != null) {
                session.removeAttribute("userObject");
                session.removeAttribute("user");
            }
            session.invalidate();
        }
    }

    public Result signIn(HttpServletRequest request, String email, String password) {
        request.getSession().removeAttribute("userObject");
        request.getSession().removeAttribute("user");

        User user = users.getByEmail(email);
        if (user == null) {
            log.info("Sign in: unknown email: " + email);
            return Result.UNKNOWN_EMAIL;
        }
        else {
            if (user.checkPassword(password)) {
                log.info("Sign in: " + email);
                request.getSession().setMaxInactiveInterval(30000);
                request.getSession().setAttribute("userObject", user);
                request.getSession().setAttribute("user", user.getEmail()) ;
                return Result.SIGNED_IN;
            }
            else {
                log.info("Sing in: wrong password for " + email);
                return Result.WRONG_PASSWORD;
            }
        }
    }

    public Result resetPassword(String resetToken, String password) {
        PasswordResetRequests resetRequests = new PasswordResetRequests();
        PasswordResetRequests.PasswordResetRequest r = resetRequests.getByToken(resetToken);

        if (r != null) {
            Users users = new Users();
            User user = users.getByEmail(r.email);
            if (user != null) {
                user.setPassword(password);
                if (users.update(user)) {
                    resetRequests.expire(r);
                    return Result.PASSWORD_CHANGED;
                } else {
                    log.error("Could not set password for token " + resetToken);
                    return Result.ERROR;
                }
            }
            else {
                return Result.UNKNOWN_EMAIL;
            }
        } else {
            return Result.INVALID_PASSWORD_RESET_REQUEST;
        }
    }

    public Result sendPasswordReset(String email) {

        User user = users.getByEmail(email);
        if (user == null) {
            return Result.UNKNOWN_EMAIL;
        } 
        String token = passwordResetRequests.createNew(email);
        if (token == null) {
            return Result.ERROR;
        }

        // TODO: decouple email msg
        Mailer mailer = platform.getMailer();
        String resetUrl = platform.getConfiguration().host + "resetpassword.jsp?token="+token;
        String msg = "Follow the link to reset your password to Circle Assessment<br><a href=\"" + resetUrl + "\">" + resetUrl + "</a>";
        
        if (mailer.sendMsg(email, "Password Reset", msg)) {
            return Result.RESET_SENT;
        } else {
            log.error("Could not send password reset email to " + email);
            return Result.COULDNT_EMAIL;
        }
    }

}

