package org.ce.assessment.platform.report;

import org.ce.assessment.limeapi.question.ConditionQuestion;

public abstract class ConditionalElement<T> {

    private ConditionQuestion conditionQuestion;

    public boolean isEnabled() {
        return conditionQuestion == null || conditionQuestion.isTrue();
    }

    T withCondition(ConditionQuestion conditionQuestion) {
        this.conditionQuestion = conditionQuestion;
        return (T) this;
    }

}
