package org.ce.assessment.platform.api;

import org.ce.assessment.platform.Communities;
import org.ce.assessment.platform.Community;
import org.ce.assessment.platform.invites.*;
import org.ce.assessment.userdb.Organization;
import org.ce.assessment.userdb.Organizations;
import org.ce.assessment.userdb.User;
import org.ce.assessment.userdb.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("communities")
public class CommunitiesAPI extends AbstractApi {

    private final static Logger log = LoggerFactory.getLogger(CommunitiesAPI.class);

    @POST @Path("create")
    public Response createCommunity(@FormParam("name") String name) {
        final boolean created = new Communities().create(name);
        if (created) {
            return Response.ok().build();
        }
        return Response.serverError().entity("could not create community due to a server error").build();
    }

    @PUT @Path("{id}")
    @Consumes("application/x-www-form-urlencoded")
    public Response renameCommunity(@PathParam("id") String id, @FormParam("name") String name) {
        Community community = new Communities().getById(id);
        if (community == null) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Unknown community with id " + id).build();
        }
        if (name == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity("New name is not provided").build();
        }
        if (new Communities().update(community.setName(name))) {
            return Response.ok().build();
        } else {
            return Response.serverError().build();
        }
    }

//    TODO:
//    @DELETE @Path("{id}")
//    public Response deleteCommunity() {
//        Community community
//    }

    @POST @Path("{id}/assessments/create")
    public Response newAssessment(@PathParam("id") String communityId,
                                  @FormParam("assessmentVersionId") String assessmentVerisonId,
                                  @FormParam("name") String name)
    {
        Community community = new Communities().getById(communityId);
        if (community == null) {
            log.warn("requested unknown community id " + communityId);
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Unknown community with id " + communityId).build();
        }

        if (!community.createAssessment(assessmentVerisonId, name)) {
            return Response.serverError().entity("Could not create an assessment for the community").build();
        }

        return Response.ok().build();
    }

    @POST @Path("{id}/assessments/{assessmentId}/delete")
    public Response deleteAssessment(@PathParam("id") String communityId,
                                     @PathParam("assessmentId") String assessmentId)
    {
        Community community = new Communities().getById(communityId);
        if (community == null) {
            log.warn("requested unknown community id " + communityId);
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Unknown community with id " + communityId).build();
        }

        if (!community.deleteAssessment(assessmentId)) {
            return Response.serverError().entity("Could not delete community assessment").build();
        }

        return Response.ok().build();
    }

    @POST @Path("{id}/admin")
    public Response setAdmin(@PathParam("id") String communityId,
                             @DefaultValue("") @FormParam(P_INVITE_TYPE) String inviteType,
                             @FormParam(P_ORG_ID) String organizationId,
                             @FormParam(P_EMAIL) String email)
    {
        Communities communities = new Communities();
        final Community community = communities.getById(communityId);
        if (community == null) {
            String msg = "Unknown community id(" + communityId + ")";
            log.error(msg);
            return Response.status(Response.Status.BAD_REQUEST).entity(msg).build();
        }

        switch (inviteType) {
            case V_INVITE_EXISTING:
                final Organization organization = new Organizations().getById(organizationId);
                if (organization == null) {
                    String msg = "Unknown organization id(" + organizationId + ")";
                    log.error(msg);
                    return Response.status(Response.Status.BAD_REQUEST).entity(msg).build();
                }
                community.setAdminOrganization(organization);
                if (!communities.update(community)) {
                    return Response.serverError().entity("Could not set admin").build();
                }
                return Response.ok().build();

            case V_INVITE_BY_EMAIL:
                Invites invites = new Invites();
                CommunityAdminByEmailInvite invite = invites.newCommunityAdminByEmail(communityId, email);

                if (!invite.sendInvitationEmails(getHost())) {
                    invites.deleteInvite(invite.token);
                    return Response.serverError().entity("Could not send invitation email").build();
                }
                return Response.ok().build();

            default:
                log.warn("Unknown invitation type " + inviteType);
                return Response.status(Response.Status.BAD_REQUEST).entity("Unknown invitation type: " + inviteType).build();
        }
    }

    @POST @Path("{id}/members/create")
    public Response createMember(@PathParam("id") String communityId,
                                 @FormParam("name") String name,
                                 @FormParam("email") String email)
    {

        final Community community = new Communities().getById(communityId);
        if (community == null) {
            log.error("Could not invite organization (" + name+ ", " + email + ") " +
                    "to unknown community id(" + communityId + ")");
            return Response.status(Response.Status.NOT_FOUND).entity("Unknown community id").build();
        }

        Organizations organizations = new Organizations();
        Organization organization = organizations.createNew(name);
        if (organization == null) {
            return Response.serverError().entity("Could not create an organization due to server error." + MONKEYS).build();
        }

        if (!community.members.join(organization)) {
            organizations.delete(organization);
            return Response.serverError().entity("Could not create an organization due to server error." + MONKEYS).build();
        }

        if (email != null && !email.isEmpty()) {

            // Check if email is already registered or invited

            InternetAddress emailAddress;
            try {
                emailAddress = InternetAddress.parse(email.toLowerCase())[0];
            } catch (AddressException e) {
                return Response.status(Response.Status.BAD_REQUEST)
                        .entity("Could not invite an invalid email " + email).build();
            }

            if (new Users().getByEmail(emailAddress.toString()) != null) {
                return Response.status(Response.Status.BAD_REQUEST)
                        .entity("User with email " + email + " is already registered").build();
            }

            Invites invites = new Invites();

            if (null != invites.getActiveByEmail(emailAddress.toString())) {
                return Response.status(Response.Status.BAD_REQUEST)
                        .entity("User with email " + email + " is already invited").build();
            }

            // Create invite and send an email
            Invite invite = invites.newOrganizationByCommunity(community, organization, emailAddress.toString());
            if (invite == null) {
                String msg = "could not create invite for " + email;
                log.error(msg);
                return Response.serverError().entity(msg).build();
            }

            if (!invite.sendInvitationEmails(getHost())) {
                return Response.status(Response.Status.CREATED).entity(
                        "Created organization but failed to send an invitation email due to server error." +
                        "Try resending the invitation from the organization page"
                ).build();
            }
        }

        return Response.status(Response.Status.CREATED).build();
    }


    @POST @Path("{id}/members/invite")
    public Response inviteMember(@PathParam("id") String communityId,
                                 @FormParam(P_INVITE_TYPE) String inviteType,
                                 @FormParam(P_ORG_ID) String organizationId,
                                 @FormParam(P_EMAIL) String email)
    {

        final Community community = new Communities().getById(communityId);
        if (community == null) {
            log.error("Could not invite organization id(" + organizationId + ") " +
                    "to unknown community id(" + communityId + ")");
            return Response.status(Response.Status.BAD_REQUEST).entity("Unknown community id").build();
        }

        switch (inviteType) {
            case V_INVITE_EXISTING:
                return inviteExisting(community, organizationId);
            case V_INVITE_BY_EMAIL:
                return inviteByEmail(community, email);
            default:
                log.warn("Unknown invitation type " + inviteType);
                return Response.status(Response.Status.BAD_REQUEST).entity("Unknown invitation type").build();
        }
    }

    @POST @Path("{id}/members/{orgId}/remove")
    public Response removeMember(@PathParam("id") String communityId,
                                 @PathParam("orgId") String orgId)
    {
        Community community = new Communities().getById(communityId);
        if (community == null) {
            log.warn("requested unknown community id " + communityId);
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Unknown community with id " + communityId).build();
        }

        Organization organization = new Organizations().getById(orgId);
        if (organization == null) {
            log.warn("requested unknown member organization id " + orgId);
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Unknown member organization with id " + orgId).build();
        }

        if (!community.members.leave(organization)) {
            return Response.serverError().entity("Failed to leave the organization due to server error").build();
        }
        return Response.ok().build();
    }

    private Response inviteExisting(Community community, String organizationId) {
        final Organization organization = new Organizations().getById(organizationId);
        if (organization == null) {
            log.error("Could not invite unknown organization id(" + organizationId + ") " +
                    "to community id(" + community.id + ")");
            return Response.status(Response.Status.BAD_REQUEST).entity("unknown organization id").build();
        }

        CommunityMemberExistingOrgInvite invite = new Invites().newCommunityByOrg(community.id, organization);
        if (invite == null) {
            return Response.serverError().entity("could not create invite").build();
        }
        invite.sendInvitationEmails(getHost());
        // TODO: add notification

        return Response.ok().build();
    }

    private Response inviteByEmail(Community community, String email) {
        if (email == null || email.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Invalid email").build();
        }

        User user = new Users().getByEmail(email);
        if (user != null) {
            log.info("Tried to invite an already registered user " + email);
            return Response.status(Response.Status.CONFLICT).entity("user already exists").build();
        }

        Invites invites = new Invites();
        CommunityMemberByEmailInvite invite = invites.newCommunityByEmail(community.id, email);
        if (!invite.sendInvitationEmails(getHost())) {
            invites.deleteInvite(invite.token);
            return Response.serverError().entity("Failed to invite - could not send email").build();
        }
        return Response.ok().build();
    }


    @Override
    protected Logger getLogger() {
        return log;
    }
}