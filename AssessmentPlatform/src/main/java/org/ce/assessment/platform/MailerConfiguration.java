package org.ce.assessment.platform;

import java.util.Properties;

public class MailerConfiguration {
    public final String fromAddress;
    public final String fromName;

    public final Properties smtpProperties;
    public final String username;
    public final String password;

    public MailerConfiguration(Builder builder) {
        fromAddress = builder.fromAddress;
        fromName = builder.fromName;
        smtpProperties = builder.smtpProperties;
        username = builder.username;
        password = builder.password;

    }

    public static class Builder {
        private String fromAddress;
        private String fromName;
        private final Properties smtpProperties = new Properties();
        private String username;
        private String password;

        public Builder withFromAddress(String fromAddress) {
            this.fromAddress = fromAddress;
            return this;
        }

        public Builder withFromName(String fromName) {
            this.fromName = fromName;
            return this;
        }

        public MailerConfiguration build() {
            return new MailerConfiguration(this);
        }
    }

    public static Builder getSparkPostBuilder() {
        Builder builder = new Builder();

        builder.username = "SMTP_Injection";
        builder.password = "738b3997ea929fb89992eeb6ad90ccd06ca8cb11";

        Properties props = builder.smtpProperties;
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.sparkpostmail.com");
        props.put("mail.smtp.port", "587");

        return builder;
    }



}