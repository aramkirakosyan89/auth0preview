package org.ce.assessment.platform.report;

import com.google.gson.Gson;
import org.ce.assessment.limeapi.LimeCallException;
import org.ce.assessment.limeapi.Survey;
import org.ce.assessment.limeapi.UncompletedSurveyException;

// TODO: move all the logic to Survey
public class Report extends Section {


    private boolean isLoaded = false;
	private boolean isSurveyCompleted = false;
	
	public Report(Survey survey) {
		super("Top-value report", survey);
	}

    public boolean isLoaded() {
        return isLoaded;
    }

    public boolean isSurveyCompleted() {
        return isSurveyCompleted;
    }

	public void loadForToken(String token) throws LimeCallException {
	    try {
	        survey.loadForToken(token);
	        isLoaded = true;
			isSurveyCompleted = true;
        } catch (UncompletedSurveyException e) {
	        isSurveyCompleted = false;
        }
    }

	public String toJson() {
        return new Gson().toJson(this);
	}

	public static final Report DUMMY_REPORT = new Report(Survey.DUMMY_SURVEY);

}
