package org.ce.assessment.platform.api;

import com.auth0.AuthenticationController;
import org.ce.assessment.platform.auth0.Auth0Config;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import java.io.IOException;
import java.net.URISyntaxException;

@Path("login")
public class Auth0Api {

    @Context
    ServletConfig config;

    @Context
    HttpServletRequest request;

    @Context
    HttpServletResponse response;

    @GET
    @Path("invite/{token}")
    public void registerByInvite(@PathParam("token") String token) throws IOException {
        request.getSession().setAttribute("token", token);
        response.sendRedirect(createAuth0Url());
    }

    @GET
    public String login() throws IOException, URISyntaxException {
        return createAuth0Url();
    }

    private String createAuth0Url() {
        Auth0Config authConfig = new Auth0Config(config);
        String domain = authConfig.getDomain();
        String clientId = authConfig.getClientId();
        String clientSecret = authConfig.getClientSecret();

        AuthenticationController authenticationController =
                AuthenticationController.newBuilder(domain, clientId, clientSecret)
                        .build();

        String redirectUri = request.getScheme() + "://" + request.getServerName() +
                ":" + request.getServerPort() + "/api/callback/";

        return authenticationController.buildAuthorizeUrl(request, redirectUri)
                .withAudience(String.format("https://%s/userinfo", domain))
                .withScope("openid email")
                .build();
    }

}
