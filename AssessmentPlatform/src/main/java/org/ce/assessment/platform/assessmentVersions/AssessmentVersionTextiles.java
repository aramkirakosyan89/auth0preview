package org.ce.assessment.platform.assessmentVersions;

import org.ce.assessment.limeapi.Survey;
import org.ce.assessment.platform.AssessmentVersion;
import org.ce.assessment.platform.AssessmentVersionSection;
import org.ce.assessment.platform.report.Report;
import org.ce.assessment.platform.report.SevenElementsReport;

public class AssessmentVersionTextiles extends AssessmentVersion {

    public static final String VERSION_ID = AssessmentVersionsCoded.AssessmentVersionId.TEXTILES.version;

    public AssessmentVersionTextiles() {
        super(VERSION_ID, "Textiles Assessment");


        addSection(
                new AssessmentVersionSection() {
                    @Override
                    public Report prepareReport() {

                        Survey survey = new Survey(getLimeSurveyId());
                        SevenElementsReport report =
                                (SevenElementsReport) new SevenElementsReport("Collab", survey)
                                        .setIcon("collaborate");

                        report.addSection("Industry Collaboration", "Ind", 1)
                                .withStrategy("Circular procurement",1)
                                .withStrategy("Joint industry ventures, projects or pilots",2)
                                .withStrategy("Guidance, dialogue with industry stakeholders",3)
                                .withStrategy("Cross-industry projects or pilots",4)
                                .withKeyChallenges()
                        ;

                        report.addSection("Customer Collaboration", "Cust", 2)
                                .withStrategy("Customer Dialogue & Marketing",1)
                                .withStrategy("Customer Programs",2)
                                .withStrategy("Takeback Programs",3)
                                .withStrategy("Customisation",4)
                                .withStrategy("Co-creation",5)
                                .withKeyChallenges()
                        ;

                        report.addSection("Government Collaboration", "Gov", 3)
                                .withStrategy("Advocacy for Circular Economy Policy",1)
                                .withStrategy("Government Programs",2)
                                .withKeyChallenges()
                        ;

                        report.addSection("Internal Collaboration", "Int", 4)
                                .withStrategy("Training on the circular economy",1)
                                .withStrategy("Dialogue with internal stakeholders",2)
                                .withStrategy("Financial incentives tied to circular economy",3)
                                .withKeyChallenges()
                        ;

                        report.addSection("Community Collaboration", "Com", 5)
                                .withStrategy(" Give-back programs", 1)
                                .withStrategy("Joint product development", 2)
                                .withKeyChallenges()
                        ;


                        return report;
                    }
                }
                        .setId(VERSION_ID + "1")
                        .setKind("1")
                        .setLimeSurveyId("637365")
                        .setName("Collaborate to Create Joint Value")
                        .setAssessmentVersionId(VERSION_ID)
        );

        addSection(
                new AssessmentVersionSection() {
                    @Override
                    public Report prepareReport() {

                        Survey survey = new Survey(getLimeSurveyId());
                        SevenElementsReport report = (SevenElementsReport) new SevenElementsReport("Design", survey)
                                .setIcon("design");

                        report.addSection("Design out Waste", "Waste", 1)
                                .withStrategy("Design for minimal waste", 1)
                                .withQuant()
                                .withKeyChallenges()
                        ;

                        report.addSection("Design for Cyclability", "Cyc", 2)
                                .withStrategy("Design for repair",1)
                                .withStrategy("Design for modularity",2)
                                .withStrategy("Design for disassembly",3)
                                .withStrategy("Design for recycling - mono-materials",4)
                                .withStrategy("Design for reuse", 5)
                                .withStrategy("Design for recycling - product trimmings and construction",6)
                                .withStrategy("Design for recycling - bio-degradability",7)
                                .withQuant()
                                .withKeyChallenges()
                        ;

                        report.addSection("Design for Durability", "Dur", 3)
                                .withStrategy("Design for product attachment / emotional durability", 1)
                                .withStrategy("Design for physical durability",2)
                                .withQuant()
                                .withKeyChallenges()
                        ;

                        return report;
                    }
                }
                        .setId(VERSION_ID + "2")
                        .setKind("2")
                        .setLimeSurveyId("167536")
                        .setName("Design for the Future")
                        .setAssessmentVersionId(VERSION_ID)
        );

        addSection(
                new AssessmentVersionSection() {
                    @Override
                    public Report prepareReport() {
                        Survey survey = new Survey(getLimeSurveyId());
                        SevenElementsReport report = new SevenElementsReport("Bus", survey);

                        report.addSection("Product Business Models", "Prod", 1)
                                .withStrategy("Sale of durable, long-lasting goods", 1)
                                .withStrategy("Sale of exchangeable parts:", 2)
                                .withStrategy("Peer to peer sharing", 3)
                                .withStrategy("Leasing, rental, pay per use", 4)
                                .withStrategy("Subscription-based product", 5)
                                .withQuant()
                                .withKeyChallenges()
                        ;

                        report.addSection("Service Business Models", "Serv", 2)
                                .withStrategy("Subscription-based services", 1)
                                .withStrategy("Payment per use", 2)
                                .withStrategy("Crowd-based services", 3)
                                .withQuant()
                                .withKeyChallenges();

                        return report;
                    }
                }
                        .setId(VERSION_ID+"3")
                        .setKind("3")
                        .setLimeSurveyId("977839")
                        .setName("Rethink the Business Model")
                        .setAssessmentVersionId(VERSION_ID)

        );

        addSection(
                new AssessmentVersionSection() {
                    @Override
                    public Report prepareReport() {
                        Survey survey = new Survey(getLimeSurveyId());
                        SevenElementsReport report = new SevenElementsReport("Digital", survey);

                        report.addSection("Data and Insights", "Data", 1)
                                .withStrategy("Data analytics, modeling", 1)
                                .withStrategy("Internet enabled, connected operations", 2)
                                .withStrategy("Sensors, monitoring systems", 3)
                                .withStrategy("Advanced robotics or artificial intelligence", 4)
                                .withQuant()
                                .withKeyChallenges()
                        ;

                        report.addSection("Digital platforms", "DigPl", 2)
                                .withStrategy("Online platforms", 1)
                                .withStrategy("Peer-to-peer marketplaces", 2)
                                .withQuant()
                                .withKeyChallenges()
                        ;

                        return report;
                    }
                }
                        .setId(VERSION_ID+"4")
                        .setKind("4")
                        .setLimeSurveyId("832219")
                        .setName("Incorporate Digital Technology")
                        .setAssessmentVersionId(VERSION_ID)
        );

        addSection(
                new AssessmentVersionSection() {
                    @Override
                    public Report prepareReport() {
                        Survey survey = new Survey(getLimeSurveyId());
                        SevenElementsReport report = new SevenElementsReport("Waste", survey);

                        report.addSection("Reuse Waste Streams - Closed Loop", "ReClsd", 1)
                                .withStrategy("Upcycling", 1)
                                .withStrategy("Closed loop downcycling", 2)
                                .withStrategy("Closed loop high-value mechanical recycling", 3)
                                .withStrategy("Closed loop high-value chemical recycling", 4)
                                .withStrategy("Using closed-loop recycled materials ", 5)
                                .withQuant()
                                .withKeyChallenges()
                        ;

                        report.addSection("Reuse Waste Streams - Open Loop", "ReOpen", 2)
                                .withStrategy("Open loop downcycling", 1)
                                .withStrategy("Open loop high-value mechanical recycling", 2)
                                .withStrategy("Open loop high-value chemical recycling", 3)
                                .withStrategy("Using open loop recycled materials", 4)
                                .withQuant()
                                .withKeyChallenges()
                        ;

                        report.addSection("Energy Recovery from Waste", "Energy", 3)
                                .withStrategy("Recovery and reuse of waste energy", 1)
                                .withStrategy("Processing waste into fuel", 2)
                                .withStrategy("Generating energy from waste", 3)
                                .withQuant()
                                .withKeyChallenges()
                        ;

                        return report;
                    }
                }
                        .setId(VERSION_ID+"5")
                        .setKind("5")
                        .setLimeSurveyId("881885")
                        .setName("Use Waste as a Resource")
                        .setAssessmentVersionId(VERSION_ID)
        );

        addSection(
                new AssessmentVersionSection() {
                    @Override
                    public Report prepareReport() {
                        Survey survey = new Survey(getLimeSurveyId());
                        SevenElementsReport report = new SevenElementsReport("Prior", survey);

                        report.addSection("Regenerative Materials", "RegMat", 1)
                                .withStrategy("Alternative bio-based materials and inputs", 1)
                                .withStrategy("Reusable, recyclable materials and inputs", 2)
                                .withStrategy("Non-toxic materials and inputs", 3)
                                .withStrategy("Non-critical materials and inputs", 4)
                                .withStrategy("Material efficiency", 5)
                                .withQuant()
                                .withKeyChallenges()
                        ;

                        report.addSection("Regenerative Water", "RegWat", 2)
                                .withStrategy("Alternative water use", 1)
                                .withStrategy("Reduced water consumption", 2)
                                .withQuant()
                                .withKeyChallenges()
                        ;

                        report.addSection("Regenerative Energy", "RegEn", 3)
                                .withStrategy("Renewable energy, fuels", 1)
                                .withStrategy("Energy efficiency", 2)
                                .withStrategy("Electrification", 3)
                                .withQuant()
                                .withKeyChallenges()
                        ;

                        return report;
                    }
                }
                        .setId(VERSION_ID+"6")
                        .setKind("6")
                        .setLimeSurveyId("525567")
                        .setName("Prioritise Regenerative Resources")
                        .setAssessmentVersionId(VERSION_ID)
        );

        addSection(
                new AssessmentVersionSection() {
                    @Override
                    public Report prepareReport() {
                        Survey survey = new Survey(getLimeSurveyId());
                        SevenElementsReport report = new SevenElementsReport("Pres", survey);

                        report.addSection("Maximise Lifetime In-Use", "InUse", 1)
                                .withStrategy("Product upgrade", 1)
                                .withStrategy("Product maintenance or repair", 2)
                                .withStrategy("Self-repair, spare part service", 3)
                                .withQuant()
                                .withKeyChallenges()
                        ;

                        report.addSection("Maximise Lifetime After-Use", "AftUse", 2)
                                .withStrategy("Secondhand sale, distribution", 1)
                                .withStrategy("Own brand secondhand sale", 2)
                                .withStrategy("Refurbishment, repair", 3)
                                .withQuant()
                                .withKeyChallenges()
                        ;

                        return report;
                    }
                }
                        .setId(VERSION_ID+"7")
                        .setKind("7")
                        .setLimeSurveyId("877599")
                        .setName("Preserve and Extend What Is Already Made")
                        .setAssessmentVersionId(VERSION_ID)
        );
    }
}
