package org.ce.assessment.platform.invites;

import org.ce.assessment.platform.Community;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.ce.assessment.platform.api.AbstractApi.P_INVITE_TOKEN;

public class CommunityMemberByEmailInvite extends CommunityMemberInvite {

    private final static Logger log = LoggerFactory.getLogger(CommunityMemberByEmailInvite.class);

    public CommunityMemberByEmailInvite(String token, String communityId, String email) {
        super(token, Invites.InviteType.COMMUNITY_BY_EMAIL, communityId);
        setEmail(email);
        setAsAdmin(false);
    }

    @Override
    public boolean sendInvitationEmails(String host) {
        final Community community = getCommunity();
        if (community == null) {
            getLogger().error("Community("+getCommunityId()+") from invite(" + token + ") does not exist");
            return false;
        }

        String emailSubject = "Join " + community.getName() + " on Circle Assessment";
        String emailMsg =
                "Your organization is invited to join " + community.getName()
                        + " on the Circle Assessment platform<br>"
                        + "<a href=\"" + host + "register.jsp?"
                        + param(P_INVITE_TOKEN, token)
                        + "\">Register here</a>";

        return sendEmail(getEmail(), emailSubject, emailMsg);
    }

    @Override
    protected Logger getLogger() {
        return log;
    }
}
