package org.ce.assessment.platform.api;

import org.ce.assessment.platform.auth0.Auth0Client;
import org.ce.assessment.platform.auth0.Auth0Config;
import org.ce.assessment.platform.invites.Invite;
import org.ce.assessment.platform.invites.Invites;
import org.ce.assessment.userdb.User;
import org.ce.assessment.userdb.Users;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import java.io.IOException;

import static org.ce.assessment.platform.api.ViewPath.HOME_PAGE;
import static org.ce.assessment.platform.api.ViewPath.ORG_HOME;
import static org.ce.assessment.platform.api.ViewPath.USER_INFO;

@Path("/callback/")
public class Auth0Callback {

    private Auth0Client client;

    @Context
    ServletConfig config;

    @Context
    HttpServletRequest request;

    @Context
    HttpServletResponse response;

    /**
     * Receives code from Auth0 after successful signIn/signUp
     */
    @GET
    public String get(@QueryParam("code") String code) throws IOException, ServletException {
        Auth0Config authConfig = new Auth0Config(config);
        client = new Auth0Client(authConfig);

        // TODO add global exception handling.
        String accessToken = client.requestAccessToken(request, code);
        String email = client.requestUserInfo(accessToken);

        String redirectUrl = checkUser(email);
        response.sendRedirect(request.getContextPath() + redirectUrl);
        return "";
    }

    /**
     * Checks if user is new registered.
     */
    private String checkUser(String email) throws ServletException, IOException {
        Users users = new Users();
        User user = users.getByEmail(email);

        if (user != null) { /* SignIn case */
            return handleSignIn(user);
        } else { /* SignUp case */
            return handleSignUp(email);
        }

    }

    private String handleSignIn(User user) {
        String redirectUrl;

        if(user.getOrganizationId() != null) {
            // User is attached to organisation.
            redirectUrl = ORG_HOME + user.getOrganizationId();
            user.setUnKnown(false);
        } else {
            redirectUrl = HOME_PAGE;
        }

        saveInSession("userObject", user);
        return redirectUrl;
    }

    private String handleSignUp(String email) {
        // It's a new user, we need to ask name.
        User user = new User();
        String inviteToken = (String) request.getSession().getAttribute("token");
        user.setEmail(email);

        if (inviteToken != null) {
            String orgID = parseInviteToken(inviteToken);
            user.setOrganizationId(orgID);
            user.setUnKnown(false);
        }

        saveInSession("userObject", user);
        return USER_INFO;
    }

    private String parseInviteToken(String inviteToken) {
        Invite invite = new Invites().getByToken(inviteToken);
        return invite.getOrganizationId();
    }


    private void saveInSession(String name, Object toSave) {
        request.getSession().setAttribute(name, toSave);
    }

}
