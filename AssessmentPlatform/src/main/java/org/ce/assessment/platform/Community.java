package org.ce.assessment.platform;

import org.ce.assessment.userdb.Organization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Community {

    private final static Logger log = LoggerFactory.getLogger(Community.class);

    public final String id;
    private String name;
    public final CommunityMembers members;

    private Organization adminOrganization = Organization.NONE;
    private int nMembers = -1;

    public Community(String id, String name, CommunityMembers members) {
        this.id = id;
        this.name = name;
        this.members = members;
    }

    public Organization getAdminOrganization() {
        return adminOrganization;
    }

    public Community setAdminOrganization(Organization adminOrganization) {
        if (adminOrganization == null) {
            this.adminOrganization = Organization.NONE;
        } else {
            this.adminOrganization = adminOrganization;
        }
        return this;
    }

    public int getNMembers() {
        if (nMembers == -1) {
            return getMembers().size();
        }
        return nMembers;
    }

    public Community setNMembers(int nMembers) {
        this.nMembers = nMembers;
        return this;
    }

    public List<Organization> getMembers() {
        return members.getAll();
    }

    public List<CommunityAssessment> getAssessments() {
        return new CommunityAssessments().getForCommunity(this);
    }

    public String getName() {
        return name;
    }
    public Community setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "community \"" + name + "\" (id=" + id + ")";
    }

    public boolean createAssessment(String assessmentVerisonId, String name) {
        CommunityAssessment communityAssessment =
                new CommunityAssessments().create(id, assessmentVerisonId, name);

        if (communityAssessment == null) {
            return false;
        }

        for (Organization organization : getMembers()) {
            if (organization.createAssessment(communityAssessment) == null) {
                log.error("Could not create community assessment " + communityAssessment.getId()
                        + " for organization " + organization.id);
            }
        }
        return true;
    }

    public boolean deleteAssessment(String assessmentId) {
        // all organization assessments will be cascade deleted by the database
        return new CommunityAssessments().delete(assessmentId);
    }


}
