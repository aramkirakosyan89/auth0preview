package org.ce.assessment.platform;

public class ContentNotFoundException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public ContentNotFoundException(String message) {
		super(message);
	}

}
