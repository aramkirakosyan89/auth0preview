package org.ce.assessment.platform.report;

import org.ce.assessment.limeapi.question.TextQuestion;


public class ScoreSector extends Sector {

    private final TextQuestion score;

    ScoreSector(String name, TextQuestion score) {
        super(name);
        this.score = score;
    }

    public int getScore() {
        return Math.round(Float.valueOf(score.value()));
    }
}
