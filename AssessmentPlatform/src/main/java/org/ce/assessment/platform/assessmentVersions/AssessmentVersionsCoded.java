package org.ce.assessment.platform.assessmentVersions;

import org.ce.assessment.platform.AssessmentVersion;
import org.ce.assessment.platform.AssessmentVersions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AssessmentVersionsCoded implements AssessmentVersions {


    private final static Logger log = LoggerFactory.getLogger(AssessmentVersionsCoded.class);


    // WARNING: do not change the existing Ids
    enum AssessmentVersionId {
        GENERAL_DEV("1"),
        TEXTILES("2"),
        GENERAL("3"),
        BUILT_ENVIRONMENT("4"),
        ;

        final String version;

        AssessmentVersionId(String version) {
            this.version = version;
        }

        @Override
        public String toString() {
            return version;
        }
    }

    private final Map<String, AssessmentVersion> versions = new HashMap<>();

    public AssessmentVersionsCoded() {
        add(new AssessmentVersionGeneral());
        add(new AssessmentVersionGeneralDev());
        add(new AssessmentVersionTextiles());
        add(new AssessmentVersionBE());
    }

    private void add(AssessmentVersion version) {
        versions.put(version.getId(), version);
    }

    @Override
    public List<AssessmentVersion> getAll() {
        return new ArrayList<AssessmentVersion>(versions.values());
    }
    @Override
    public List<AssessmentVersion> getAll(String params) {
        throw new UnsupportedOperationException();
    }

    @Override
    public AssessmentVersion getById(String id) {
        AssessmentVersion assessmentVersion = versions.get(id);
        if (assessmentVersion == null) {
            log.error("Unkonwn assessment version with id=" + id);
        }
        return assessmentVersion;
    }

    @Override
    public AssessmentVersion create(String name) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean delete(AssessmentVersion assessmentVersion) {
        throw new UnsupportedOperationException();
    }
}
