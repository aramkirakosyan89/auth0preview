package org.ce.assessment.platform.auth0;

import javax.servlet.ServletConfig;

/**
 * Wrapper for Auth0 configurations.
 */
public class Auth0Config {

    private ServletConfig config;

    public Auth0Config(ServletConfig config) {
        this.config = config;
    }

    public String getDomain() {
        return config.getServletContext().getInitParameter("com.auth0.domain");
    }

    public String getClientId() {
        return config.getServletContext().getInitParameter("com.auth0.clientId");
    }

    public String getClientSecret() {
        return config.getServletContext().getInitParameter("com.auth0.clientSecret");
    }

}
