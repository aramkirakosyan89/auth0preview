package org.ce.assessment.platform.assessmentVersions;

import org.ce.assessment.limeapi.Survey;
import org.ce.assessment.platform.AssessmentVersion;
import org.ce.assessment.platform.AssessmentVersionSection;
import org.ce.assessment.platform.report.Report;
import org.ce.assessment.platform.report.SevenElementsReport;
import org.ce.assessment.platform.report.SevenElementsTier2Report;

class AssessmentVersionBE extends AssessmentVersion {

    private static final String VERSION_ID = AssessmentVersionsCoded.AssessmentVersionId.BUILT_ENVIRONMENT.toString();

    AssessmentVersionBE() {
        super(VERSION_ID, "Built Environment Assessment");


        addSection(
                new AssessmentVersionSection() {
                    @Override
                    public Report prepareReport() {

                        Survey survey = new Survey(getLimeSurveyId());
                        SevenElementsReport report =
                                (SevenElementsReport) new SevenElementsTier2Report("Collab", survey)
                                        .setIcon("collaborate");

                        report.addSection("Industry Collaboration", "Ind", 1);
                        report.addSection("Customer Collaboration", "Cust", 2);
                        report.addSection("Government Collaboration", "Gov", 3);
                        report.addSection("Internal Collaboration", "Int", 4);
                        report.addSection("Community Collaboration", "Com", 5);
                        return report;
                    }
                }
                        .setId(VERSION_ID + "1")
                        .setKind("1")
                        .setLimeSurveyId("246231")
                        .setName("Collaborate to Create Joint Value")
                        .setAssessmentVersionId(VERSION_ID)
        );

        addSection(
                new AssessmentVersionSection() {
                    @Override
                    public Report prepareReport() {

                        Survey survey = new Survey(getLimeSurveyId());
                        SevenElementsReport report = (SevenElementsReport) new SevenElementsTier2Report("Design", survey)
                                .setIcon("design");

                        report.addSection("Design out Waste", "Waste", 1);
                        report.addSection("Design for Cyclability", "Cyc", 2);
                        report.addSection("Design for Durability", "Dur", 3);
                        return report;
                    }
                }
                        .setId(VERSION_ID + "2")
                        .setKind("2")
                        .setLimeSurveyId("422758")
                        .setName("Design for the Future")
                        .setAssessmentVersionId(VERSION_ID)
        );

        addSection(
                new AssessmentVersionSection() {
                    @Override
                    public Report prepareReport() {
                        Survey survey = new Survey(getLimeSurveyId());
                        SevenElementsReport report = new SevenElementsTier2Report("Bus", survey);

                        report.addSection("Product Business Models", "Prod", 1);
                        report.addSection("Service Business Models", "Serv", 2);
                        return report;
                    }
                }
                        .setId(VERSION_ID+"3")
                        .setKind("3")
                        .setLimeSurveyId("692531")
                        .setName("Rethink the Business Model")
                        .setAssessmentVersionId(VERSION_ID)

        );

        addSection(
                new AssessmentVersionSection() {
                    @Override
                    public Report prepareReport() {
                        Survey survey = new Survey(getLimeSurveyId());
                        SevenElementsReport report = new SevenElementsTier2Report("Digital", survey);

                        report.addSection("Data and Insights", "Data", 1);
                        report.addSection("Digital platforms", "DigPl", 2);
                        return report;
                    }
                }
                        .setId(VERSION_ID+"4")
                        .setKind("4")
                        .setLimeSurveyId("818379")
                        .setName("Incorporate Digital Technology")
                        .setAssessmentVersionId(VERSION_ID)
        );

        addSection(
                new AssessmentVersionSection() {
                    @Override
                    public Report prepareReport() {
                        Survey survey = new Survey(getLimeSurveyId());
                        SevenElementsReport report = new SevenElementsTier2Report("Waste", survey);

                        report.addSection("Reuse Waste Streams - Closed Loop", "ReClsd", 1);
                        report.addSection("Reuse Waste Streams - Open Loop", "ReOpen", 2);
                        report.addSection("Energy Recovery from Waste", "Energy", 3);
                        return report;
                    }
                }
                        .setId(VERSION_ID+"5")
                        .setKind("5")
                        .setLimeSurveyId("689482")
                        .setName("Use Waste as a Resource")
                        .setAssessmentVersionId(VERSION_ID)
        );

        addSection(
                new AssessmentVersionSection() {
                    @Override
                    public Report prepareReport() {
                        Survey survey = new Survey(getLimeSurveyId());
                        SevenElementsReport report = new SevenElementsTier2Report("Prior", survey);

                        report.addSection("Regenerative Materials", "RegMat", 1);
                        report.addSection("Regenerative Water", "RegWat", 2);
                        report.addSection("Regenerative Energy", "RegEn", 3);

                        return report;
                    }
                }
                        .setId(VERSION_ID+"6")
                        .setKind("6")
                        .setLimeSurveyId("252856")
                        .setName("Prioritise Regenerative Resources")
                        .setAssessmentVersionId(VERSION_ID)
        );

        addSection(
                new AssessmentVersionSection() {
                    @Override
                    public Report prepareReport() {
                        Survey survey = new Survey(getLimeSurveyId());
                        SevenElementsReport report = new SevenElementsTier2Report("Pres", survey);
                        report.addSection("Maximise Lifetime In-Use", "InUse", 1);
                        report.addSection("Maximise Lifetime After-Use", "AftUse", 2);
                        report.addSection("Maximise Lifetime Biological", "Bio", 3);
                        return report;
                    }
                }
                        .setId(VERSION_ID+"7")
                        .setKind("7")
                        .setLimeSurveyId("986675")
                        .setName("Preserve and Extend What Is Already Made")
                        .setAssessmentVersionId(VERSION_ID)
        );
    }
}
