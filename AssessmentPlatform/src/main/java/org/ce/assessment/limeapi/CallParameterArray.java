package org.ce.assessment.limeapi;

import java.util.ArrayList;
import java.util.List;

public abstract class CallParameterArray extends CallParameter {
    private final List<String> values = new ArrayList<>();

    public CallParameterArray(String name) {
        super(name);
    }

    public CallParameterArray withValue(String value) {
        values.add(value);
        return this;
    }

    protected abstract String getDelimiter();

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String comma = "";
        sb.append("\"").append(name).append("\" : [");
        for (String value : values) {
            sb.append(comma).append(getDelimiter()).append(value).append(getDelimiter());
            comma = ", ";
        }
        sb.append("]");
        return sb.toString();
    }

}
