package org.ce.assessment.limeapi.question;

import org.ce.assessment.limeapi.Survey;

public class ConditionQuestion {

    private final TextQuestion question;
    private final String value;
    private final boolean equalToValue;

    public ConditionQuestion(Survey survey, String questionCode, String value, boolean equalToValue) {
        question = (TextQuestion) survey.question(new TextQuestion.Builder(questionCode));
        this.equalToValue = equalToValue;
        this.value = value;
    }

    public boolean isTrue() {
        boolean isEqual = value.equals(question.value());
        return equalToValue == isEqual;
    }
}
