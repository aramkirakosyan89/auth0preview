package org.ce.assessment.limeapi;

public class CallParameterArrayString extends CallParameterArray {

	public CallParameterArrayString(String name) {
		super(name);
	}

	@Override
	protected String getDelimiter() {
		return "\"";
	}
}
