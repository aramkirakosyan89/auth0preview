package org.ce.assessment.limeapi.answeroptions;

import java.util.Map;

public class RadioAnswerOption extends AbstractAnswerOption {

    private String selectedValueCode = "none";
    private final Map<String, String> answerTexts;

    public RadioAnswerOption(String questionCode, String code, String text, Map<String, String> answerTexts) {
        super(questionCode, code, text);
        this.answerTexts = answerTexts;
    }

    @Override
    public void setValue(Map<String, String> responses) {
        selectedValueCode = responses.get(questionCode+"["+code+"]");
    }

    public String getSelectedValueCode() {
        return selectedValueCode;
    }

    public String getSelectedValue() {
        if (answerTexts.size() == 0) {
            return selectedValueCode;
        }
        else return answerTexts.get(selectedValueCode);
    }
}
