package org.ce.assessment.limeapi.question;

import org.ce.assessment.limeapi.AbstractQuestionBuilder;
import org.ce.assessment.limeapi.answeroptions.CheckedAnswerOption;
import org.ce.assessment.limeapi.answeroptions.MultipleChoiceAnswerOptionWithComment;
import org.ce.assessment.limeapi.answeroptions.RadioAnswerOption;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * A multiple choice with comments question that relies on another array question for
 * information which options are relevant
 *
 * Conventions:
 *  * questionCode = relevance question code + "Sub"
 *  * answer option codes are the same
 *
 */
public class MultipleChoiceQuestionWithRelevance extends MultipleChoiceQuestionWithComment {

    private ArrayQuestion relevanceQuestion;

    public static class Builder extends AbstractQuestionBuilder {

        private final String limeDsName;

        public Builder(String questionCode, String limeDsName) {
            super(questionCode);
            this.limeDsName = limeDsName;
        }

        @Override
        public Question build(String surveyId) {
            ArrayQuestion relevanceQuestion = (ArrayQuestion)
                    new ArrayQuestion.Builder(questionCode, limeDsName).build(surveyId);
            return new MultipleChoiceQuestionWithRelevance(surveyId, questionCode+"Sub", relevanceQuestion, limeDsName);
        }
    }

    private MultipleChoiceQuestionWithRelevance(String surveyId, String questionCode, ArrayQuestion relevanceQuestion, String limeDataSourceName) {
        super(surveyId, questionCode, limeDataSourceName);
        this.relevanceQuestion = relevanceQuestion;
    }

    @Override
    public void addQuestionCodesToList(List<String> questionCodes) {
        super.addQuestionCodesToList(questionCodes);
        relevanceQuestion.addQuestionCodesToList(questionCodes);
    }

    @Override
    public void setAnswer(Map<String, String> responses) {
        super.setAnswer(responses);
        relevanceQuestion.setAnswer(responses);
    }

    @Override
    protected void loadText(String limeDataSourceName) {
        super.loadText(limeDataSourceName);
        String text = getText();
        // remove <script>
        if (text != null && text.indexOf("<")>0) {
            setText(text.substring(0, text.indexOf("<")));
        }
    }

    /**
     *
     * @return options with text from the relevance option and comments from the implemented options
     */
    @Override
    public List<CheckedAnswerOption> getSelectedOptions() {
        List<CheckedAnswerOption> selectedOptions = super.getSelectedOptions();

        Map<String, RadioAnswerOption> relevanceOptions = relevanceQuestion.getOptions();

        List<CheckedAnswerOption> res = new ArrayList<>();
        for (CheckedAnswerOption selectedOption : selectedOptions) {
            MultipleChoiceAnswerOptionWithComment option =
                    prepareOption(selectedOption, relevanceOptions.get(selectedOption.getCode()));
            res.add(option);
        }

        return res;
    }

    @Override
    public List<CheckedAnswerOption> getUnselectedOptions() {
        List<CheckedAnswerOption> res = new ArrayList<>();

        List<CheckedAnswerOption> unselectedOptions = super.getUnselectedOptions();

        Map<String, RadioAnswerOption> relevanceOptions = relevanceQuestion.getOptions();

        for (CheckedAnswerOption usO : unselectedOptions) {
            RadioAnswerOption rO = relevanceOptions.get(usO.getCode());
            if (rO.getSelectedValueCode() == "A1") {
                MultipleChoiceAnswerOptionWithComment option = prepareOption(usO, rO);
                res.add(option);
            }
        }
        return res;
    }

    private MultipleChoiceAnswerOptionWithComment prepareOption(CheckedAnswerOption optionWithComment,
                                                                RadioAnswerOption relevanceOption) {
        return new MultipleChoiceAnswerOptionWithComment("", "", relevanceOption.getText())
                .setComment(((MultipleChoiceAnswerOptionWithComment) optionWithComment).getComment());
    }
}
