package org.ce.assessment.limeapi;

import org.ce.assessment.platform.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetSessionKey extends LimeCall {

    private static final Logger log = LoggerFactory.getLogger(GetSessionKey.class);

//    public final static String LIME_USER = Platform.instance().configuration.limeUser;
//    public final static String LIME_PASSWORD = Platform.instance().configuration.limePassword;

//    private static String getContextParam(String paramName) {
//        try {
//            InitialContext cxt = new InitialContext();
//            return (String) ((Context) cxt.lookup("java:/comp/env")).lookup(paramName);
//        } catch (NamingException e) {
//            log.error("Could not find " + paramName + " context parameter (should be specified in META-INF/context.xml");
//            return "";
//        }
//    }

	public GetSessionKey() {
		params.add(new CallParameterString("username", Platform.instance().configuration.limeUser));
		params.add(new CallParameterString("password", Platform.instance().configuration.limePassword));
	}
	
	@Override
	String methodName() {
		return "get_session_key";
	}
	
	@Override
	protected boolean needsSessionKey() {
		return false;
	}
	
	public String getKey() throws LimeCallException {
		String key = getResult();
		if (key.charAt(0) == '"' && key.charAt(key.length()-1) == '\"') {
			key = key.substring(1, key.length()-1);
		}
		return key;
	}
}
