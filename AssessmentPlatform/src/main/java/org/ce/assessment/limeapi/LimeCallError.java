package org.ce.assessment.limeapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum LimeCallError {
	INVALID_SESSION_KEY("Invalid session key", "wrong credentials?"),
	NO_RESPONSE_FOR_TOKEN("No Response found for Token", "the survey has not been completed?"),
	NO_MAX_ID("No Data, could not get max id", "the survey has not been completed?"),
	NO_SURVEY_TABLE("No Data, survey tableName does not exist", "the survey is not activated?"),
	
	
	UNKNOWN("other", "who knows? Read logs");
	
	private final static Logger log = LoggerFactory.getLogger(LimeCallError.class);
	public final String msg;
	public final String suggestion;
	private LimeCallError(String msg, String suggestion) {
		this.msg = msg;
		this.suggestion = suggestion;
	}
	
	public String toString() {
		return msg + ". Perhaps " + suggestion;
	}
	
		
	public static LimeCallError getError(String msg) {		
		if (msg.startsWith("{\"status")) {
			msg = msg.substring(11, msg.length()-2);
			log.info("New lime call error: " + msg);
			for (LimeCallError e : values()) {
				if (msg.startsWith(e.msg)) {		        	
		            return e;
		        }
		    }
			log.error("unknown error message: '" + msg + "'");			
			return LimeCallError.UNKNOWN;
		}
	    return null;
	}
}