package org.ce.assessment.limeapi;

public class CallParameterObject extends CallParameter {

    final String value;

    public CallParameterObject(String name, String value) {
        super(name);
        this.value = value;
    }

    @Override
    public String toString() {
        return "\"" + name + "\": " + value + "";
    }
}
