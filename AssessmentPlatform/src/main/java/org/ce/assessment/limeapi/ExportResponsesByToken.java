package org.ce.assessment.limeapi;

import java.util.Map;

import javax.xml.bind.DatatypeConverter;

public class ExportResponsesByToken extends ExportResponses {
	
	@Override
	String methodName() {
		return "export_responses_by_token";
	}
	
	public ExportResponsesByToken(Survey survey, String token) {
		super(survey);
		params.add(new CallParameterString("sToken", token));		
	}
	
	public Map<String, String> getResponses() throws LimeCallException {
		try {
			String res = getResult();
			res = new String( DatatypeConverter.parseBase64Binary(res)) ;		
	        return extractAnswers(parse(res)).get(0);
		} catch (LimeCallException e) {
			if (e.error == LimeCallError.NO_MAX_ID || e.error == LimeCallError.NO_RESPONSE_FOR_TOKEN) {
				throw new UncompletedSurveyException(e.error);
			}
			throw e;
		}
	}
}
