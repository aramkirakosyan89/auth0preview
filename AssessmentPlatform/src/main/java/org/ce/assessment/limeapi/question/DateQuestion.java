package org.ce.assessment.limeapi.question;

import org.ce.assessment.limeapi.AbstractQuestionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class DateQuestion extends TextQuestion {

    private final static Logger log = LoggerFactory.getLogger(DateQuestion.class);
    private Date date;

    public static class Builder extends AbstractQuestionBuilder {

        public Builder(String questionCode) {
            super(questionCode);
        }

        @Override
        public DateQuestion build(String surveyId) {
            return new DateQuestion(surveyId, questionCode);
        }
    }


    protected DateQuestion(String surveyId, String questionCode) {
        super(surveyId, questionCode);
    }

    @Override
    public void setAnswer(Map<String, String> responses) {
        super.setAnswer(responses);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.ENGLISH);
        try {
            date = df.parse(answer);
        } catch (ParseException e) {
            e.printStackTrace();
            log.error("Could not parse date " + answer + ". Survey id: " + surveyId + ". Question: "  + questionCode);
            date = null;
        }
    }

    public Date getDate() {
        return date;
    }

    public String getFormattedDate() {
        if (date == null) {
            return "unknown";
        }

        DateFormat df = new SimpleDateFormat("MMM yyyy", Locale.ENGLISH);
        return df.format(date);
    }

    @Override
    public String value() {
        return getFormattedDate();
    }
}
