package org.ce.assessment.limeapi;

public class CallParameterInt extends CallParameter {

	private final int value;
	
	public CallParameterInt(String name, int value) {
		super(name);
		this.value = value;
	}
	
	@Override
	public String toString() {		
		return "\"" + name + "\" : " + value;
	}

}
