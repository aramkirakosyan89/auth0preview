package org.ce.assessment.limeapi.question;

import org.ce.assessment.limeapi.AbstractQuestionBuilder;

import java.util.Map;

public class TextQuestion extends Question {

	protected String answer = "";

	public static class Builder extends AbstractQuestionBuilder {

		public Builder(String questionCode) {
			super(questionCode);
		}

		@Override
		public TextQuestion build(String surveyId) {
			return new TextQuestion(surveyId, questionCode);
		}
	}

	protected TextQuestion(String surveyId, String questionCode) {
	    super(surveyId, questionCode);
    }

    @Override
	public void setAnswer(Map<String, String> responses) {
		answer = responses.get(questionCode);
	}

	@Override
	public String value() {
		return answer;
	}

	@Override
	public String toString() {
		return value();
	}

	public static final TextQuestion DUMMY = new TextQuestion("", "");

}
