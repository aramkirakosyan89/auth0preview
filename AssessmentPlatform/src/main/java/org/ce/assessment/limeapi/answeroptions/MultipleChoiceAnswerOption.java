package org.ce.assessment.limeapi.answeroptions;

public class MultipleChoiceAnswerOption extends CheckedAnswerOption {

    public MultipleChoiceAnswerOption(String questionCode, String code, String text) {
        super(questionCode, code, text, "Y");
    }
}