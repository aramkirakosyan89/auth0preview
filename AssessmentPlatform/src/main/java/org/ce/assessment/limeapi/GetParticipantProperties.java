package org.ce.assessment.limeapi;

import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetParticipantProperties extends LimeCall {

    private static final Logger log = LoggerFactory.getLogger(GetParticipantProperties.class);

    @Override
    String methodName() {
        return "get_participant_properties";
    }

    public GetParticipantProperties(Survey survey) {
        super(survey);
    }

    public String getParticipantWithToken(String token) {
        params.clear();
        params.add(new CallParameterString("iSurveyID", survey.getId()));
        params.add(
                new CallParameterObject("aTokenQueryProperties",
                        "{\"token\":\"" + token + "\"}"));
        params.add(
                new CallParameterArrayString("aTokenProperties").withValue("completed").withValue("surveylink"));
        try {
            String result = getResult();
            JsonObject jsonObject = parse(result);
            return jsonObject.get("completed").getAsString();
        } catch (LimeCallException e) {
            e.printStackTrace();
        }
        return "nothing";
    }
}

//(string $sSessionKey,
// integer $iSurveyID,
// array|integer $aTokenQueryProperties, array $aTokenProperties = null) : array
