package org.ce.assessment.limeapi.question;

import org.ce.assessment.limeapi.answeroptions.MultipleChoiceAnswerOptionWithComment;


public class MultipleChoiceQuestionWithComment extends MultipleChoiceQuestion {

    protected MultipleChoiceQuestionWithComment(String surveyId, String questionCode, String limeDataSourceName) {
        super(surveyId, questionCode, limeDataSourceName);
    }

    @Override
    protected void addOption(String code, String text) {
        MultipleChoiceAnswerOptionWithComment option = new MultipleChoiceAnswerOptionWithComment(questionCode, code, text);
        options.put(code, option);
    }
}
