package org.ce.assessment.limeapi;

public class CallParameterArrayObject extends CallParameterArray {

    public CallParameterArrayObject(String name) {
        super(name);
    }

    @Override
    protected String getDelimiter() {
        return "";
    }
}
