package org.ce.assessment.limeapi.answeroptions;

import java.util.Map;

public class CheckedAnswerOption extends AbstractAnswerOption {

    /** value that indicates that this option is checked */
    protected final String checkedValue;

    private boolean isSelected = false;

    public CheckedAnswerOption(String questionCode, String code, String text, String checkedValue) {
        super(questionCode, code, text);
        this.checkedValue = checkedValue;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    @Override
    public void setValue(Map<String, String> responses) {
        isSelected = checkedValue.equals(responses.get(questionCode+"["+code+"]"));
    }
}
