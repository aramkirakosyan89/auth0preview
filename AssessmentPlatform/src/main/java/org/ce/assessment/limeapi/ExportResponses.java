package org.ce.assessment.limeapi;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ExportResponses extends LimeCall {
	
	private Logger log = LoggerFactory.getLogger(ExportResponses.class);
	
	List<Map<String, String>> responses;

	public ExportResponses(Survey survey) {
		super(survey);
		params.add(new CallParameterString("iSurveyID", survey.getId()));
		params.add(new CallParameterString("sDocumentType", "json"));
	}
	
	@Override
	String methodName() {
		return "export_responses";
	}

	protected List<Map<String, String>> extractAnswers(JsonObject json) throws UncompletedSurveyException {

		List<Map<String, String>> answerSets = new ArrayList<Map<String, String>>();
		
		JsonArray allResponses = json.getAsJsonArray("responses");
		for (JsonElement responseSet : allResponses) {
			
			JsonObject res = responseSet.getAsJsonObject().entrySet().iterator().next().getValue().getAsJsonObject();
			if (res.get("submitdate").isJsonNull()) {
				throw new UncompletedSurveyException();
			}
			
			Map<String, String> answers = new HashMap<String, String>();
			for (String q : survey.getQuestionCodes()) {
				JsonElement answer = res.get(q);
				if (answer == null) {
                    log.error("Unknown question " + q);
                } else if (!answer.isJsonNull()) {
					answers.put(q, answer.getAsString());
				} else {
					answers.put(q, "");
				}
			}
			answerSets.add(answers);
		}
		
		return answerSets;
	}
	
	
	public List<Map<String, String>> getAnswers() throws LimeCallException {		
		String res = new String( DatatypeConverter.parseBase64Binary(getResult())) ;		
        return extractAnswers(parse(res));
	}
	
	public List<String> getTokens() throws LimeCallException {
		params.add(new CallParameterString("sLanguageCode", "en"));
		params.add(new CallParameterString("sCompletionStatus", "all"));
		params.add(new CallParameterString("sHeadingType", "full"));		
		params.add(new CallParameterString("sResponseType", "long"));
		params.add(new CallParameterInt("iFromResponseID", 0));
		params.add(new CallParameterInt("iToResponseID", Integer.MAX_VALUE));
		params.add(new CallParameterArrayString("aFields").withValue("token"));
		
		List<String> tokens = new ArrayList<String>();

		JsonObject resJson = parse(new String( DatatypeConverter.parseBase64Binary(getResult()))) ;					
		JsonArray allResponses = resJson.getAsJsonArray("responses");		
		for (JsonElement responseSet : allResponses) {			
			JsonObject res = responseSet.getAsJsonObject().entrySet().iterator().next().getValue().getAsJsonObject();
			tokens.add(res.get("Token").getAsString());				
		}
		
		return tokens;
	}
}
