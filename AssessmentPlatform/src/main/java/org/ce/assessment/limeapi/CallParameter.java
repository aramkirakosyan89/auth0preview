package org.ce.assessment.limeapi;

public abstract class CallParameter {
	
	final String name;
	
	CallParameter(String name) {
		this.name = name;
	}
	
	@Override
	public abstract String toString();
	

}
