package org.ce.assessment.limeapi.answeroptions;

import java.util.List;
import java.util.Map;

public abstract class AbstractAnswerOption {
    protected final String questionCode;
    protected final String code;
    protected final String text;

    AbstractAnswerOption(String questionCode, String code, String text) {
        this.questionCode = questionCode;
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return getText();
    }

    public void addQuestionCodeToList(List<String> questionCodes) {
        questionCodes.add(questionCode + "[" + code + "]");
    }

    public abstract void setValue(Map<String, String> responses);
}
