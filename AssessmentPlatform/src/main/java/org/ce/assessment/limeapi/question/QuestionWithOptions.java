package org.ce.assessment.limeapi.question;

import org.ce.assessment.limeapi.answeroptions.AbstractAnswerOption;
import org.ce.assessment.platform.DbQuery;
import org.slf4j.Logger;

import javax.naming.NamingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class QuestionWithOptions<T extends AbstractAnswerOption> extends QuestionWithAnswerTexts {

    protected abstract Logger logger();

    protected final Map<String, T> options = new LinkedHashMap<>();

    protected QuestionWithOptions(String surveyId, String questionCode) {
        super(surveyId, questionCode);
    }

    protected abstract void addOption(String code, String text);

    protected void loadOptions(String limeDataSourceName) {
        String querySQL =
                "SELECT " +
                        "  M.title as code, " +
                        "  M.question as answerOption " +
                        "FROM " +
                        "  lime_questions L " +
                        "  INNER JOIN lime_questions M ON L.qid = M.parent_qid " +
                        "WHERE " +
                        "  L.sid = " + surveyId + " AND " +
                        "  L.title = '"+ questionCode + "';";

        try (
                DbQuery selectQuery = new DbQuery(limeDataSourceName, querySQL)
        )
        {
            ResultSet results = selectQuery.getResults();
            while (results.next()) {
                String optionText = results.getString("answerOption");
                if (!optionText.toLowerCase().contains("none of the above")) {
                    addOption(results.getString("code"), optionText);
                }
            }
        } catch (SQLException | NamingException e) {
            logger().error("Could not get answer options for question '" + questionCode + "'. SurveyId: " + surveyId);
            logger().error(e.toString());
        }
    }

    @Override
    public void addQuestionCodesToList(List<String> questionCodes) {
        for (T option : options.values()) {
            option.addQuestionCodeToList(questionCodes);
        }
    }

    @Override
    public void setAnswer(Map<String, String> responses) {
        for (T option : options.values()) {
            option.setValue(responses);
        }
    }


}
