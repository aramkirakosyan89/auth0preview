package org.ce.assessment.limeapi;


import com.google.gson.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class LimeCall  {
		
	private static final Logger log = LoggerFactory.getLogger(LimeCall.class);
	
	final List<CallParameter> params = new ArrayList<>();
	protected final Survey survey;
	
	protected LimeCall(Survey survey) {
		this.survey = survey;
	}
	
	protected LimeCall() {
		this.survey = null;
	}

	abstract String methodName();
	
	protected JsonObject parse(String jsonLine) {
		try {
		    JsonElement jelement = new JsonParser().parse(jsonLine);
		    return jelement.getAsJsonObject();
		} catch (JsonSyntaxException | NullPointerException e) {
		    log.error("Could not parse json: \n" + jsonLine);
		    throw e;
		}
	}

	protected JsonArray parseAsArray(String jsonLine) {
		try {
			JsonElement jelement = new JsonParser().parse(jsonLine);
			return jelement.getAsJsonArray();
		} catch (JsonSyntaxException | NullPointerException e) {
			log.error("Could not parse json: \n" + jsonLine);
			throw e;
		}
	}
	
	/** Indicates that the RC call needs session key. Most calls need it.
	 *  Calls, that do not need it, should override this method and return false
	 */
	protected boolean needsSessionKey() {
		return true;
	}
	
	protected String prepareCallString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{\"method\": \"").append(methodName()).append("\", \"params\": {");
		
		String comma = "";
		if (needsSessionKey()) {
			sb.append("\"sSessionKey\": \"").append(LimeClient.instance().getSessionKey()).append("\"");
			comma = ", ";
		}
		
		for (CallParameter param : params) {
			sb.append(comma).append(param.toString());
			comma = ", ";
		}
		sb.append("}, \"id\": 1}");
		return sb.toString();
	}

	protected String makeCall() throws IOException, LimeCallException {
		String callString = prepareCallString();
		log.debug(callString);
		String callResult = LimeClient.instance().execute(callString);

		try {
			JsonObject jobject = parse(callResult);
            String result = jobject.get("result").toString();
            log.debug(result);
            return result;
		} catch (JsonSyntaxException e) {
			throw new LimeCallException("Lime call failed with message: " + callResult);
		}
	}
	
	public String getResult() throws LimeCallException {
		String result;
		try {
			result = makeCall();
		} catch (IOException e) {
			throw new LimeCallException(e);
		}
		
		LimeCallError error = LimeCallError.getError(result);
		if (error == null) {
			return result;
		} 
		else if (error == LimeCallError.INVALID_SESSION_KEY) {
			log.info("invalid session key");
			LimeClient.instance().invalidateSessionKey();
			return getResult();
		} 
		else {
			throw new LimeCallException(error);
		}
	}
	
}
