package org.ce.assessment.limeapi.answeroptions;

import java.util.List;
import java.util.Map;

public class MultipleChoiceAnswerOptionWithComment extends MultipleChoiceAnswerOption {

    private String comment;

    public MultipleChoiceAnswerOptionWithComment(String questionCode, String code, String text) {
        super(questionCode, code, text);
    }

    public String getComment() {
        return comment;
    }

    public MultipleChoiceAnswerOptionWithComment setComment(String comment) {
        this.comment = ("null".equals(comment))?"":comment;
        return this;
    }

    @Override
    public void setValue(Map<String, String> responses) {
        super.setValue(responses);
        setComment(responses.get(questionCode+"[" + code + "comment]"));
    }

    @Override
    public void addQuestionCodeToList(List<String> questionCodes) {
        super.addQuestionCodeToList(questionCodes);
        questionCodes.add(questionCode + "[" + code + "comment]");
    }
}
