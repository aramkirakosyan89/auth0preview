<%@ page import="org.ce.assessment.userdb.Organization" %>
<%@ page import="org.ce.assessment.userdb.Organizations" %>
<%@ page import="javax.ws.rs.core.Response" %>


<%
	String msg = "";
	AuthManager mgr = new AuthManager();
	User user = mgr.getAuthedUserWithException(request);
    Organization org = new Organizations().getById(user.getOrganizationId());
%>


<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="main-style.css" rel="stylesheet" type="text/css">
    <title>Circle Assessment - <%=user.getFullName()%></title>
</head>

<%@include file="includes.jsp" %>


<script>

    function resetPwForm() {
        $("#curPw").val("");
        $("#pw1").val("");
        $("#pw2").val("");
    }

    function changePw() {
        $.ajax({
            method: 'post',
            url: '<%=response.encodeURL("api/users/"+user.getId()+"/password/")%>',
            data: {
                currentPassword: $("#curPw").val(),
                password: $("#pw1").val(),
                password2: $("#pw2").val()
            }
        })
            .done(function(x) {
                alert("Password changed");
                $('#change_pw_modal').modal('close');
            })
            .fail(
                function(x) {
                    console.log(x.responseText);
                    if (x.status == <%=Response.Status.BAD_REQUEST.getStatusCode()%>) {
                        alert(x.responseText);
                    } else {
                        alert("Could not change password due to an interal error. Try again later");
                    }
                });
    }
</script>


<body>
	<%@include file="header.jsp" %>

    <div class="container body white">


		<% //TODO: replace with material msg
            if (!msg.isEmpty()) { %><div class="page_message"><%=msg%></div><%}%>
		
        <h2><%=user.getName() %></h2>
		<button class="btn waves-effect waves-teal modal-trigger"
                onclick="resetPwForm()"
                data-target="change_pw_modal">
            Change password
        </button>
		<br>

		<span style="font-size: 1.2rem;">
            <%=user.getJobTitle() %> at
            <a href="<%=response.encodeURL("organization.jsp?id=" + org.id)%>"><%=org.getName() %></a>
			<br>
			<br>
			<img width="20" height="20" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMwAAADMCAMAAAAI/LzAAAAAvVBMVEUAW40ASXH///8AVInf4+gAN2YAPGmnvcsAQGsARW6Op7+OoLEAWYyGobqGmawAV4sAQ23v9PcAUIbz9/nl7/QATYWvxdJ9mq/L198tYoQAVYQATnhlmbg+gqc4fqUfbJg/c5xcj7Cnx9iBq8RZiKuauc0iaJUpdqESYZG3ztsfVnpwob3P3+iPqLrE2eUZbJlxkKc+cpG9y9Vjhp6OsMZSfpqZscA2aostW34+ZYQWUXdzlrNPd5IALmFZh6I0AtXeAAANQ0lEQVR4nNWdaXebPBOGWbygt7Ss3prYifeljpvYWfq4af//z3rBxiABgpGQjDufck4ORheaezTaFU2GeY7TWE9+3A9HDwpmD6Ph/Y/JuuE4npTXKqJ/0N/uutPxctVxXcsyTYUw07Qs1+2sluNpd7f1Rb9bKIy3m3+dLRVXt1IQaTMt3VWWs6/zndAqEgfjTMaPC6WUgyBSFo/jiSOsCGJgHH+6DNzKTLtVKU/gd667nPpigATABM41tFxGDALJtYZCHK4yjD//vdItfpKzWfrq97xyQKgIs71fKHCVFFaPpSzut/XB+OtlR2eVSQGOqXeW6yrVww/jd0duZfdKm+WOuvw4vDD+68gSjnLCsUavvDicMJOVKUYqWQt+eDW5Howz+emKk0oOjun+5GpKOWB2M10iScSjz3ZXgPGnC/ksIc1iyiwdVpjJQpZWMjjWYiIVZjvsXAnlhNMZsrWiLDDe/EFKNKab9TBnSdkYYPyxzBCWb6Y5ZlAOHGb9eOVqOZv1uBYO40zlNPgAGmsKbXOAMNuxXg9KaPoYGAdgMLtRjSwBzQjWgoJg5quaXOxiwCYHAOO9XrNxyTfTfQXE6HIYf1qlfy+OBpDdlMI44xtACc0clwa1MhhvWKv0cdOHZZ5WAuPXG8ZI00clnlYMsx3VHMZIs0bFDU4hjH9bLCFNYd0UwXi3xhLSFOmmAMa5He0npg8LYhodxq8zHaObXtAnoMJ40xtpX9JmTqmeRoV5desuNc3cV1aYyS3kMPlmuhM2mN3iZlkCmgWlR5APc2ONZdpojWcujHObgSwxPT/pzIWZ3jhLQDOFwqxv2sfOZuWN2eTA+PWMKbGZ9ZjTdmZhvPE/wBLQjLNtZxZmfsNBGTdzXg6zffhXYB4y8TkDM/wnnCw0a1gGM+nUXUa4dSbFMP4tpzFpMxd+Icz0n3Gy0KxpEcxN55dZS2ecBIwzg+cxpmXpUsximDTVZw4VZgKeR7b0xWz8Pyk2ni3Aq6RMfUKF+QlkMd1hty9owVvGHL/fHUK7huZPGswE1lM23eVaEkgMtF4CcYheJwbjr0DPW6tsHiHBgHNC5goLzxjMK2gy2XqAT5hWsjVont40seGNBAbWVb4aC5QGH7FNYLqQkGiursYS0ED83rS6WRjYILmL68XxG8KNiJFzSETCqiaGWYMeXGIo+8Og1RRsrcFhj+EsQR94nYGBPGfqSfrQP6gGUoUbMtRDP37JDpKRJF/4ArOFpP5WMsLTPtriSc5mH9tx7b9AaDrbFMw95Ck9fkv7j4RauRj6E2u6bXwHFOuehAH1Y5Iktb+RhxLaph+/CJXTxP2aCGauAGCsWfSQc5DmY2ezD5E/+59ILaUxlTkO4/2GhA39dzS6s5eLEtr+/CbvOfhsd6VfOSrYGWYHSsv0r5c3tGSztJ6j7/bNUMtpzNUOg5mDOmUXGK29kSj/0NDmEmpOMOpdiavp8wTGgY0vxTBaWy6LqsZh8wyjlgjHOk/bnmB8WM8ugdG6LZmhuZWkWxeYMho/hpnCemUYTNBoSqNBSaOJwRQLx53GMEtYrw6HkaebRC8kTCGNubzAOMCJZQJGnm5wFhxGvSv45q4TwQD7/ikYrSsn0exqNJgi4ZzGAkIY6IRMCkaGbgi9ZGHoNNb4DOM9Asd10jDidUPqJQeGKhzz0TvBgMdkLzBJ76ktGiZm8Rr5MDSaUxKsAJNMHKb9JEc3mF6cQ5sCQwkDp2QzgPkKHWCOYZoJjUBPw3zMef5DhaHkNmHhFM2fQUd2Y5iWLYEGYwky2RYdJj8MhP0TRdsCm0wcBtnvwnWD6aVnqIUweTTmchvA7IAoOIyKEKYbWwAOshO9hH2YYpjcMLALYLrghWUYTPByoZ5G6CUsfwlMDo3bDWDgC2VwGLE0hF5OxS+DyYYBfaopDAsyCBiV0E3FMQGb0AsIJkNjjT3FAes/BUPUTRdVqBuEML1EhS+HSYcBc+kozgrKkoYR5WkZvUBhUsJZOUoDvoohDSOGJqsXMAxJ02kooAFzCgypm8LX0s3I6AUOQ9C4awXamcmFqa6bPL0wwODCcSfKD/iajByYqp6WqxcmmITG+qHcV4MhaVh7a1hfzCNYGGBiGuteGTKsh8iDqaSbfL0wwlyEYw6VUVUYUjcqQ90gNV8vrDARjTlSHkoIymF4dUPVCzNMlA0woNBh+HRD1ws7TNmAJwtMEGAx3QCnCFqYXrKpHSsMMw0dhl03RXrhgimfxQHDsOqG0EteVbLDMNIUwaR0U/biQr1wwrDRFMKQumkWv7ZZqBdeGFUcjIoMoG4IvVDCRd0wxChHgW4K25ebgVFRM1kpQm1v8LHx7h9aueqHaR2wnRM03SR6CRRzoP5U3TCoR+wC6ebVDToS8y9ej1J/dcMYvdQy1BzdZOYsnF5+aflgBCSaZ2s+Z3bnZNqbzFxS2M7kuiMPzEZAFyB6e8KSQHWbRN2gZhKT41rMX+/BAYM+qnfOol9K9NLH25sBNhBtD+JgF7Qv8d+5uuGBeancbT4bppf+EW9vts/H1qmNt1vHXgOrDXRMyHJ0wwPzt+qAxtkwvfQHiMgFvP7Ti9psqi9P/cQPQ89CST3l6IYDxv5bcagpejPGcopgSE1oTkdRE8dNR/kY2mA0mZ9khzHeqw0Cng3XSyQRPIdOW5yPIZuuGw6YVrfS8Gz0RQi9XAARjQarhQLdcMA0G1UGzqPfIPUSF5RSN14PH0Kk6oannXEqTGmczUjrJS5oKru5sBD9F0I3fCOayU85/JNN0U9k9ZIU6KOvpaw/yAxT5euGHcZ+87inAaN3YnrJZmL23Zc+HsX6X7Jj61jdOJgHssMYPY93gvZsFL0kJbU3h6fdidfZPR02eV3kfN2wwwRPcE6dRx+DphfMbHTc/Apsc0SUic9c3XBoZs+5qCF6vEAvxGtOVvD/HN0ww4QVzLfc5Py+Qr2wWI5umGHsT59vIdDpfWV6YaLJ6IYZxvjGuUQrhIHohYEmrRt2zbzzLZ47LTcB6gVMk9INKwzarPmWNQYw4vSSlIbQDTPMfx7fgtN2q3kQp5e4OJhuDk1WGPtN41sK3Ma1L24FLZZDB1GAEaY11/gWabefxeolpsF088wI0/Q1vuXzjXhjpCC9xDSJbnza6lnKkwNN49/YIFYvSZnSeTYQxujFMOxbTkTrJaY59vlgGjEM82Yg8XqJaew+B4z9kmwGYt2mFfmYlC2BNulpMBjjPGnHs4FOjl4uRuoGBIOOewyGcWtjyHInb2fTXUJz2tpYavYB39rIuOlUkl5imkQ3/ifIl6OpYb7twM6H1C209scl8wPtO0a/yO3ArBu13+Vu1I4n4UFrvuy/GgkD20KfnGL7fp0t9J+Qb9ZMb6GHHW5gXflwgz3ki51TGQLmJo+dAEmztc/A3OKBIO+Q1V5okD0Q5AaPasmde89Yso8Ag4FVzRUP0dmD8iX7JZ5cvOHjjbqw3A+fB7rZg6eAsT9uMFMwt3QkWHcAjPst/MtyHtY2ass8rK39Aj1uAG3wR3mP0XM3n4cvUuzwuTHA2YXRpsKwHHD4HdmGFLMZEvJwtJwGw3b05B34ndJsQ/axqxwKWjsN6mlFMGzHtdZMgzbFx7UyHqT7vVaY5rtWDMN4xHGdNPZLuuxVD5/+XpuroU1mlUHlY8Fro8lZziLgwPZ6aIxDdjGLiKP066BBg0a25EIuOaiBJnU8DR2G/fqJqwc1o5dXbkEXg1yZxn7LvetI1JUtVw1q6IPhyhaew86vSXPc55da4DVHV6PJpDFlMDwXUF2JpkVdmCv0arCrhIHcpZ8lMFyXtl2BhhLISmC4rtOTTmN8cF2nx3fRoeSgZhexiL+CUiqN/cF9BSXnfWfyaOzsQmkGGL5rW2XRGMX1IulCXTlhoFgvEBi+q45l0KC3ylcdc15CLZ6m1RNwCXV4PTgPjWDhNJ+EXA8e2GRRc4hGR1puyQ6j7eoNasYHJefngtG2PJdSiqIx3orvnmaF0ZypxeFqIlCQAZA+G4ymrTkucxRAYwzyxmGqwgR9AtB0tFAapB5yxscEwGjeHHS9DUlTSTj25h0Qkblggjgw7LBWjslPg5ovxYllNZhTk8OKw0uDNqDGpQJMkN0swHPSVWjsDTiI8cMELeiMlYY9DCDjk83DeGE0Z/LTZQtsjDSotWkzVwsnTGCTlcmkHRYaG/3iXJ3DCaP5ryOmlAAcohF6eeKplSowAU535DLgwEI0ag3aDK2kMJgAZ73s6HDxlNIguznY89ZKVZjAtvcLBSyeQhpkq7/+ArNjSTBB9cx/r6CXedLDgG0cD+9VKkUMTJCy7eZDC9azzqdBhvHyvmdJwuTBaOGCt+nSdS2zVEBpGoTsVnPQa4hZiCcGJjRnMn5cKHqJgrAQjWxD3fz3Nq/sXbGJg9FODvd1tlTcQqK7M0cLDT6/va8FOFdiQmFC87e77nS8XHUCt7PSbmealu62murmrdfe98VVSWTCYU7mOU5jPflxPxwRJ449jIb3PybrhuN4QmvkYv8HzA2XCFGEIIwAAAAASUVORK5CYII=">
			<%=user.getEmail() %>
			<br>
			<br>
			<img width="20" height="20" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMwAAADMCAMAAAAI/LzAAAAAflBMVEVYkar///9Gfpk7eJW6z9k/epZTjqg2dpP4+vtZkquzydRPjKeAprjt8vXQ2+GLrb7Y5Orp8POjwc9QhJ7g6u/B1uBhmK/K2uLc5+yPtMVsnbOZu8tzo7itx9OErL9lmrAmbo12n7NgjqWGprefvMqSr79wma5kkaiov8xWiKGTISxJAAAMzElEQVR4nNWd7YKqIBCG1QQ1s/Iry6y2PW1t93+DRzTLUpAZsdz3l6ezazzLMAwwgKYPINvzvMgMsmy1crSanNUqywIzyv/bHuJ7NdUvjOaJuUmp67qUai2ixX+lGzOZR6q/WymMFwfbzOFhvCI52TaIPZXfrw7GD2apQ7s56kTUSWeBr6wIamA8f69J1AenjrS9r6aCFMB4ye7gokDuQO5hlyjg6Q0T7fb537a3XLrf9XYIPWGS2QFnXU1Repgln4PxY6efeTV4XCfu4w7wMGvTUWBer3Idc/12mGibDoBS4KRbbONBwgQrpfb1LLoK3gfj71T4L5FcusO0HTiMnQxlYE84aQKPRcEw0cYZ0MIeos4G3HSgMMF7UEocaNOBwSwPb7Cwh9zDcjAYO9DeVi2lqBZAWg4AZr1/a7WUcveAPlQeZjpk18IXXU2Vw/iLD1RLKXch2+dIwiyzj7HkNJmkH5CDSQ4fMbFK9CA3NpCBeb8Xa9DIeTUJGHv7QROr5G4laLph/P2Hq6UU3Xe7gU4YLxsFS06TdU55dMF4n+ld2kRXXTQdMP54WBhNh6WJYZafdcmvoh2BpxDGHxcLoxHWjQhmRO2lkrjdCGBGyNJBw4fxx+KTn0UzvqVxYexx9JVN0T03FuDB2NuRsuQ03MiGBxOMIB7jyeVNdHBgkk8XWCzOiKAdZmSd5at4nWcrzEgd2UMcl9YK87nxvqzchSzMdPQsOU3bnE0LzHr16ZLKaNUyn9aEGW1v+ay2vrMJM+Yepq6W3qYBs/x0IeXV8M8NmHH3MHXRQxfMXzEypoahvcBETvc7xiMnEsHYmz9jZEx0Ywtgkj9VMXnVJHwYP/1TFZNXTepzYXZ/qPWXcndcmD9WL0yUB6PULdOb3JpuHyn8lmf3XIOJ1ASYrLjOKs2y2X4bBGZc0y4ItvtZlh5WmjKmVdQKo2AKI68IJ9ucdmay9rlzKJ6/jM2c6kBVZKvRbRvMMu37WpdmQbyM5JItbX+9NDcHtzdQ+hgLPGDMPi2G2dYmBueR23ZkZsi020qu2YTxe/SX1Em3sMSQJ6J4kfZZNHXufc0dJkZXDF1temVW5oqmC3yCkRs3YLAVQ+l2rWCXgrfcoFuP8wqT4CqGavCsMI7saI80NreK0CqYGeY9VNsr3WmxxOHQ2TNMdMC8JJXP0ZGTPUWFuofoCWaHeIertlpKRZg8MLqrw3iI6SW6UY/ChBgf0r1Xg0ngL3BMUYmYbD8yf2fn7+tXoev3efabRN27S6Zwx0qTGgx8ILOKReVh+1Cmi29iTQgx7iJkYpHv2XQu5rFjcMR7G9YUMB64+bvinCn/dLzWMYwnouvxV4wD78AP3h3Gh/6yK6wXb0uNdpI70PVX2NGCR1auf4cBN/+dqCRmKCQpcazLXPQOaFYY3d9hgCh0I7AS+7cbpcAJRTT+OQQWqoKBWtlKFCHHYgur0VxESb5mGGoQr1bYGYMJYFYm7GCiqxwKozmK3MCRGJBi0eAGA4zLXFHHv5hIwxgTUTSUWIZxBcDMShgPOF52BEVYA1jyZiOqGvYTAFNLvQImhvW4pePgaHZvMHl/0go2qfWjliiKOLMfC6ls4Zy4gIE2mS2/AHZYlTM8n4KfS5Pl8hOczvefughgFiWzrKmxRqOBp5hEMElVLedyHB2/spSdrX+uKkdgZzcYQ9JHs2JpegTMYBDB/FaFrnzE6cnUJqfbx1EFI+hr7q4klCofzaIcZg4MUkUwP2UhybH6wH/qdMh93uN4+1zQaBaPX73KFNGZ5zDQ0b8I5l9ZgMlv9YFX73bI9R6Q/ZZ/dnJqf88LjJSp5bGvBp78k4G5dyDetT4EON5hpjAYQ6IDdU1dA6/89YCZgWCePXunV6MbW/OgUwgCGPsIhfnhw2xeYryuDpSmnuZBw3+acr8/ukBhjvxhTTPI6ygpzWHAozp+bDa9fas0jHHljljXVgOmw9RcT4vAMNz01eWtYgAw3MjZO7aNJISm5kYaZiWjPX3V/ybEKvR1H4l6ofXQ18Oodl/FJ3mcdmxlsRecUZEgVnNNDRiZFaLZ3H6VP837kUW8LPT4c6+XNT3s0yv+vTZn4SSMvcbLzCt3hMc3NRpoqHRM6jrH2ZMuhmVIb0GsaXmcWMb5+V3n0BKMVrkdKM20DMHC5OZBfk15Q1mgVja8POgkL+KTFDQ8U8s0/BJz+PQV5II8OMYXF73V1NppVj1gHPfpC9DrgKbkDMhT5bTD9FjKdOqVg1/dsH8wNC3l7pvFVFUO+ddjKTD6BsOAJjtk5dCicojRa4V2Cq8a0GSHvFgMRZqDrFuP8Xiynz581hlDA5pXk1ReOS3B77roh44sFJiXj2yEvCsfG1OZkcT0dIsGyChz6LlpZHOLdRgWS9k3y0dWeYvysTny36FgZCc7QDQtCxzzIuglJQx7LGGKMrfAeEfI7GGdRnHDoW3DRSCMPudHY2IpNrVVm1eGwugnJIxaU2tfRQPD2PLLB6800lO4nXrNLa5gigHLF5ubN8tHBjMrH1un/pKWoaWkXEU0L8m4fYSIah6Vo4ZGXcbJ+hsNU5lavxriHp/gJYXWj0f2g+vHY4t+uwvNV25qTo8hABM3H2BuhLkMFhrE5SPzEz/lI2e63LvgDY15tT7jGU20vAn2ZsUv9YExjBV62FyIfw4ECqa28IbRATehUVUMv/XjYKI+LEaKmmqqWARb83Ew1VIHSmSLmgS8qS3AvMN88aPm9k6zkI8b2RSa7BDTs/eK4Z8ykDvhf4tc/5gdLstHNuExLR8FqRnT7kLzZEWIifNK4jSt2qDy9Uk0XWDjq2aCWNK4V0wmYkErxrcaD77YdIcRTpT5uykTq7yoeJqyieakeBKfXoruOS8efBmwYmmPlivNvya5rDJqZo9l1GwVj8JUszmyasjMhi/QVhInNiJds/5YS4RqMkUsnd8qZi+eWsbD3NM8oDAJIqmhVFcmcA8Y+x+uahJEuklZMV1jsh4wuoliuUSIRKASRrCAX8KAh821qjkjWNhEJDxFq5DbdYjqbUQW6U+Ds0g4OLsLE6Gx/Ah48lyhxlZ8pbIRjSY0UWmN2usuXPWCj2vIt4dKONU6ck4VCD7tRM46KhWYhWWD3IXzkBd2F/8FZqvjkrQFh3IpEjengSsr0nHp8/V9ngNpCvZnuo7c2EA7Oor+mgNRSHaHUboZQA0MsNFYjy0n0M1Ao4Mh9AED3aY1PpgyukJtoBsfTH0DHXBr49hgqhQ81KbT0cHcYnjUduCxwVzXTzCw+GxkMEVcVocBzQSMDMaqpolRhxuMDMaofgt17ATteR9ZtxJAoPnYH4U6EGT4qPkHEGiGzQNBQJOB7sB2BpnVJI8VrwfMGjB67jwHvp98QMYW+X78YZHHG9EsHszSbBMyd17PdcMePEWd48kcRLXNgjIK160wwI3r1JgMI9CIeVLfzdPjsLbnvOYPidg8GOjpE/TTKLWteg0Y8AGHn6a5jTBbYeBHTzrgCS61Ehw9iTkU9JM0r3nu/Y9r/aAbCF/WIhQcpPsxmsnrJksFRxw7n3ID19eyKzl8mn6m4TQGIkqOBXc+4QYmzZVIVQe2v52mbfOtqqP0HXSONVJhy5BK2SUHb47UrLaFSHXXT7zVqZFZW7lVXgzyPqdGWnbs8GDQV7a8i4azj1LxZTpvcgOc5FDV1xy9g8bi5boov4BqeDfA30St/GowZ2g3QGbc1JshLm0blIbjyDpgelynNyANCQXTj8NcdDhYNCA+Em2YKyiHigbIFX0FZZ/7zgZxA+QqTg0Z7tpW9TTC9iIB0+dSTdU04vYiA9PnqmO10YDIJ8vC9LmEWqVTm8wUXELd63pwdU7NWkgcnjLsxe3qYpuToovbcyX4C8NU0JBrx1HXIBh9maFNrT8NOUuuB0vC6D7+Wsq+Ts2SaPowGF2ffiZSI6F8Qrg8jL7GHNnP1McNkCMg5QAAg/dqPaZvpbwYBoYFntiWg6KZ8I8MVACj6wH2nhi4GyCh4MROJTB6tMHhQKdviSHaaKsIRreTFGVrsAHbhCbwfBY4DNtQSnE7COVRjF9MphEGJleA63Tk3EDeWHBZRkgYPdpijE0mbYB8/4AbS0+YvA81HQROlxuwwikWpQ9M3nZiB34DloiGWIbZJyuvD0yuZHYA3h/naDwScj3LRfpDweSNZ7eH+raWhkMIOZ7w9qUKhm0q3R1g5vZKQyx66tyJ+h4Ynd0qudcg9xW69SqxjMxXk7+qBobJD2apI3nn5y0aICT8Pm/V7flUB5PLi4Nt5rgydeSSiXH5OZkKjOshpTBM0TwxNyl1eUjFrbQ03ZjlLm6lUg7DZHueF5lBlq1WT/lrzmqVZYEZ5f89SFb0f9TZFNak+LgGAAAAAElFTkSuQmCC">
			<%=user.getPhone() %>
        </span>
		<br>

        <div id="change_pw_modal" class="modal">


            <div id="error-msg"></div>

            <span class="inputLabel">Old password <span class="inputErrorMsg" id="curPwErr"></span></span>
            <input type="password" name="currentPassword" id="curPw">
            <br>

            <span class="inputLabel">New password <span class="inputErrorMsg" id="pw1Err"></span></span>
            <input type="password" name="password" id="pw1">
            <br>

            <span class="inputLabel">New password <span class="inputErrorMsg" id="pw2Err"></span></span>
            <input type="password" name="password2" id="pw2">

            <div class="modal-footer">
                <button type="button" class="btn-flat waves-effect waves-light btn-card cancel modal-close">Cancel</button>
                <button type="button" onclick="changePw()" class="btn waves-effect waves-teal btn-card">Submit</button>
            </div>
        </div>
	</div>
    <%@include file="footer.jsp"%>
</body>
</html>
