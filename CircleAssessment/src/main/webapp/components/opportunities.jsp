<%@ page import="org.ce.assessment.platform.DataException" %>
<%@ page import="org.ce.assessment.platform.dashboard.Opportunities" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>


<%--
  requires session attributes:
  * assessmentId

--%>


<%
    String assessmentId_opp = (String) session.getAttribute("assessmentId");

    try {
        Map<String, List<String>> opportunities = new Opportunities().getForAssessment(assessmentId_opp);

        if (opportunities.isEmpty()) {
            %>There are no known opportunities. Fill out the assessment if you have not done it yet. <%
        }
        for (Map.Entry<String, List<String>> keyElement : opportunities.entrySet()) {
        %>
            <h4><%=keyElement.getKey()%></h4>
            <ul>
            <%
                for (String strategy : keyElement.getValue()) {
                    %><li><%=strategy%></li><%
                }
            %></ul><%
        }

    } catch (DataException e) {
%>
    <h3>Opportunities list could not be loaded due to a server error. We will fix it. Try again soon!</h3>
    <script>
        console.log('Error loading opportunities: <%=e.toString()%>');
    </script>
<%
    }
%>