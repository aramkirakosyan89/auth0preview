<%--Requires session attributes

    Organization teamTabOrganization
    boolean teamTabCanAdmin
--%>

<%@ page import="org.ce.assessment.platform.invites.Invite" %>
<%@ page import="org.ce.assessment.userdb.Organization" %>
<%@ page import="org.ce.assessment.userdb.User" %>
<%
    Organization teamOrganization = (Organization) session.getAttribute("teamTabOrganization");
    boolean canAdminOrganization = (boolean) session.getAttribute("teamTabCanAdmin");
%>

<% if (canAdminOrganization) { %>
<script>

    function resendUserInvite(inviteToken) {
        var userMsg = $("#user_msg_" + inviteToken);
        userMsg.html("Sending invitation email .. This may take a minute. We will let you know if something goes wrong.");

        var apiCallUrl = 'api/invites/'+inviteToken;
        $.post(apiCallUrl,
            {action: 'resend'},
            function(data, status) {
                userMsg.html("Invitation email has been sent again");
                setTimeout(function() {userMsg.html("");}, 2000);
            }
        )
            .fail(function(reason) {
                console.log(reason);
                if (reason.status === 400) {  // TODO: make this work
                    alert("Could not re-send an invitation. Bad request");
                } else {
                    alert("Could not re-send an invitation. Server error");
                }
                userMsg.html("");
            });
    }

    function withdrawUserInvite(inviteToken) {
        var userMsg = $("#user_msg_" + inviteToken);
        userMsg.html("Withdrawing invitation .. This may take a minute. We will let you know if something goes wrong.");
        var apiCallUrl = 'api/invites/'+inviteToken+'/withdraw/';

        $.ajax({
            method: 'post',
            url: apiCallUrl,
            data: {}
        })
            .done(function() {
                // TODO: show toaster
                location.reload(true);
            })
            .fail(function(x) {
                //TODO: show if bad request alert("Could not withdraw invitation. Bad request");
                console.log(x.responseText);
                alert("Could not withdraw invite " + email + ". Server error.");
            });
    }

    function makeAdmin(userId) {
        $.ajax({
            url: "api/organizations/<%=teamOrganization.id%>/admins/" + userId,
            method: 'post',
            data: {
            }
        })
            .done(function(data, status) {
                location.reload();
            })
            .fail(function(x) {
                console.log(x.responseText);
                alert("Could not make admin");
            });
    }

    function revokeAdmin(userId) {
        $.ajax({
            url: "api/organizations/<%=teamOrganization.id%>/admins/" + userId + "/revoke",
            method: 'post',
            data: {}
        })
            .done(function(x) {
                location.reload();
            })
            .fail(function(x) {
                console.log(x.responseText);
                alert("Could not revoke admin");
            });
    }

    function inviteTeammate() {
        const url = "<%=response.encodeURL("api/organizations/"+teamOrganization.id+"/users/invite/")%>";

        const data = {
            email: $("#new-teammate-email").val(),
            admin: $("#as-admin-checkbox").is(":checked")
        };

        $("#teammate-invite-process").show();
        $("#teammate-invite-form").hide();
        $("#teammate-invite-result").hide();

        $.ajax({
            url: url,
            method: "post",
            data: data
        })
            .done(function(x) {
                $("#teammate-invite-process").hide();
                $("#teammate-invite-result").show();

                resultMsg = 'Invited';
                if (x.responseText != null) {
                    resultMsg = x.responseText;
                }
                $("#teammate-invite-result-msg").html(resultMsg);

                setTimeout(function () {
                    location.reload(true)
                }, 1500);
            })
            .fail(function(x) {
                $("#teammate-invite-process").hide();
                $("#teammate-invite-result").show();
                $("#teammate-invite-result-msg").html("Error: " + x.responseText);
            });
    }

    function showTeammateInviteModal() {
        $("#teammate-invite-process").hide();
        $("#teammate-invite-result").hide();
        $("#teammate-invite-result-msg").html("");
        $("#teammate-invite-form").show();
        $("#teammate-invite-modal").modal('open');
    }

</script>

<% } %>


<div id="teammate-invite-modal" class="modal">
    <h5>Invite a teammate</h5>
    <div id="teammate-invite-form">
        <div class="modal-content">
            <div class="input-field">
                <input type="text" id="new-teammate-email">
                <label for="new-teammate-email">Email</label>
            </div>
            <p>
                <input type="checkbox" id="as-admin-checkbox">
                <label for="as-admin-checkbox">as admin</label>
            </p>
        </div>
        <div class="modal-footer">
            <button class="btn-flat waves-effect waves-light btn-card cancel modal-close">Cancel</button>
            <button onclick="inviteTeammate()" class="btn waves-effect waves-teal btn-card">Invite</button>
        </div>
    </div>
    <div id="teammate-invite-process" style="display: none">
        <div class="modal-content">
            <div class="progress">
                <div class="indeterminate"></div>
            </div>
            Inviting... This may take a minute
        </div>
        <div class="modal-footer">
            <button class="btn-flat waves-effect waves-light btn-card cancel modal-close">Close</button>
        </div>
    </div>
    <div id="teammate-invite-result" style="display: none">
        <div class="modal-content" id="teammate-invite-result-msg">
        </div>
        <div class="modal-footer">
            <button class="btn-flat waves-effect waves-light btn-card cancel modal-close">Close</button>
        </div>
    </div>
</div>



<div id="team-tab" class="col s12">
    <div class="container body">
    <%
        if (teamOrganization == Organization.NONE) {
    %>
        Unknown organization. If you are a global admin - set the community admin organization in the settings.
    <%
        } else {
    %>

    <table class="highlight">
        <% if (canAdminOrganization) { %>
        <thead>
        <tr>
            <td colspan="2">
                <button onclick="showTeammateInviteModal()" class="btn grey lighten-3 black-text">
                    Invite
                </button>
            </td>
        </tr>
        </thead>
        <% } %>
        <%
            for (User u : teamOrganization.getUsers()) {
        %>
        <tr>
            <td class="nowrap-column">
                <%=u.getName()%>
                <% if (u.isOrganizationAdmin()) { %>
                <em style="font-weight: lighter">admin</em>
                <% } %>
                <br><span style="font-weight: lighter"><%=u.getJobTitle()%></span>
            </td>
            <td width="100%">
                <% if (u.isOrganizationAdmin()) {
                    if (canAdminOrganization) { %>
                <button class="btn-flat white grey-text waves-effect waves-teal" onclick="revokeAdmin('<%=u.getId()%>')">
                    Revoke admin
                </button>
                <%
                    }
                } else {
                    if (canAdminOrganization) { %>
                <button class="btn-flat white grey-text waves-effect waves-teal" onclick="makeAdmin('<%=u.getId()%>')">
                    Make admin
                </button>
                <%
                        }
                    }
                %>
            </td>
        </tr>
        <%
            }
        %>
        <%
            if (canAdminOrganization) {
                for (Invite invite : teamOrganization.getInvites()) {
        %>
        <tr>
            <td class="nowrap-column">
                <em style="font-weight: lighter">Invited</em><br>
                <%=invite.getEmail()%>
            </td>
            <td width="100%">
                <button class="waves-effect waves-teal btn-flat white grey-text" onclick="resendUserInvite('<%=invite.token%>')">Resend</button>
                <button class="waves-effect waves-teal btn-flat white grey-text" onclick="withdrawUserInvite('<%=invite.token%>')">Withdraw</button>
                <span id="user_msg_<%=invite.token%>"></span>
            </td>
        </tr>
        <% 	    } %>
        <%  } %>
    </table>
    <%
    }
    %>
    </div>
</div>

