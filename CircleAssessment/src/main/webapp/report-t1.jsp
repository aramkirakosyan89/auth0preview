<%@ page import="org.ce.assessment.limeapi.answeroptions.CheckedAnswerOption" %>
<%@ page import="org.ce.assessment.limeapi.answeroptions.MultipleChoiceAnswerOptionWithComment" %>
<%@ page import="org.ce.assessment.platform.AssessmentSection" %>
<%@ page import="org.ce.assessment.platform.AssessmentSections" %>
<%@ page import="org.ce.assessment.platform.report.*" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    String sectionId = request.getParameter("sectionId");
    AssessmentSection section = new AssessmentSections().getById(sectionId);

    String sectionDiv = section.getName().replace(" ", "_") + "_div";
    LetterScore reportScore = LetterScore.getForScore(section.getScore());

%>

<div class="row section-page" id="<%=sectionDiv%>">
    <div class="col s1 section-nav print-hide">
        <a href="javascript: showSection(<%=section.getPrevId()%>)"> &lt; </a>
    </div>
    <div class="col s10 section">
        <div class="row t1-name">
            <%=section.getName()%>
            <div class="score-box <%=reportScore.cssClass%>" title="<%=reportScore.getScoreTitle()%>" style="float: right; font-size: 2em" >
                <%=reportScore.letter%>
            </div>
        </div>


<%
    Report report = section.getReport();
    if (report.isSurveyCompleted()) {
%>
        <a class="btn grey lighten-3 black-text survey-button no-print" href="<%=response.encodeURL("survey.jsp?section-id=" + section.getId())%>">Edit survey</a>
<%  } else { %>
        <a class="btn grey lighten-3 black-text survey-button no-print" href="<%=response.encodeURL("survey.jsp?section-id=" + section.getId())%>">Take survey</a>
        <span class="print-tip">Assessment for this section is not completed</span>
<%  } %>

<%
    if (report.isSurveyCompleted()  && reportScore.getScore() > -1) { //0
%>
        <div class="tip no-print">Click on a scorecard to see full report</div>
<%
        for (Section reportSection : report.sections.values()) { //1
            SevenElementsSection t2Section = (SevenElementsSection) reportSection;
            LetterScore sectionScore = LetterScore.getForScore(-1);
            if (t2Section.isEnabled()) {
                sectionScore = LetterScore.getForScore(t2Section.getScore());
            }
%>
    <div class="t2-scorecard <%=sectionScore.cssClass%>" title="<%=sectionScore.getScoreTitle()%>"
         <% if (t2Section.isEnabled()) { %>
         onclick="showT2Report('<%=sectionId%>', '<%=t2Section.getName()%>')"
         <% } %>
    >
        <div class="row scorecard-header">
            <div class="col s10">
                <%=t2Section.getName()%>
            </div>
            <div class="col s2 score-box">
                <%=sectionScore.letter%>
            </div>
        </div>
        <%
            if (t2Section.isEnabled()) { // 2
        %>
        <table>
            <tr>
                <td colspan="2" class="scorecard-section">
                    <h4 class="part-header">Strategies</h4>
                    <%      for (Strategy strategy : t2Section.getStrategies()) { // 3
                        if (!strategy.isEnabled()) {
                            continue;
                        }
                    %>
                    <div class="row">
                        <div class="col s7 scorecard-strategies-name">
                            <%=strategy.name%>
                        </div>
                        <div class="col s5 scorecard-strategies-value">
                            <%
                                Strategy.EngagementLevel value = strategy.getEngagementLevel();
                                if (value.value == 1) {
                            %><div class="value-gauge gauge-empty"></div><%
                        } else {
                            for (int i = 0; i<value.value; i++) {
                        %><div class="value-gauge gauge-full"></div><%
                                }
                            }
                        %>
                            <%=value.name%>
                        </div>
                    </div>
                    <%      } //e3 %>
                </td>
            </tr>
            <tr>
                <td class="scorecard-section">
                    <h4 class="part-header">Key challenges</h4>
                    <%
                        MultipleChoiceSector keyChallenges = t2Section.getKeyChallenges();
                        List<CheckedAnswerOption> selectedOptions = keyChallenges.getSelectedOptions();
                    %>
                    <ul>
                        <%
                            for (CheckedAnswerOption option : selectedOptions) {
                        %>
                        <li class="option">
                            <%=option.getText()%>
                            <% if (option instanceof MultipleChoiceAnswerOptionWithComment) {%>
                            <span class="option-comment">
                    <%=((MultipleChoiceAnswerOptionWithComment) option).getComment()%>
                    </span>
                            <% } %>
                        </li>
                        <%
                            } // eo for option
                        %>
                    </ul>
                </td>
                <% if (t2Section.getQuant() != null) {%>
                <td style="width: 12em" class="scorecard-section">
                        <h4 class="part-header">% of Products Affected</h4>
                        <%=t2Section.getQuant().value()%>
                </td>
                <%}%>
            </tr>

        </table>
        <%--<div class="scorecard-section">--%>
            <%--<h4 class="part-header">Strategies</h4>--%>
        <%--<%      for (Strategy strategy : t2Section.getStrategies()) { // 3--%>
                    <%--if (!strategy.isEnabled()) {--%>
                        <%--continue;--%>
                    <%--}--%>
        <%--%>--%>
            <%--<div class="row">--%>
                <%--<div class="col s7 scorecard-strategies-name">--%>
                    <%--<%=strategy.name%>--%>
                <%--</div>--%>
                <%--<div class="col s5 scorecard-strategies-value">--%>
                <%--<%--%>
                    <%--Strategy.EngagementLevel value = strategy.getEngagementLevel();--%>
                    <%--if (value.value == 1) {--%>
                <%--%><div class="value-gauge gauge-empty"></div><%--%>
                    <%--} else {--%>
                        <%--for (int i = 0; i<value.value; i++) {--%>
                <%--%><div class="value-gauge gauge-full"></div><%--%>
                        <%--}--%>
                    <%--}--%>
                <%--%>--%>
                    <%--<%=value.name%>--%>
                <%--</div>--%>
            <%--</div>--%>
        <%--<%      } //e3 %>--%>
        <%--</div>--%>
        <%--<div class="scorecard-section">--%>
            <%--<h4 class="part-header">Key challenges</h4>--%>
            <%--<%--%>
                <%--MultipleChoiceSector keyChallenges = t2Section.getKeyChallenges();--%>
                <%--List<CheckedAnswerOption> selectedOptions = keyChallenges.getSelectedOptions();--%>
            <%--%>--%>
            <%--<ul>--%>
            <%--<%--%>
                <%--for (CheckedAnswerOption option : selectedOptions) {--%>
            <%--%>--%>
                <%--<li class="option">--%>
                    <%--<%=option.getText()%>--%>
                    <%--<% if (option instanceof MultipleChoiceAnswerOptionWithComment) {%>--%>
                    <%--<span class="option-comment">--%>
                        <%--<%=((MultipleChoiceAnswerOptionWithComment) option).getComment()%>--%>
                        <%--</span>--%>
                    <%--<% } %>--%>
                <%--</li>--%>
            <%--<%--%>
                <%--} // eo for option--%>
            <%--%>--%>
            <%--</ul>--%>
        <%--</div>--%>
        <%  } // e2 %>

    </div>

    <%
            } //e1 for report sections
        }   // e0 if survey is complete
    %>
    </div>
    <div class="col s1 section-nav print-hide">
        <a href="javascript: showSection(<%=section.getNextId()%>)"> &gt; </a>
    </div>
</div>

