
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>

<style>
    div.gauge-container {
        background-color: #fafaff;
        border-bottom: thin solid hsla(240, 14%, 91%, 1);
        box-shadow: 0 3px 3px #eee;
        margin: 20px;
        padding: 20px;
        display: block;
        min-width: 550px;
        height: 600px;
        text-align: center;
    }

    .tt-desc {
         display: block;
         width: 300px;
         background-color: #fffeec;
         border: thin solid grey;
         color: #263238;
         text-align: left;
         padding: 1em;
         font-size: .9em;
         font-weight: 400;
         font-family: 'Montserrat', sans-serif;
     }

    .material-tooltip {
        padding: 0 !important;
    }

    #nOrganizations {
        text-align: center;
        font-size: .8em;
        margin: 3em;
    }
        #nOrganizations h4 {
            font-weight: bold;
            margin: 0;
            line-height: 100%;
            font-size: 1.2em;
        }



</style>

<script>
    function loadDashboard() {
        showNOrganizations();
        loadMMA();
        loadOpportunities();
        loadChallenges();
    }

    function showLoader() {
        showLoaderMMA();
        showLoaderOpportunities();
        showLoaderChallenges();
    }

    function showNOrganizations() {
        $.get(
            'api/dashboard/<%=session.getAttribute("dashboardCommunityAssessmentId")%>/nOrganizations',
            {},
            function(d) {
                $('#nOrganizations').html('' +
                    '<h4>Aggregated results for  ' + d + ' organizations within <%=(String) session.getAttribute("dashboardCommunityName")%></h4> ' +
                    'Note: not all organizations may have fully completed the assessment'
                );
            }
        );
    }

    $(()=>loadDashboard());
</script>


<%
    if ((Boolean) session.getAttribute("dashboardCanUpdate")) {
%>
<script>
    function update() {
        showLoader();

        $.ajax({
            method: 'post',
            url: 'api/dashboard/<%=session.getAttribute("dashboardCommunityAssessmentId")%>/update',
            data: {}
        })
            .done(function() {
                loadDashboard();
            })
            .fail(function(x) {
                console.log(x.responseText);
                alert("Could not update dashboard. Server error.");
            });
    }
</script>


<button class="btn-flat" type="button" onclick="update()">Update</button>
<br/>

<% } %>

<div id="nOrganizations"> </div>

<div id="gauges-container">
    <%@include file="dashboard/minmaxavg.jsp"%>
    <%@include file="dashboard/opportunities.jsp"%>
    <%@include file="dashboard/challenges.jsp"%>
</div>