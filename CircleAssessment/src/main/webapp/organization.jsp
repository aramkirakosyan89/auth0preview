<%@ page import="org.ce.assessment.fashiontool.WasteMapping" %>
<%@ page import="org.ce.assessment.platform.Assessment" %>
<%@ page import="org.ce.assessment.platform.AssessmentSection" %>
<%@ page import="org.ce.assessment.platform.Communities" %>
<%@ page import="org.ce.assessment.platform.Community" %>
<%@ page import="org.ce.assessment.platform.api.WebappApi" %>
<%@ page import="org.ce.assessment.platform.invites.Invites" %>
<%@ page import="org.ce.assessment.platform.report.LetterScore" %>
<%@ page import="org.ce.assessment.platform.report.SectorInterface" %>
<%@ page import="static org.ce.assessment.platform.api.WebappApi.Tab.ASSESSMENTS" %>
<%@ page import="static org.ce.assessment.platform.api.WebappApi.Tab.PROFILE" %>
<%@ page import="static org.ce.assessment.platform.api.WebappApi.Tab.*" %>
<%@ page import="org.ce.assessment.userdb.OrganizationProfile" %>
<%@ page import="org.ce.assessment.userdb.Organizations" %>
<%@ page import="java.util.List" %>

<%
    AuthManager mgr = new AuthManager();
    User user = mgr.getAuthedUserWithException(request);

    String organizationId = request.getParameter("id");
    Organization organization = new Organizations().getById(organizationId);
    if (organization == null) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Could not find organization with id = " + organizationId);
        return;
    }

    if (!user.canSeeOrganization(organization)) {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        return;
    }

    WebappApi.Tab curTab = (WebappApi.Tab) session.getAttribute(WebappApi.ORGANIZATION_CUR_TAB);
    if (curTab == null) {
        curTab = ASSESSMENTS;
    }

    String showTab = request.getParameter("show");
    if ("profile".equals(showTab)) {
        curTab = PROFILE;
    } else if ("team".equals(showTab)) {
        curTab = TEAM;
    } else if ("waste".equals(showTab)) {
        curTab = WASTE;
    }


    // TODO: remove this

    /*
     * This is temporarily integrated in the assessment platform,
     * but it should be a separate tool, and part of the Fashion Tool
     */
    boolean isFashion = false;
    {
        List<Community> communities = new Communities().getWithOrganization(organizationId);
        for (Community community : communities) {
            if ("18".equals(community.id)) {
                isFashion = true;
            }
        }
    }

%>

<%@ page contentType="text/html; charset=ISO-8859-1"
         pageEncoding="UTF-8"%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="main-style.css" rel="stylesheet" type="text/css">
	<title>Circle Assessment - <%=organization.getName()%></title>
</head>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>

<script src="charts.js"></script>


<%@include file="includes.jsp" %>

<% if (user.canAdminOrganization()) { %>
<script>

    function joinCommunity(inviteToken) {
        const apiUrl = 'api/invites/'+inviteToken + '/';
        $.post(apiUrl,
            {action: 'accept'},
            ()  => {
                location.reload();
                //TODO: show popup that joined
            }
        )
        .fail(function(reason) {
            console.log(reason);
            if (reason.status === 400) {
                alert("Could not join community without invitation");
            } else {
                alert("Could not join community due to a server error. Try again later");
            }
            location.reload();
        });
    }

    function leaveCommunity(communityId) {
        const url = 'api/communities/'+communityId+"/members/"+<%=organizationId%> + "/remove/";
        $.ajax({
            method: 'post',
            url: url,
            data: {}
        })
            .done(function () {
                location.reload(true);
            })
            .fail(function (t) {
                alert(t.responseText);
            })
    }

    function setTab(tab) {
        $.post(
            "<%=response.encodeURL("api/webapp/organization-tabs/current")%>",
            {
                tab: tab
            }
        ).fail(function (x) {
            alert(x.responseText);
        })
    }

    $(function() {
        setTab('<%=curTab.name()%>');
    })
</script>

<% } %>

<body>
    <%@include file="header.jsp" %>
    <div class="container breadcrumbs">
    <%
        if (user.isAdmin()) {
    %>
        <a href="<%=response.encodeURL("communities.jsp")%>">
            Home >
        </a>
    <%
            String communityId = (String) session.getAttribute("communityId");
            Community parentCommunity;
            if (communityId != null) {
                parentCommunity = new Communities().getById(communityId);
    %>
        <a href="<%=response.encodeURL("community.jsp?id="+communityId)%>">
            <%=parentCommunity.getName()%> >
        </a>
    <%      }
        }
        else if (user.isCommunityAdmin()) {
    %>
        <a href="<%=response.encodeURL(mgr.getUserHome(request))%>">
            Home >
        </a>
    <% } %>
    </div>

    <h2>
        <%=organization.getName()%>
    </h2>

    <ul class="tabs">

    <% if (isFashion) { %>
        <li class="tab col s1">
            <a href="#waste-tab" <%=(curTab == WASTE)?"class=\"active\"":""%> onclick="setTab('<%=WASTE.name()%>')">
                Waste Mapping
            </a>
        </li>
    <% } %>

        <li class="tab col s1">
            <a href="#assessments-tab" <%=(curTab == ASSESSMENTS)?"class=\"active\"":""%> onclick="setTab('<%=ASSESSMENTS.name()%>')">
               Assessments
            </a>
        </li>
    <% if (user.isAdmin()) { %>
        <li class="tab col s1">
            <a href="#communities-tab"  <%=(curTab == COMMUNITIES)?"class=\"active\"":""%> onclick="setTab('<%=COMMUNITIES.name()%>')">
                Communities
            </a>
        </li>
    <% } %>
        <li class="tab col s1">
            <a href="#team-tab"  <%=(curTab == TEAM)?"class=\"active\"":""%> onclick="setTab('<%=TEAM.name()%>')">
                Team
            </a>
        </li>
        <li class="tab col s1">
            <a href="#profile-tab"  <%=(curTab == PROFILE)?"class=\"active\"":""%> onclick="setTab('<%=PROFILE.name()%>')">
                Profile
            </a>
        </li>
    </ul>


<% if (isFashion) { %>
    <div id="waste-tab" class="col s12">
        <div class="container body">
            <%
                WasteMapping wasteMapping = WasteMapping.getForOrganization(organization);
                if (wasteMapping.isSurveyCompleted()) {
            %>
            <a class="btn grey lighten-3 black-text" href="<%=response.encodeURL("waste-mapping.jsp?id="+organizationId)%>">
                Edit survey
            </a>
            <%

                for (SectorInterface sector : wasteMapping.sectors) {
            %>
            <p>
                <%=sector.getName()%><br>
                <b><%=sector.value()%></b>
            </p>
            <%
                }
            } else {
            %>
            <a class="btn grey lighten-3 black-text" href="<%=response.encodeURL("waste-mapping.jsp?id="+organizationId)%>">
                Fill out survey
            </a>
            <% } %>
        </div>
    </div>
<% } %>

    <div id="assessments-tab" class="col s12">
        <div class="container body">
            <div class="row">
            <%
                List<Assessment> assessments = organization.getAssessments();
                int i = 0;
                for (Assessment a : assessments) {
                        ++i;
            %>
                <div class="col s12 m6 l4">
                    <a href="<%=response.encodeURL("assessment.jsp?id=" + a.getId())%>">
                    <div class="card assessment-card" style="text-align: center; padding: 20px 10px; font-weight: bold">
                            <div id="assessmentChart<%=i%>"></div>
                            <%=a.getName()%>
                    </div>
                    </a>
                </div>

                <script>
                    reportSections = [];
                    data = [
                <%
                    String comma = "";
                    for (AssessmentSection section : a.getSections()) {
                        int score = section.getScore();
                        if (score == LetterScore.NOT_APPLICABLE) {
                %>
                        <%=comma%>{y: 101, name:'<%=section.getName()%>', color:'#fff'}
                <%
                    } else {
                %>
                        <%=comma%>{y: <%=score%>, name:'<%=section.getName()%>', color: getColorForStyle('<%=LetterScore.getForScore(score).cssClass%>')}
                <%
                        }
                        comma = ",";
                    }
                %>
                    ];

                    drawSmallChart("assessmentChart<%=i%>", data, reportSections);

                </script>
            <% } %>
            </div>
        </div>
    </div>

<% if (user.isAdmin()) { %>
    <div id="communities-tab" class="col s12">
        <div class="container body">
            <table class="highlight">
            <%
                List<Community> communities = new Communities().getWithOrganization(organizationId);
                for (Community community  : communities) {
            %>
                <tr>
                    <td class="nowrap-column">
                        <a href="<%=response.encodeURL("community.jsp?id="+community.id)%>">
                            <%=community.getName()%>
                        </a>
                    </td>
                    <td width="100%">
                        <% if (user.canAdminOrganization()) { %>
                            <button type="button" class="btn-flat white grey-text" onclick="leaveCommunity('<%=community.id%>')">
                                Leave
                            </button>
                        <% } %>
                    </td>
                </tr>
            <%  }

                if (user.canAdminOrganization()) {

                    List<Invite> invites = new Invites().getCommunityInvitesForOrganization(organization.id);
                    for (Invite invite : invites) {
                        Community community = invite.getCommunity();
            %>
                <tr>
                    <td class="nowrap-column">
                        <a href="<%=response.encodeURL("community.jsp?id="+community.id)%>">
                            <b><%=community.getName()%></b>
                        </a>
                        <em>invited</em>
                    </td>
                    <td width="100%">
                        <button type="button" class="btn-flat white grey-text"
                                onclick="joinCommunity('<%=invite.token%>')">
                            Join
                        </button>
                    </td>
                </tr>
            <%      }
                }
            %>
            </table>
        </div>
    </div>
<% } %>

<%
    session.setAttribute("teamTabOrganization", organization);
    session.setAttribute("teamTabCanAdmin", user.canAdminOrganization());
%>
    <%@include file="components/tabs/team.jsp"%>

    <div id="profile-tab" class="col s12">
        <div class="container body">
        <%
            OrganizationProfile profile = organization.getProfile();
            if (profile.isSurveyCompleted()) {
                if (user.canAdminOrganization()) {
        %>
            <a class="btn grey lighten-3 black-text" href="<%=response.encodeURL("edit-org-profile.jsp?id="+organizationId)%>">
                Edit Profile
            </a>
        <%
                }
                for (SectorInterface sector : profile.sectors) {
        %>
            <p>
                <%=sector.getName()%><br>
                <b><%=sector.value()%></b>
            </p>
        <%
                }
            } else if (user.canAdminOrganization()) {
        %>
            <a class="btn grey lighten-3 black-text" href="<%=response.encodeURL("edit-org-profile.jsp?id="+organizationId)%>">
                Fill out profile
            </a>
        <% } %>
        </div>
    </div>

    <%@include file="footer.jsp"%>
</body>
</html>
