function validatePasswords(fpw1, fpw2, btn) {
		fpw1.removeClass("valid");
		fpw2.removeClass("invalid");
		
		var pw1 = fpw1.val();				
	    if (pw1 == null || pw1 == "") {
	    	fpw1.addClass("invalid");
	    	btn.prop('disabled', true);
	    	return false;
	    } else {
	    	fpw1.removeClass("invalid");
	    	btn.prop('disabled', false);
	    }
		
	    var pw2 = fpw2.val();
    	if (pw1 != pw2) {		    		
    		if (pw2 != "") {				
				fpw2.addClass("invalid");
    		} else {    			
    			fpw2.removeClass("invalid");
    		}		    		
    		btn.prop('disabled', true);
    		
	        return false;
	    }
    	
    	fpw2.removeClass("invalid");
    	fpw2.addClass("valid");
    	btn.prop('disabled', false);
    	return true;
	}