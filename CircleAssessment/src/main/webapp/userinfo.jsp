
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Circle Assessment</title>
    <link rel="icon" type="image/png" href="images/favicon.png">
</head>

<%@include file="includes.jsp" %>


<style>

    /* Fix for auto fill */
    input:-webkit-autofill + label {
        -webkit-transform: translateY(-14px) scale(0.8);
        transform: translateY(-14px) scale(0.8);
        -webkit-transform-origin: 0 0;
        transform-origin: 0 0;
    }

    .login-form {
        display: inline-block;
        position: relative;
        right: 0;
        margin: 30px -10px;
        max-width: 400px;
        padding: 20px 40px 40px 40px;
        border: 1px solid #ddd;
        background-color: #fff;
    }
    .login-form a {
        color: #263232;
        font-size: .8em
    }

    .login-form a#reset_pw_link {
        margin-right:30px;
    }

    ul {
        padding-left: 2em !important;
        margin: 0 0 1em 0 !important;
    }
    li {
        list-style-type: disc !important;
    }

</style>

<script>

    function save() {
        var name = $("#name").val();

        $.ajax({
            url: '<%=response.encodeURL("api/users/save/")%>',
            method: 'post',
            data: {name: name}
        })
            .done(
                function(x) {
                    location.href = x;
                })
            .fail(function(x) {
                if (x.status === 400) {
                    alert(x)
                } else if (x.status === 401) {
                    alert(x)
                } else {
                    alert(x);
                }
            });

        return false;
    }

</script>


<body>


<%@include file="header.jsp" %>


<div class="body-container">
    <br>
    <h1 style="color: black">Please enter your name</h1>
    <form method="post" id="nameForm" class="boxed">
        <input name="name" id="name" type="text">

        <button type="button" class="wide" onclick="save()">Save</button>
    </form>

</div>


<%@include file="footer.jsp"%>
</body>
</html>