<%@include file="includes.jsp"%>

<script>
    {
        let parentHost = null;

        const c = function(event) {
            parentHost = event.data.host;
            parent.postMessage({action: 'scrollup'}, parentHost);
            parent.postMessage(
                {
                    action: 'resize',
                    height: 600
                }
                , parentHost);
            parent.postMessage({action: "complete"}, parentHost);
        };

        if (window.addEventListener) {
            window.addEventListener("message", c, false);
        } else if (window.attachEvent) {
            window.attachEvent("onmessage", c);
        }
    }
</script>


<%@ page contentType="text/html;charset=UTF-8"%>
<body style="background-color: white;">
    <div class="container white">
        Your responses were successfully saved. Taking you back to the assessment.
        <div class="progress">
            <div class="indeterminate"></div>
        </div>
    </div>
</body>