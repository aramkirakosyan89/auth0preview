<%@include file="../includes.jsp"%>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>

<div id="opportunities-container" class="gauge-container">
    <br/>
    <br/>

    <div class="preloader-wrapper small active">
        <div class="spinner-layer spinner-green-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>
</div>

<script>

    const loader_opp = '<div class="preloader-wrapper small active">\n' +
        '        <div class="spinner-layer spinner-green-only">\n' +
        '            <div class="circle-clipper left">\n' +
        '                <div class="circle"></div>\n' +
        '            </div>\n' +
        '            <div class="gap-patch">\n' +
        '                <div class="circle"></div>\n' +
        '            </div>\n' +
        '            <div class="circle-clipper right">\n' +
        '                <div class="circle"></div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>';

    function showLoaderOpportunities() {
        $("#opportunities-container").html(loader_opp);

    }

    function drawOpportunities(container, data) {
        Highcharts.chart(container, {
            chart: {
                type: 'bar'
            },

            exporting: {enabled: false},
            credits: {enabled: false},

            title: {
                text: 'Key Opportunities by 7 Key Elements. <i id="opportunities-tooltip" class="tiny material-icons orange-text">info</i>',
                useHTML: true
            },
            subtitle: {
                text: 'Click the bars to view strategies.'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total number of mentions'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> mentions<br/>'
            },

            "series": [
                {
                    "name": "Key Elements",
                    "colorByPoint": true,
                    "data": data.t1
                }
            ],
            "drilldown": {
                drillUpButton: {
                    relativeTo: 'spacingBox',
                    position: {
                        y: 0,
                        x: 0
                    },
                    theme: {
                        fill: 'white',
                        'stroke-width': 1,
                        stroke: 'silver',
                        r: 4,
                        states: {
                            hover: {
                                fill: '#a4edba'
                            },
                            select: {
                                stroke: '#039',
                                fill: '#a4edba'
                            }
                        }
                    }

                },
                "series": data.drilldown
            }
        });

        $('#opportunities-tooltip').tooltip(
            {
                tooltip: "<div class='tt-desc'>" +
                    "<p>In this overview you can see which of the 7 key elements and circular strategies " +
                    "are considered key opportunities by the industry. For every key element you can see " +
                    "how many times the circular strategies in that element have been mentioned as an " +
                    "opportunity*. When you click on one of the key element bars you will drill down to " +
                    "see the how many times the various circular strategies have been mentioned as an opportunity. </p>" +
                    "</p>*Strategies that have been marked with either one of the following levels of engagement " +
                    "are considered a key opportunity in this benchmark: Currently no development & Currently " +
                    "exploring. When strategies are piloted, partially or fully operational they are not counted " +
                    "as key opportunities since they are already quite far advanced in their implementation.</p>" +
                    "</div>",
                delay: 20,
                html: true,
                position: 'right'
            }
        );
    }

    <%
    String communityAssessmentId_opportunities = (String) session.getAttribute("dashboardCommunityAssessmentId");
    String apiUrl = "api/dashboard/" + communityAssessmentId_opportunities + "/opportunities";
    %>
    function loadOpportunities() {
        $.get(
            '<%=apiUrl%>',
            {},
            function (r) {
                const data = JSON.parse(r);
                drawOpportunities("opportunities-container", data);
            }
        )
    }

</script>
