<%@include file="../includes.jsp"%>

<%
    String communityAssessmentId_challenges =
            (String) session.getAttribute("dashboardCommunityAssessmentId");
%>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>

<div id="challenges-container"  class="gauge-container">
    <br/>
    <br/>

    <div class="preloader-wrapper small active">
        <div class="spinner-layer spinner-green-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>
</div>

<script>

    let loader_ch = '<div class="preloader-wrapper small active">\n' +
        '        <div class="spinner-layer spinner-green-only">\n' +
        '            <div class="circle-clipper left">\n' +
        '                <div class="circle"></div>\n' +
        '            </div>\n' +
        '            <div class="gap-patch">\n' +
        '                <div class="circle"></div>\n' +
        '            </div>\n' +
        '            <div class="circle-clipper right">\n' +
        '                <div class="circle"></div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>';

    function showLoaderChallenges() {
        $("#challenges-container").html(loader_ch);
    }

    function showChallenges(allData, data) {

        const {t1} = allData;

        const title = 'Key Challenges ' + ((data.drilldown === true)?' by 7 Key Elements':'to ' + data.name)
            + ' <i id="challenges-tooltip" class="tiny material-icons orange-text">info</i>';

        const subTitle = ((data.drilldown === true)
            ? "Click the bars to drill down"
            : "<button id=\"drillUp_ch\">< Back to Key Elements</button>") ;


        const chart = Highcharts.chart('challenges-container', {

                exporting: {enabled: false},
                credits: {enabled: false},
                chart: {
                    type: 'bar',
                    cursor: 'pointer'
                },
                title: {
                    text: title,
                    useHTML: true
                },
                subtitle: {
                    text: subTitle,
                    useHTML: true
                },
                xAxis: {
                    categories: data.categories,
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total number of mentions'
                    }
                },
                legend: {
                    reversed: true
                },
                plotOptions: {
                    series: {
                        stacking: 'normal',
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    if (data.drilldown === true) {
                                        showChallenges(allData, allData.byElement[this.x]);
                                    } else {
                                        showChallenges(allData, t1);
                                    }
                                }
                            }
                        }
                    }
                },
                series: data.series
            },
            function(chart) {
                $("#drillUp_ch").on("click", ()=>showChallenges(allData, t1));
            }

        );

        chart.xAxis[0].labelGroup.element.childNodes.forEach(function(label)
        {
            label.style.cursor = "pointer";
            label.onclick = function() {
                var found = false;
                for (var i in allData.byElement) {
                    const t2 = allData.byElement[i];
                    if (t2.name === this.textContent) {
                        showChallenges(allData, allData.byElement[i]);
                        found = true;
                    }
                }
                if (!found) {
                    showChallenges(allData, allData.t1);
                }
            }
        });

        $('#challenges-tooltip').tooltip(
            {
                tooltip: "<div class='tt-desc'>" +
                    "<p>In this overview you can see which challenges Circle Assessment " +
                    "users experience when engaging in the 7 key elements and the 19 Strategy groups.</p>" +
                    "<p>Click on the chart bars to drill down/up.</p>" +
                    "</div>",
                delay: 20,
                html: true,
                position: 'right'
            }
        );

    }

    <%
    String path = "api/dashboard/" + communityAssessmentId_challenges + "/challenges";
    %>

    function loadChallenges() {
        $.get(
            '<%=path%>',
            {},
            function (r) {
                const data = JSON.parse(r);
                showChallenges(data, data.t1);
            }
        )
    }

</script>
