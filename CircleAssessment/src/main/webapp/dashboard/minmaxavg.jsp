<%@ page import="org.ce.assessment.platform.CommunityAssessment" %>
<%@ page import="org.ce.assessment.platform.CommunityAssessments" %>


<%
    CommunityAssessment communityAssessment_mma =
            new CommunityAssessments().getById((String) session.getAttribute("dashboardCommunityAssessmentId"));

    boolean showOwnScore;
    String assessmentId_mma = (String) session.getAttribute("dashboardOwnAssessmentId");
    showOwnScore = assessmentId_mma != null;
%>

<%@include file="../includes.jsp"%>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>




<div id="mma-container" class="gauge-container">
    <br/>
    <br/>

    <div class="preloader-wrapper small active">
        <div class="spinner-layer spinner-green-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>
</div>

<script>

    const loader_mma = '<div class="preloader-wrapper small active">\n' +
        '        <div class="spinner-layer spinner-green-only">\n' +
        '            <div class="circle-clipper left">\n' +
        '                <div class="circle"></div>\n' +
        '            </div>\n' +
        '            <div class="gap-patch">\n' +
        '                <div class="circle"></div>\n' +
        '            </div>\n' +
        '            <div class="circle-clipper right">\n' +
        '                <div class="circle"></div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>';

    function showLoaderMMA() {
        $("#mma-container").html(loader_mma);

    }

    const scoreLetters_mma = {101: "N/A", 90: "A+",  70: "A", 50: "B", 30: "C", 10: "D", 2: "E", 0: ""};
    function getScoreLetter(score) {
        let m = 0;
        for (let k in scoreLetters_mma)  {
            if (score > k) {
                m = k;
            }
        }
        return scoreLetters_mma[m];
    }

    function getColorForLetter(letterScore) {
        return {
            "A+": "#00bf6d",
            "A" :"#008040",
            "B" : "#dce300",
            "C" : "#dd7b00",
            "D" : "#df1b00",
            "E" : "#ac0025",
            "F" : "#c0c0cc"
        }[letterScore];
    }

    function drawMMA(chartContainer, allData, data) {

        const {min, max, avg, tickSize} = data;
        const {t1, t2s} = allData;

        const title = "Scores " + ((data.drilldown === true)?"by 7 Key Elements":"for " + data.name) + " <i id=\"mma-tooltip\" class=\"tiny material-icons orange-text\">info</i>";
        const subTitle = ((data.drilldown === true)?"Click the chart to drill down" : "<button id=\"drillUp_mma\">< Back to Key Elements</button>") ;


        const seriesData = [{
            type: 'column',
            name: 'Max',
            color: '#1e94ba',
            data: max
        }, {
            type: 'column',
            name: 'Average',
            color: '#6fc9f2',
            data: avg
        }, {
            type: 'column',
            name: 'Min',
            color: '#bde3f8',
            data: min
        }];

        if (data.hasOwnProperty('own')) {
            seriesData.push({
                type: 'line',
                name: 'My score',
                color: 'rgba(255, 255, 255, 0)',
                marker: {
                    symbol: 'circle',
                    fillColor: 'yellow'
                },
                data: data.own
            });
        }

        const chart = Highcharts.chart(chartContainer, {
            chart: {
                polar: true
            },
            exporting: {enabled: false},
            credits: {enabled: false},

            title: {
                text: title,
                useHTML: true
            },
            subtitle: {
                text: subTitle,
                useHTML: true
            },

            pane: {
                startAngle: 5,
                endAngle: 365,
                color: 'white'
            },

            xAxis: {
                tickInterval: tickSize,
                min: 0,
                max: 360,
                showLastLabel: true,
                gridLineColor: 'rgba(100,100,100,0.1)',
                labels: {
                    formatter: function () {
                        return data.labels[Math.round(this.value / tickSize)];
                    }
                }
            },

            yAxis: {
                min: 0,
                max: 120,
                tickPositions: [0, 10, 30, 50, 70, 90, 100, 120],
                gridLineColor: 'rgba(255,255,255, 0.2)',
                gridZIndex: 10,
                labels: {
                    align: "left",
                    x: 10,
                    y: -2,
                    formatter: function () {
                        return scoreLetters_mma[this.value];
                    }
                }
            },

            tooltip: {
                formatter: function() {
                    const label =  data.labels[Math.round(this.x / data.tickSize)];
                    const scoreLetter = getScoreLetter(this.y);
                    return label + '<br/>' +
                        '<b>' + this.series.name + ': ' +
                        '<span style="font-weight: 700; color:' + getColorForLetter(scoreLetter) + '">' +
                        '' + scoreLetter + '' +
                        '</span>' +
                        '</b> ' +
                        '(' + this.y + ')';
                }
            },

            plotOptions: {
                series: {
                    pointStart: 0,
                    pointInterval: data.tickSize,
                    pointPlacement: "on",
                    point: {
                        events: {
                            click: function () {
                                if (data.drilldown === true) {
                                    drawMMA(chartContainer, allData, t2s[this.index]);
                                } else {
                                    drawMMA(chartContainer, allData, t1);
                                }
                            }
                        }
                    }
                },
                column: {
                    grouping: false,
                    pointPadding: 0,
                    groupPadding: 0
                }
            },

            series: seriesData
        },
        function(chart) {
            $("#drillUp_mma").on("click", ()=>drawMMA(chartContainer, allData, t1));
        }
        );

        chart.xAxis[0].labelGroup.element.childNodes.forEach(function(label)
        {
            label.style.cursor = "pointer";
            label.onclick = function() {
                const [t2s] = data;
                let found = false;
                for (let i = 0; i < t2s.length; i++) {
                    const t2 = t2s[i];
                    if (t2.name === this.textContent) {
                        drawMMA(chartContainer, allData, t2s[i]);
                        found = true;
                    }
                }
                if (!found) {
                    drawMMA(chartContainer, allData, t1);
                }
            }
        });

        $('#mma-tooltip').tooltip(
            {
                tooltip: "<div class='tt-desc'>" +
                    "<p>In this overview you can see the minimum, average and maximum score by " +
                    "each of the 7 key elements across all organizations " +
                    "within <%=(String) session.getAttribute("dashboardCommunityName")%>. </p>" +
                    <% if (showOwnScore) { %>"<p>You can also benchmark your scores against the community.</p>" + <% } %>
                    "<p>Click anywhere on the chart to drill down/up</p>" +
                    "</div>",
                delay: 20,
                html: true,
                position: 'right'
            }
        );


    }

    <%

    String apiMMA = "api/dashboard/" + communityAssessment_mma.getId() + "/minMaxAvg";
    if (showOwnScore) {
        apiMMA += "/ownAssessment/" + assessmentId_mma;
    }

    %>
    function loadMMA() {
        $.get(
            '<%=apiMMA%>',
            {},
            function (r) {
                const data = JSON.parse(r);
                const {t1} = data;
                drawMMA("mma-container", data, t1);
            }
        );
    }
</script>
