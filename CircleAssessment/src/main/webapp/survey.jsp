<%@ page import="org.ce.assessment.platform.*" %>
<%
    //String host = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";

    String host = request.getServerName();
    host = (("localhost".equals(host))?"http://":"https://") + host;

    if (request.getServerPort() != 80) {
        host += ":" + request.getServerPort();
    }
    host += "/";

    AuthManager mgr = new AuthManager();

    String assessmentSectionId = request.getParameter("section-id");
    AssessmentSection section = new AssessmentSections().getById(assessmentSectionId);

    if (section  == null) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Could not find assessment section with id = "
                + assessmentSectionId);
        return;
    }

    Assessment assessment = section.getAssessment();

    // TODO: enable auth
     User user = mgr.getAuthedUserWithException(request);
//    if (!user.isAdmin()) {
//        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
//    }

%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Circle Assessment</title>
</head>
<%@include file="includes.jsp" %>

<script>
    {       
        // THIS WILL NOT WORK ON DEV PREVIEW

        var onMessage = function (event) {
            const action = event.data.action;


            if (action === 'complete') {
                const url = '<%=response.encodeURL(
                        "api/assessments/" + assessment.getId() +"/sections/"+assessmentSectionId + "/load-report/")%>';
                $.ajax({
                    method: 'post',
                    url: url
                })
                    .done(function() {
                        location.replace('<%=response.encodeURL("assessment.jsp?id=" + assessment.getId())%>');
                    })
                    .fail(function(x) {
                        alert("Could not load report after completion");
                        location.replace('<%=response.encodeURL("assessment.jsp?id=" + assessment.getId())%>');
                    });
            }
            else if (action === 'resize') {
                $('#surveyFrame').height(event.data.height+100);
            }
            else if (action === 'scrollup') {
                window.scrollTo(0,0);
            }
            else if (action === 'alert') {
                $("#alert-modal-content").html(event.data.message);

                if ("Your responses were successfully saved." === event.data.message) {
                    $("#alert-modal-content").html(
                        event.data.message + " Taking you back to the assessment."
                        + "<div class=\"progress\">\n" +
                        "      <div class=\"indeterminate\"></div>\n" +
                        "  </div>"
                    );

                    setTimeout(function() {
                        location.replace('<%=response.encodeURL("assessment.jsp?id=" + assessment.getId())%>');
                    }, 1500);
                }

                $("#alert-modal").modal({complete: function() {$("#alert-modal-content").html("");}});
                $("#alert-modal").modal('open');
            }
            else {
                console.error("unknown action: " + action);
            }
        }

        if (window.addEventListener) {
            window.addEventListener("message", onMessage, false);
        } else if (window.attachEvent) {
            window.attachEvent("onmessage", onMessage);
        }
    }
</script>

<style>
    iframe {
        border: none;
        overflow-y: hidden;
        margin-top: 40px;
        width: 100%;
    }
</style>


<body>
<%@include file="header.jsp" %>

<div class="container breadcrumbs">
    <%
        if (user.isAdmin()) {
    %>
    <a href="<%=response.encodeURL("communities.jsp")%>">
        Home >
    </a>
    <%
        String communityId = (String) session.getAttribute("communityId");
        if (communityId != null) {
            Community parentCommunity = new Communities().getById(communityId);
    %>
    <a href="<%=response.encodeURL("community.jsp?id="+communityId)%>">
        <%=parentCommunity.getName()%> >
    </a>
    <%  }
    %>
    <a href="<%=response.encodeURL("organization.jsp?id="+assessment.getOrganizationId()+"&show=assessments")%>">
        <%=assessment.getOrganization().getName()%> >
    </a>
    <%
    }
    else if (user.isCommunityAdmin()) {
    %>
    <a href="<%=response.encodeURL(mgr.getUserHome(request))%>">
        Home >
    </a>
    <a href="<%=response.encodeURL("organization.jsp?id="+assessment.getOrganizationId()+"&show=assessments")%>">
        <%=assessment.getOrganization().getName()%> >
    </a>
    <% } else { %>
    <a href="<%=response.encodeURL("organization.jsp?id="+assessment.getOrganizationId()+"&show=assessments")%>">
        Home >
    </a>

    <% } %>


    <a href="<%=response.encodeURL("assessment.jsp?id="+assessment.getId())%>">
        <%=assessment.getName()%> >
    </a>
</div>

<iframe src="<%=section.getSurveyLink(false)%>" width="100%" height="100%" id="surveyFrame" scrolling="no"></iframe>

<div id="alert-modal" class="modal">
    <div class="modal-content" id="alert-modal-content">
    </div>
    <div class="modal-footer">
        <button class="btn-flat waves-effect waves-light btn-card cancel modal-close">OK</button>
    </div>
</div>

<%@include file="footer.jsp"%>
</body>
</html>
