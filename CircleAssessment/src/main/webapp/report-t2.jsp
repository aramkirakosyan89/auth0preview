<%@ page import="org.ce.assessment.limeapi.answeroptions.CheckedAnswerOption" %>
<%@ page import="org.ce.assessment.limeapi.answeroptions.MultipleChoiceAnswerOptionWithComment" %>
<%@ page import="org.ce.assessment.platform.AssessmentSection" %>
<%@ page import="org.ce.assessment.platform.AssessmentSections" %>
<%@ page import="org.ce.assessment.platform.report.*" %>
<%@ page import="java.util.List" %>
<%
    String sectionId = request.getParameter("sectionId");
    String t2SectionName = request.getParameter("t2Name");

    AssessmentSection section = new AssessmentSections().getById(sectionId);
    LetterScore t1Score = LetterScore.getForScore(section.getScore());

    SevenElementsSection t2Section = (SevenElementsSection) section.getReport().sections.get(t2SectionName);
    LetterScore t2Score = LetterScore.getForScore(t2Section.getScore());
%>

<div class="row no-print" style="margin-bottom: 40px">
    <div class="col s10 offset-s1 t1-name">
        <a href="javascript: showSection(<%=sectionId%>)">
            <%=section.getName()%>
        </a>
        <div class="score-box <%=t1Score.cssClass%>">
            <%=t1Score.letter%>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s1 section-nav print-hide">
        <a href="javascript: showSection(<%=sectionId%>)">
            ^
        </a>
    </div>
    <div class="col s10 t2-name">
        <%=t2Section.getName()%>
        <div class="score-box <%=t2Score.cssClass%>" title="<%=t2Score.getScoreTitle()%>">
            <%=t2Score.letter%>
        </div>
    </div>

</div>
<div class="row">
    <div class="col s10 offset-s1">
        <h4 class="part-header">Strategies</h4>
    <% for (Strategy strategy: t2Section.getStrategies()) {
        if (!strategy.isEnabled()) {
            continue;
        }
    %>
        <h5 class="strategy-name">
            <%=strategy.name%>
            <div class="scorecard-strategies-value">
        <%
            Strategy.EngagementLevel value = strategy.getEngagementLevel();
            if (value.value == 1) {
                %><div class="value-gauge gauge-empty"></div><%
            } else {
                for (int i = 0; i<value.value; i++) {
                %><div class="value-gauge gauge-full"></div><%
                }
            }
        %>
                <%=value.name%>
            </div>
        </h5>
        <div class="strategy-comment">
            <%=strategy.getComment().value()%>
        </div>
    <% } %>
        <hr>
    <%
        Sector quant = t2Section.getQuant();
        if (quant != null) {
    %>
        <h4 class="part-header"><%=quant.getName()%></h4>
        <%=quant.value()%>
    <% } %>

        <h4 class="part-header">Key challenges</h4>
        <%
            MultipleChoiceSector keyChallenges =  t2Section.getKeyChallenges();
            List<CheckedAnswerOption> selectedOptions = keyChallenges.getSelectedOptions();
        %>
        <ul>
            <%
                for (CheckedAnswerOption option : selectedOptions) {
            %>
            <li class="option">
                <%=option.getText()%>
                <% if (option instanceof MultipleChoiceAnswerOptionWithComment) {%>
                <span class="option-comment">
                        <%=((MultipleChoiceAnswerOptionWithComment) option).getComment()%>
                        </span>
                <% } %>
            </li>
            <%
                } // eo for option
            %>
        </ul>
        <div class="user-comment">
            <%=keyChallenges.getExplanation()%>
        </div>
    <%
        Sector keyOpportunities = t2Section.getKeyOpportunities();
        if (keyOpportunities != null) {
    %>
        <h4 class="part-header">Key opportunities</h4>
        <div class="user-comment">
            <%=keyOpportunities.value()%>
        </div>
    <% }

    Sector notes = t2Section.getNotes();
    if (notes != null) {
    %>
        <h4 class="part-header">Notes</h4>
        <div class="user-comment">
            <%=notes.value()%>
        </div>
    <% } %>

    </div>
</div>