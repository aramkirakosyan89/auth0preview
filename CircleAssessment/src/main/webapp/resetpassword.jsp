<%@page import="org.ce.assessment.platform.AuthManager.InputFields"%>
<%@page import="org.ce.assessment.platform.PasswordResetRequests"%>
<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>


<%
	String msg = "";
	InputFields f = AuthManager.fields;
	String token = request.getParameter(AuthManager.fields.TOKEN);

    if (!new PasswordResetRequests().isValid(token)) {
        msg = "Invalid request. Your request may have expired.";
    } else {
        msg = "";
    }
%>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Reset Password - Circle Assessment</title>
	<link href="form.css"  rel="stylesheet"  type="text/css">

</head>


<%@include file="includes.jsp" %>

<script src="form-validate.js" type="text/javascript"></script>
<script>
    $(function() {
        $("#pw2In").bind('input propertychange', function() {
            validatePasswords($("#pw1In"), $("#pw2In"), $("#submitButton"));
        });
        $("#pw1In").bind('input propertychange', function() {
            validatePasswords($("#pw1In"), $("#pw2In"), $("#submitButton"));
        });
    });

	function resetPassword() {
	    if (validatePasswords($("#pw1In"), $("#pw2In"), $("#submitButton"))) {
	        $.ajax({
                method: 'post',
                url: 'api/auth/reset-password/',
                data: {
                    token: '<%=token%>',
                    password: $("#pw1In").val()
                }
            })
                .done(function() {
                    $("#password-form").addClass("hidden");
                    $("#success-msg").removeClass("hidden");
                })
                .fail(function(x) {
                    alert(x.responseText);
                });
        }
	}
</script>
<style>
    div.boxed {
        width: 30em;
        margin: 0 auto;
    }
</style>
<body>
    <%@include file="header.jsp" %>
    <h2>Reset password</h2>

    <div class="container">
        <div id="password-form" class="boxed">
            <div class="input-field">
                <input type="password" name="<%=f.PW1 %>" id="pw1In" autocomplete="off">
                <label for="<%=f.PW1 %>">New Password</label>
            </div>
            <div class="input-field">
                <input type="password" name="<%=f.PW2 %>" id="pw2In" autocomplete="off">
                <label for="<%=f.PW2 %>" data-error="does not match" >Confirm Password</label>
            </div>

            <button type="button" class="btn grey lighten-3 black-text wide"  id="submitButton" onclick="resetPassword()">Submit</button>
        </div>

		<div id="success-msg" class="boxed hidden">
            Password is set.
            <a href="index.jsp">Sign in here</a>
        </div>
        <br><br>
	</div>
</body>
</html>