<div class="container white">

	<div class="footer">
		<a href="http://www.circle-economy.com" id="ce_logo" target="_blank"><img src="images/celogo.png" width=120></a>
		<br>
		<div style="max-width: 425px; margin: 10px auto; display: block; font-size: 12px; color: black;">
				Powered by <b>Circle Economy</b> - <br>
			a social enterprise, organised as a cooperative, Circle Economy accelerates the transition to circularity through on the ground, action focused, development of practical and scalable solutions.
		</div>
		<a href="#terms-modal" class="modal-trigger">Terms of use</a> | <a href="#privacy-modal" class="modal-trigger">Privacy and Cookie Policy</a>
        <br><br>
	</div>

    <style>
        .legal-modal {
            width: 60em !important;
            height: 80% !important;
        }
    </style>
	<div class="legal-modal modal" id="terms-modal">
		<iframe
				style="width: 100%; height: 90%; border: none"
				src="https://docs.google.com/document/d/e/2PACX-1vTfaN2fU_KmjY7JZiZyvOtyJwQ2OMyCRuxGyqvTwOaf1qOgiBGEWK4pdlPHSAzG7JeO6k_N0uv_9lNb/pub?embedded=true">
		</iframe>

		<div class="modal-footer">
			<button class="btn-flat waves-effect waves-light btn-card modal-close">OK</button>
		</div>
	</div>

    <div class="legal-modal modal" id="privacy-modal">
        <iframe
                style="width: 100%; height: 90%; border: none"
                src="https://docs.google.com/document/d/e/2PACX-1vR854Oerfeag47aCHVGyWhvNwPUN7TGjKlV6TSZNoYgYMVkpIHAK7vMxDWyBq7STrpRE_1iYxJf-yLO/pub?embedded=true">
        </iframe>

        <div class="modal-footer">
            <button class="btn-flat waves-effect waves-light btn-card modal-close">OK</button>
        </div>
    </div>


</div>