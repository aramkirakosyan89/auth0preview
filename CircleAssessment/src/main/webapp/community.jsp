<%@ page import="org.ce.assessment.platform.*" %>
<%@ page import="org.ce.assessment.platform.api.WebappApi" %>
<%@ page import="org.ce.assessment.platform.invites.Invites" %>
<%@ page import="org.ce.assessment.platform.report.LetterScore" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.List" %>
<%@ page import="static org.ce.assessment.platform.api.WebappApi.Tab.DASHBOARD" %>
<%@ page import="static org.ce.assessment.platform.api.WebappApi.Tab.MEMBERS" %>
<%@ page import="static org.ce.assessment.platform.api.WebappApi.Tab.TEAM" %>
<%@ page import="static org.ce.assessment.platform.api.WebappApi.Tab.*" %>

<%
    AuthManager mgr = new AuthManager();
    User user = mgr.getAuthedUser(request);

    String communityId = request.getParameter("id");
    Community community = new Communities().getById(communityId);
    if (community == null) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Could not find community with id = " + communityId);
        return;
    }
    // TODO: confirm this is admin or community admin
    if (!mgr.canSeeCommunity(request, community)) {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }
    session.setAttribute("communityId", communityId);


    WebappApi.Tab curTab = (WebappApi.Tab) session.getAttribute(WebappApi.COMMUNITY_CUR_TAB);
    if (curTab == null) {
        curTab = MEMBERS;
    }
%>

<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="main-style.css" rel="stylesheet" type="text/css">
    <link href="report.css" rel="stylesheet" type="text/css">
    <title>Circle Assessment - <%=community.getName()%></title>
</head>

<%@include file="includes.jsp" %>

<script>

    $(function() {
        $('select').material_select();
    });

    MEMBER_ORG = 0;
    ADMIN_ORG = 1;

    isAdmin = MEMBER_ORG;
    selectedOrganizationId = -1;

    function showInviteModal() {
        isAdmin = MEMBER_ORG;
        // $("#modal-title").html("Invite an organization");
        // $("#btn-invite-existing").html("Invite");
        // $("#org-select-modal").modal('open');
        $("#org-invite-process").hide();
        $("#org-invite-result").hide();
        $("#org-invite-result-msg").html("");
        $("#org-invite-form").show();
        $("#org-invite-modal").modal('open');
    }

    function showAdminModal() {
        isAdmin = ADMIN_ORG;
        $("#modal-title").html("Select admin organization");
        $("#btn-invite-existing").html("Select");
        $("#org-select-modal").modal('open');
    }

    function showAssessmentModal() {
        $("#new-assessment-name").val("<%=community.getName() + " " + Calendar.getInstance().get(Calendar.YEAR)%>");
        $("#new-assessment-modal").modal('open');
    }

    function inviteExisting() {
        if (selectedOrganizationId !== -1) {
            var apiCallUrl = (isAdmin == ADMIN_ORG)
                ?'<%=response.encodeURL("api/communities/"+communityId+"/admin/")%>'
                :'<%=response.encodeURL("api/communities/"+communityId+"/members/invite/")%>';

            $.ajax({
                method: 'post',
                url: apiCallUrl,
                data: {
                    inviteType: 'inviteExisting',
                    orgId: selectedOrganizationId
                }
            })
                .done(function() {
                    location.reload(true);
                })
                .fail(function(x) {
                    console.log(x.responseText);
                    const orgName = $("#select-org-name").val();
                    alert("Could not invite " + orgName + ". Server error.");
                });
        } else {
            alert("unknown organization");
        }
    }

    function inviteByEmail() {
        var email = $("#select-email").val();
        var apiCallUrl = (isAdmin == ADMIN_ORG)
            ?'<%=response.encodeURL("api/communities/"+communityId+"/admin/")%>'
            :'<%=response.encodeURL("api/communities/"+communityId+"/members/invite/")%>';

        $.ajax({
            method: 'post',
            url: apiCallUrl,
            data: {
                inviteType: 'inviteByEmail',
                email: email
            }
        })
            .done(function() {
                location.reload(true);
            })
            .fail(function(x) {
                //TODO: check if email is already registered  alert("Email is already registered. Try inviting the organization. ");
                console.log(x.responseText);
                alert("Could not invite " + email + ". Server error.");
            });
    }

    function inviteNewOrg() {
        const url = "api/communities/" + <%=communityId%> + "/members/create/";
        const data = {
            name: $("#new-org-name").val(),
            email: $("#new-org-email").val()
        };

        $("#org-invite-process").show();
        $("#org-invite-form").hide();
        $("#org-invite-result").hide();

        $.ajax({
            url: url,
            method: "post",
            data: data
        })
            .done(function(x) {
                $("#org-invite-process").hide();
                $("#org-invite-result").show();

                let resultMsg = 'Invited';
                if (x.responseText != null) {
                    resultMsg = x.responseText;
                }
                $("#org-invite-result-msg").html(resultMsg);

                setTimeout(function () {
                    location.reload(true)
                }, 1500);
            })
            .fail(function(x) {
                $("#org-invite-process").hide();
                $("#org-invite-result").show();
                $("#org-invite-result-msg").html("Error: " + x.responseText);
            });
    }

    function withdrawInvitation(inviteToken) {
        var apiCallUrl = 'api/invites/'+inviteToken+'/withdraw/';

        $.ajax({
            method: 'post',
            url: apiCallUrl,
            data: {}
        })
            .done(function() {
                // TODO: show toaster
                location.reload(true);
            })
            .fail(function(x) {
                // TODO: show bad request
                console.log(x.responseText);
                alert("Could not withdraw invite. Server error.");
            });
    }

    function resendInvite(inviteToken) {
        $("#user_msg").html("Sending invitation email .. This may take a minute. We will let you know if something goes wrong.");

        var apiCallUrl = 'api/invites/'+inviteToken+'/';
        $.post(apiCallUrl,
            {action: 'resend'},
            function() {
                alert('Invitation email has been sent again'); // TODO: make toaster
                $("#user_msg").html("");
            }
        )
            .fail(function(reason) {
                console.log(reason);
                if (reason.status === 400) {  // TODO: make this work
                    alert("Could not re-send an invitation. Bad request");
                } else {
                    alert("Could not re-send an invitation. Server error");
                }
                $("#user_msg").html("");
            });
    }

    function createAssessment() {
        const name = $("#new-assessment-name").val();
        const assessmentVersionId = $("#assessment-version").val();
        $.ajax({
            method: 'post',
            url: '<%=response.encodeURL("api/communities/"+communityId+"/assessments/create/")%>',
            data: {
                name: name,
                assessmentVersionId: assessmentVersionId
            }
        })
            .done(function() {
                location.reload(true);
            })
            .fail(function (x) {
                alert(x.responseText);
            });
    }

    function deleteAssessment(assessmentId) {
        // TODO: prompt confirmation
        $.ajax({
            method: 'post',
            url: 'api/communities/' + <%=communityId%> + '/assessments/' + assessmentId + '/delete/', // TODO: user DELETE
            data: {}
        })
            .done(function () {
                location.reload(true);
            })
            .fail(function (x) {
                alert(x.responseText);
            });
    }

    function changeName() {
        const name = $("#commnunity-name").val();
        $.ajax({
            method: 'put',
            url: '<%=response.encodeURL("api/communities/" + communityId + "/")%>',
            data: {
                name: name
            }
        })
            .done(function () {
                location.reload(true);
            })
            .fail(function(x) {
                alert(x.responseText);
            });
    }

    function setTab(tab) {
        $.post(
            "<%=response.encodeURL("api/webapp/community-tabs/current")%>",
            {
                tab: tab
            }
        ).fail(function (x) {
            alert(x.responseText);
        })
    }

</script>

<style>
    .score-cell {
        width: 55px;
        min-width: 30px;
        text-align: center;
        font-weight: bold;
        font-family: sans-serif;
        border: 1px solid white;
        opacity: 0.7;
        color:white;
    }

    .score-cell  img {
        width: 40px;
    }



</style>

<body class="landing">
<%@include file="header.jsp" %>

<div class="container breadcrumbs">
    <%
        if (user.isAdmin()) {
    %>
    <a href="<%=response.encodeURL("communities.jsp")%>">
        Home >
    </a>
    <%
        }
    %>
</div>


<h2><%=community.getName()%></h2>

<div id="org-select-modal" class="modal">
    <h5 id="modal-title">Invite organization</h5>
    <div class="modal-content">
        <div class="input-field">
            <%@include file="components/org_select.jsp"%>
        </div>
        <button onclick="inviteExisting()"
                class="btn waves-effect waves-teal btn-card"
                id="btn-invite-existing">
            Invite
        </button>

        <div class="input-field">
            <input type="text" id="select-email">
            <label for="select-email">email</label>
        </div>
        <button onclick="inviteByEmail()" class="btn waves-effect waves-teal btn-card">Invite by email</button>
    </div>

    <div class="modal-footer">
        <button class="btn-flat waves-effect waves-light btn-card cancel modal-close">Cancel</button>
    </div>
</div>

<div id="org-invite-modal" class="modal">
    <h5>Invite organization</h5>
    <div id="org-invite-form">
        <div class="modal-content">
            <div class="input-field">
                <input type="text" id ="new-org-name">
                <label for="new-org-name">Organization name</label>
            </div>
            <div class="input-field">
                <input type="text" id="new-org-email">
                <label for="new-org-email">Invite by email (optional)</label>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn-flat waves-effect waves-light btn-card cancel modal-close">Cancel</button>
            <button onclick="inviteNewOrg()" class="btn waves-effect waves-teal btn-card">Invite</button>
        </div>
    </div>
    <div id="org-invite-process" style="display: none">
        <div class="modal-content">
            <div class="progress">
                <div class="indeterminate"></div>
            </div>
            Inviting... This may take a minute
        </div>
        <div class="modal-footer">
            <button class="btn-flat waves-effect waves-light btn-card cancel modal-close">Close</button>
        </div>
    </div>
    <div id="org-invite-result" style="display: none">
        <div class="modal-content" id="org-invite-result-msg">
        </div>
        <div class="modal-footer">
            <button class="btn-flat waves-effect waves-light btn-card cancel modal-close">Close</button>
        </div>
    </div>
</div>

<div id="new-assessment-modal" class="modal">
    <h5>Create assessment</h5>
    <div class="modal-content">
        <div class="input-field">
            <input type="text" id="new-assessment-name" value="<%=(community.getName() + " " + Calendar.getInstance().get(Calendar.YEAR))%>">
            <label for="new-assessment-name">Assessment Name</label>
        </div>
        <div class="input-field">
            <select id="assessment-version" name="assessment-version">
                <%
                    for (AssessmentVersion a : Platform.instance().getAssessmentVersions().getAll()) {
                %>
                <option value="<%=a.getId()%>"><%=a.getName()%></option>
                <% } %>
            </select>
            <label for="assessment-version">Assessment Version</label>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn-flat waves-effect waves-light btn-card cancel modal-close">Cancel</button>
        <button class="btn waves-effect waves-light btn-card" onclick="createAssessment()">Create</button>
    </div>
</div>

<ul class="tabs">
    <li class="tab col s1">
        <a href="#members-tab" <%=(curTab == MEMBERS)?"class=\"active\"":""%> onclick="setTab('<%=MEMBERS.name()%>')">
            Members
        </a>
    </li>
    <li class="tab col s1">
        <a href="#dashboard-tab" <%=(curTab == DASHBOARD)?"class=\"active\"":""%> onclick="setTab('<%=DASHBOARD.name()%>')">
            Dashboard
        </a>
    </li>
    <li class="tab col s1">
        <a href="#team-tab" <%=(curTab == TEAM)?"class=\"active\"":""%> onclick="setTab('<%=TEAM.name()%>')">
            Team
        </a>
    </li>
    <% if (user.isAdmin()) { %>
    <li class="tab col s1">
        <a href="#settings-tab" <%=(curTab == SETTINGS)?"class=\"active\"":""%> onclick="setTab('<%=SETTINGS.name()%>')">
            Settings
        </a>
    </li>
    <% } %>
</ul>

<div id="members-tab" class="col s12">
    <div class="container body">
        <table class="highlight">
            <thead>
            <tr>
                <td colspan="2">
                    <button onclick="showInviteModal()"
                            class="btn grey lighten-3 black-text">
                        Invite
                    </button>
                </td>
                <th class="score-cell"><img src="images/nav/grey-collaborate.png" title="Collaborate to Create Joint Value"></th>
                <th class="score-cell"><img src="images/nav/grey-design.png" title="Design for the Future"></th>
                <th class="score-cell"><img src="images/nav/grey-business.png" title="Rethink the Business Model"></th>
                <th class="score-cell"><img src="images/nav/grey-digital.png" title="Incorporate Digital Technology"></th>
                <th class="score-cell"><img src="images/nav/grey-waste.png" title="Use Waste as a Resource"></th>
                <th class="score-cell"><img src="images/nav/grey-prioritise.png" title="Prioritise Regenerative Resources"></th>
                <th class="score-cell"><img src="images/nav/grey-preserve.png" title="Preserve and Extend What Is Already Made"></th>
            </tr>
            </thead>

            <%
                final List<Organization> members = community.getMembers();
                for (Organization member : members) {
            %>
            <tr>
                <td class="nowrap-column">
                    <a href="<%=response.encodeURL("organization.jsp?id=" + member.id)%>"><%=member.getName()%></a>
                </td>
                <td width="100%">
                    <button class="btn-flat white grey-text waves-effect waves-light">Remove</button>
                </td>
                <%
                    if (!member.getAssessments().isEmpty()) {
                        Assessment assessment = member.getAssessments().get(0);
                        for (AssessmentSection section : assessment.getSections()) {
                            LetterScore ls = LetterScore.getForScore(section.getScore());
                %>
                <td width="50" class="score-cell <%=ls.cssClass%>">
                    <%=ls.letter%>
                </td>
                <%      }
                } %>
            </tr>
            <%
                }

                List<Invite> memberInvites = new Invites().getCommunityMemberInvites(communityId);
                if (!memberInvites.isEmpty()) {
                    for (Invite invite : memberInvites) {
            %>
            <tr>
                <td class="nowrap-column">
                    <%=invite.getInvitee()%> <em>invited</em>
                </td>
                <td width="100%">
                    <button class="btn-flat white grey-text waves-effect waves-light"
                            onclick="resendInvite('<%=invite.token%>')">Resend</button>
                    <button class="btn-flat white grey-text waves-effect waves-light"
                            onclick="withdrawInvitation('<%=invite.token%>')">Withdraw</button>
                    <span id="user_msg"></span>
                </td>
                <%--TODO: add date--%>
            </tr>
            <%
                    }
                }
            %>
        </table>
    </div>
</div>

<div id="dashboard-tab">
    <div class="container body">
        <%
            List<CommunityAssessment> assessments = community.getAssessments();
            if (!assessments.isEmpty()) {
                session.setAttribute("dashboardCommunityAssessmentId", assessments.get(0).getId());
                session.setAttribute("dashboardCommunityName", community.getName());
                session.setAttribute("dashboardCanUpdate", true); // TODO: only for admin
                session.removeAttribute("dashboardOwnAssessmentId");
        %>
        <%@include file="dashboard.jsp"%>
        <% } else { %>
        This community does not have assessments yet. Let your administrator add an assessment first.
        <%}%>
    </div>
</div>

<%
    session.setAttribute("teamTabOrganization", community.getAdminOrganization());
    session.setAttribute("teamTabCanAdmin", user.canAdminOrganization());
%>
<%@include file="components/tabs/team.jsp"%>

<%
    if (user.isAdmin()) {
%>
<div id="settings-tab" class="col s12">
    <div class="container body">
        <%
            Organization adminOrg = community.getAdminOrganization();
        %>

        <div class="row">
            <div class="col s12 m6">
                <div class="card">
                    <div class="card-content">
                        <div class="input-field" style="display:inline-block; width: 20em">
                            <input name="community-name" type="text" id="community-name" value="<%=community.getName()%>">
                            <label for="community-name">Name</label>
                        </div>
                        <button class="btn" onclick="changeName()">Change</button>
                    </div>
                </div>
            </div>
            <div class="col s12 m6">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Admin Organization</span>
                        <% if (!Organization.NONE.id.equals(adminOrg.id)) { %>
                        <a href="<%=response.encodeURL("organization.jsp?id=" + adminOrg.id)%>">
                            <%=adminOrg.getName()%>
                        </a>
                        <% } else {
                            List<Invite> adminInvites = new Invites().getCommunityAdminInvite(communityId);
                            if (!adminInvites .isEmpty()) {
                                if (adminInvites .size() > 1) {
                                    System.out.println("ERROR: More than one active invite to be administrator for community(" + communityId + ")");
                                } // TODO: add withdraw buttons
                        %><%=adminInvites.get(0).getEmail()%>
                        <% } else { %>
                        none
                        <% }
                        } %>

                    </div>
                    <div class="card-action">
                        <button onclick="showAdminModal()" class="btn">
                            Change
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col l12">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Assessments</span>
                        <table class="highlight">
                            <%
                                for (CommunityAssessment communityAssessment : community.getAssessments()) {
                            %>
                            <tr>
                                <td><%=communityAssessment.getName()%></td>
                                <td><%=communityAssessment.getAssessmentVersion().getName()%></td>
                                <td>
                                    <button onclick="deleteAssessment(<%=communityAssessment.getId()%>)"
                                            class="btn-flat white teal-text waves-effect waves-light"
                                            style="font-weight: bold">
                                        Delete
                                    </button>
                                </td>
                            </tr>
                            <%
                                }
                            %>
                        </table>
                    </div>
                    <div class="card-action">
                        <button onclick="showAssessmentModal()"
                                class="btn-flat white teal-text waves-effect waves-light">
                            Create New
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<% } %>

<%@include file="footer.jsp"%>
</body>
</html>

