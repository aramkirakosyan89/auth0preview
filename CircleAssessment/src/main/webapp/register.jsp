<%@ page import="org.ce.assessment.platform.AuthManager" %>
<%@ page import="org.ce.assessment.platform.api.AbstractApi" %>
<%@ page import="org.ce.assessment.platform.invites.Invite" %>
<%@ page import="static org.ce.assessment.platform.invites.Invites.InviteType.COMMUNITY_BY_ORG" %>
<%@ page import="static org.ce.assessment.platform.invites.Invites.InviteType.COMMUNITY_BY_EMAIL" %>
<%@ page import="org.ce.assessment.platform.invites.Invites" %>
<%@ page import="javax.ws.rs.core.Response" %>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
    String msg;
    String orgName = "";
    boolean showForm = false;
    boolean isOrgNameSet = false;

	AuthManager mgr = new AuthManager();

	String email = "";
    Invite invite = null;
    String inviteToken = request.getParameter(AbstractApi.P_INVITE_TOKEN);
    if (inviteToken == null) {
        inviteToken = "";
    } else {
        invite = new Invites().getByToken(inviteToken);
    }

    if (invite == null) {
        msg = "Unknown invitation";
    } else if (invite.getStatus() == Invites.InviteStatus.WITHDRAWN) {
        msg = "This invitation has been withdrawn";
    } else if (invite.getStatus() == Invites.InviteStatus.ACCEPTED) {
        msg = "This invitation has already been accepted";
    } else {
        showForm = true;
        email = invite.getEmail();

        switch (invite.type) {
            case USER:
                orgName = invite.getOrganization().getName();
                isOrgNameSet = true;
                msg = "You are invited to join " + orgName + ". Welcome!";
                break;
            case ORGANIZATION_BY_COMMUNITY:
                orgName = invite.getOrganization().getName();
                isOrgNameSet = false;
                msg = "You are invited to join " + invite.getCommunity().getName() + ". Welcome!";
            case COMMUNITY_BY_EMAIL:
                if (invite.asAdmin()) {
                    msg = "You are invited to create " + invite.getCommunity().getName() + ". Welcome!";
                } else {
                    msg = "Your organization is invited to join " + invite.getCommunity().getName() + ". Welcome!";
                }
                break;
            default:
                msg = "Unknown invite type";
                showForm = false;
                break;
        }
    }
%>


<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Circle Assessment - Register</title>
	<link href="main-style.css" rel="stylesheet" type="text/css">
	<link href="form.css"  rel="stylesheet"  type="text/css">
    <%--TODO: clean up those styles--%>
</head>

<%@include file="includes.jsp" %>

<script>
    function validateName() {
        const name = $("#nameIn").val();
        if (name === null || name === "") {
            $("#nameMsg").html("name cannot be empty");
            return false;
        } else {
            $("#nameMsg").html("");
            return true;
        }
    }
    $("#nameIn").on("change", function() {
        validateName();
    });

    function validateEmail() {
        const email = $("#emailIn").val();
        if (email === null || email === "") {
            $("#emailMsg").html("e-mail cannot be empty");
            return false;
        }

        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(email)) {
            $("#emailMsg").html(" ");
            return true;
        } else {
            $("#emailMsg").html("invalid e-mail");
            return false;
        }
    }

    $("#emailIn").on("change", function() {
        validateEmail();
    });

    function validatePasswords() {
        const pw1In = $("#pw1In");
        const pw1Msg = $("#pw1Msg");
        const pw2In = $("#pw2In");
        const pw2Msg = $("#pw2Msg");
        const submitButton = $("#submitButton");


        const pw1 = pw1In.val();
        if (null === pw1 || "" === pw1) {
            pw1Msg.html("password cannot be empty");
            return false;
        } else {
            pw1Msg.html(" ");
        }

        const pw2 = pw2In.val();
        if (pw1 !== pw2) {
            pw2Msg.html("passwords do not match");
            pw2In.css("background-color", "#ffe5e5");
            submitButton.prop('disabled', true);

            return false;
        } else {
            pw2Msg.html(" ");
        }
        pw2In.css("background-color", "#F3FFEA");
        submitButton.prop('disabled', false);
        return true;
    }

    function submitForm() {
        var valid = 1;
        valid &= validateName();
        valid &= validateEmail();
        valid &= validatePasswords();

        <% if (invite.type == COMMUNITY_BY_ORG) { %>
        valid &=  $("#joinIn").is(":checked");
        <% } %>

        if (1 === valid) {

            $("#submitButton").prop("disabled",true);
            $("#register_msg").html("Registering..");

            var data = {
            <% if (invite.type == COMMUNITY_BY_EMAIL && !invite.asAdmin()) { %>
                okJoin: $("#joinIn").is(":checked"),
            <% } %>
                inviteToken: '<%=inviteToken%>',
                email: $("#emailIn").val(),
                name: $("#nameIn").val(),
                password: $("#pw1In").val(),
                password2: $("#pw2In").val(),
                orgName: $("#orgIn").val()
            }

            var apiUrl = 'api/users/register/';
            $.ajax({
                url: apiUrl,
                method: 'post',
                data: data
            })
            .done(function(res) {
                window.location.href = res;
            })
            .fail(function(x) {
                if (x.status === <%=Response.Status.BAD_REQUEST.getStatusCode()%>) {
                    alert(x.responseText);
                } else if (x.status === <%=Response.Status.CONFLICT.getStatusCode()%>) {
                    alert("Email is already registered");
                } else if (x.status === <%=Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()%>) {
                    console.log(x.responseText);
                    alert("Internal server error - could not register. Try later");
                } else if (x.status === <%=HttpServletResponse.SC_UNAUTHORIZED%>) {
                    alert("Registered but could not sign in.");
                } else if (x.status === <%=HttpServletResponse.SC_EXPECTATION_FAILED%>) {
                    alert("You need accept joining the community to register");
                } else {
                    alert("Unknown error. Try again later");
                    console.error("Unknown error - code: " + reason.status);
                }

                $("#register_msg").html("");

            });
        }
        return false;
    }
</script>

<body>
	<div class="body-container">
		<br>
		<h1 style="color: black">Circle Assessment</h1>
		<div><%=msg%></div>
        <% if (showForm) { %>
		<form method="post" id="regForm" class="boxed">
			<input type="hidden" name="<%=AbstractApi.P_ACTION%>" value="<%=AbstractApi.A_REGISTER_BY_INVITATION%>">
            <input type="hidden" name="<%=AbstractApi.P_INVITE_TOKEN%>" value="<%=inviteToken%>">

			<label>
				Name<span class="errorMsg" id="nameMsg"></span>
				<input type="text" name="<%=AbstractApi.P_NAME%>" id="nameIn" placeholder="">
			</label>

            <label>
                Organization<span class="errorMsg" id="orgMsg"></span>
                <input type="text" name="<%=AbstractApi.P_ORG_NAME%>" id="orgIn" placeholder="" value="<%=orgName%>" <%=((isOrgNameSet)?"disabled":"")%>>
            </label>
			
			<label>
				Email
				<span class="errorMsg" id="emailMsg"> </span>
            <% if (email.isEmpty()) { %>
                <input type="text" name="<%=AbstractApi.P_EMAIL%>" id="emailIn" onchange="validateEmail()">
            <% } else { %>
                <input type="hidden" name="<%=AbstractApi.P_EMAIL%>" id="emailIn"  value="<%=email%>">
                <input type="text" value="<%=email%>" disabled>
            <% } %>
			</label>

			<label>	
				Password *
				<span class="errorMsg" id="pw1Msg"></span>
				<input type="password" name="<%=AbstractApi.P_PASSWORD %>" id="pw1In" onkeyup="validatePasswords()">
			</label>
			<label>
				Confirm Password *
				<span class="errorMsg" id="pw2Msg"></span>
				<input type="password" name="<%=AbstractApi.P_PASSWORD2 %>" id="pw2In" onkeyup="validatePasswords()">
			</label>

            <% if (invite.type == COMMUNITY_BY_EMAIL && !invite.asAdmin()) { %>
            <br>
            <p>
                <input type="checkbox" name="<%=AbstractApi.P_JOIN_COMMUNITY%>" id="joinIn">
                <label for="joinIn">Join community</label>
            </p>
            <% } %>
			
			<button type="button" id="submitButton" disabled="disabled" class="wide" onclick="submitForm()">Register</button>
            <span id="register_msg" style="font-style: italic"></span>
		</form>

        <% } %>
    </div>
	<br><br><br>
</body>
</html>
