<%@ page import="org.ce.assessment.fashiontool.WasteMapping" %>
<%@ page import="org.ce.assessment.userdb.Organization" %>
<%@ page import="org.ce.assessment.userdb.Organizations" %>
<%
    String host = "https://" + request.getServerName();
    if (request.getServerPort() != 80) {
        host += ":" + request.getServerPort();
    }
    host += "/";

    WasteMapping wasteMapping = WasteMapping.getForOrganization(organization);
    if (wasteMapping == null) {
        return;
    }
%>

<script>
    {
        // THIS DOES NOT WORK IN DEV PREVIEW


        var onMessage = function (event) {
            const action = event.data.action;

            if (action === 'complete') {
                <%--location.replace('<%=backUrl%>');--%>
            }
            else if (action === 'resize') {
                $('#surveyFrame').height(event.data.height+100);
            }
            else if (action === 'scrollup') {
                window.scrollTo(0,0);
            }
            else if (action === 'alert') {
                $("#alert-modal-content").html(event.data.message);

                if ("Your answers were successfully saved." === event.data.message) {
                    $("#alert-modal-content").html(
                        event.data.message + " Taking you back to the organization profile."
                        + "<div class=\"progress\">\n" +
                        "      <div class=\"indeterminate\"></div>\n" +
                        "  </div>"
                    );

                    <%--setTimeout(function() {--%>
                        <%--location.replace('<%=backUrl%>');--%>
                    <%--}, 1500);--%>
                }

                $("#alert-modal").modal({complete: function() {$("#alert-modal-content").html("");}});
                $("#alert-modal").modal('open');
            }
            else {
                console.error("unknown action: " + action);
            }
        }

        if (window.addEventListener) {
            window.addEventListener("message", onMessage, false);
        } else if (window.attachEvent) {
            window.attachEvent("onmessage", onMessage);
        }
    }
</script>

<style>
    iframe {
        border: none;
        overflow-y: hidden;
        margin-top: 40px;
        width: 100%;
    }
</style>

<iframe src="<%=wasteMapping.getSurveyLink(false)%>" width="100%" height="100%" id="surveyFrame" scrolling="no"></iframe>

<div id="alert-modal" class="modal">
    <div class="modal-content" id="alert-modal-content">
    </div>
    <div class="modal-footer">
        <button class="btn-flat waves-effect waves-light btn-card cancel modal-close">OK</button>
    </div>
</div>
