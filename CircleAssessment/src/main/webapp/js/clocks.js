
function getDefaultOptions() {
    return {
        "color": "#00c1f2",
        "label": "",
        "number_size": "1.4rem",
        "caption": "",
        "caption_position": "below",
        "fill": "white",
        "stroke_color": "#ccc",
        "stroke_width": 15,
        "progress_width": 15
    };
}

function newAssessmentClock(divName, value, label) {
    var options = getDefaultOptions();
    options.label = label;
    options.caption = 'score';
    options.caption_position = 'above';
	options.fill = '#263238';
	options.stroke_color = options.fill;
	options.stroke_width = 6;
	options.progress_width = 7;
		
	if (value == -1) {
		options.color = 'white';
	}
	newClock(divName, '#00c1f2', value, options );
}

function newReportClock(divName, color, value, caption) {
	var options = getDefaultOptions();
	options.fill = '#263238';
	options.caption = caption;
	// options.color = '#555';
    options.color = 'white';
	options.stroke_width = 1;
	options.progress_width = 3;
	newClock(divName, color, value, options)
	// newClock(divName, getClockColor(value), value, options)
}

function getClockColor(value) {
	var g = Math.floor(255 / 100 * (100 - value));
	var color = "rgb(" + g + ", 255, " + g + ")";
	return color;

}

function newReportTotalClock(divName, value) {
	var options = getDefaultOptions();
	options.fill = '#263238';
	options.caption = ' / 100';
	options.color = 'white';
	
	options.stroke_width = 1;
	options.progress_width = 7;
	options.number_size = '2.7rem';
	newClock(divName, '#00bfaf', value, options)
}

function newClock(divName, color, value, options) {
	var collabChart = new ProgressBar.Circle("#" + divName, {
		fill: options.fill,
		color: options.color,
	  trailColor: options.stroke_color,
	  strokeWidth: options.progress_width,
	  trailWidth: options.stroke_width,
	  easing: 'bounce',
	  duration: 800 + Math.random()*1300,
	  text: {		
	    autoStyleContainer: false
	  },	  
	  from: { color: color, width: options.progress_width },
	  to: { color: color, width: options.progress_width },
	  // Set default step function for all animate calls
	  step: function(state, circle) {
	    circle.path.setAttribute('stroke', state.color);
	    circle.path.setAttribute('stroke-width', state.width);
	    
	    var value = Math.round(circle.value() * 100);
	    if (value === -1) {
		     circle.setText('pending');
		     collabChart.text.style.fontSize = '0.7rem';
		     collabChart.text.style.fontStyle = 'italic';
		     value = 0;
	    } else {
	    	var caption = value;
	    	if (options.caption_position == 'above') {
	    		caption = "<div class=\"clock_caption\">"+options.caption+"</div>" + value;
	    	} else if ((options.caption_position == 'below')) {
	    		caption = value + "<div class=\"clock_caption\">"+options.caption+"</div>";
	    	}
	    	circle.setText(caption);
	    }
	  }
	});
	
	collabChart.text.style.fontFamily = '"Montserrat", Helvetica, sans-serif';
	collabChart.text.style.fontSize = options.number_size;
	if (options.label != null) {
		$("#"+divName).append("<p>" + options.label + "</p>");
	}

	collabChart.animate(value * 0.01);  // Number from 0.0 to 1.0
}