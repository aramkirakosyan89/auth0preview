<%@page import="org.ce.assessment.platform.AuthManager"%>
<%@page import="org.ce.assessment.userdb.User"%>


<%--<link href="form.css" rel="stylesheet" type="text/css">--%>

<style>

    div#user_box {
        padding-top: 2px;
        text-align: right;
    }

    div#user_box a {
        display: inline-block;
        color: #eee;
        text-decoration: none;
        vertical-align: middle;
        margin: 5px 10px;

    }

    div#user_box a span#user_name {
        font-size: 15px;
        font-weight: bold;
        font-family: 'Open sans';
    }

    div#user_box a:hover span#user_name {
        text-decoration: underline;
    }

    div#user_box a#signOutButton {
        color: #eee;
        margin-left: 15px;
    }

    div#user_box a#signOutButton:HOVER {
        color: #263238;
    }

    div.lab-bar {
        color: #b1b1b1;
        font-family: "Montserrat", sans-serif;
        font-size: 15px;
        z-index: 15;
        position: absolute;
        width: 100%;
        /*padding-left: 20px;*/
        /*top: -2px*/

    }

    div.lab-bar-bg {
        background-color: #263238;
        opacity: 0.5;
        /*transform: skewY(0.3deg);*/
        transform-origin: top left;
        position: absolute;
        top: -20px;
        height: 50px;
        width: 100%;
        z-index: 10;
    }

    div.app-bar {
        height: 110px;
        box-shadow: 3px 1px 7px 0px #42424278;
        padding: 45px 0 0 0px;
    }

    div.app-purple {
        background-color: hsl(262, 58%, 43%);
        /*background-color: #6e2987;*/
    }

    #lab-menu-button {
        position: relative;
        top: 3px;
        margin-right: 10px;
    }

    #ce-logo {
        margin-top: 7px;
        height: 20px;
        opacity: 0.8;
    }

        #ce-logo:hover {
            opacity:1;
        }

</style>

    <div class="lab-bar-bg"> </div>
    <div class="lab-bar"><div class="container">
        <%--<i class="material-icons" id="lab-menu-button">menu</i>--%>
        <%--<img src="images/logo-cefull-white.png" height="20" id="ce-logo">--%>
        <%--CIRCLE<b>LAB</b>--%>

    <%
        String name;
        String message = "";

        User user_h = (User) session.getAttribute("userObject");

        if (user_h == null) {
            // TODO: log error
    %>
    <div id="user_box">
    </div>
    <%
    } else {
        if(user_h.isUnKnown()) {
            message = "Unknown user!";
        }
        name = user_h.getName();
    %>


    <script>
        function signOut() {
            $.ajax({
                url: "<%=response.encodeURL("api/auth/signout/")%>",
                method: 'post',
                data: {}
            })
                .done(
                    function() {
                        window.location.href = "index.jsp";
                    })
                .fail(
                    function(x) {
                        console.log(x.responseText);
                        alert("Could not sign out");
                    });
        }
    </script>

    <div id="user_box" style="float: right; display: inline-block; color: white">
        <a href="<%=response.encodeURL("user.jsp")%>">
            <span id="user_name"><%=name%><br></span>
        </a>
        <h4><%=message%></h4>
        <a href="javascript: signOut();" id="signOutButton">
            <i class="fas fa-sign-out-alt"></i>
            <%--<i class="fa fa-sign-out fa-1x" aria-hidden="true" title="sign out"></i>--%>
        </a>
    </div>
    <%
        }
    %>

</div></div>
<div class="app-bar app-purple">
    <div class="container">
        <a href="<%=response.encodeURL(new AuthManager().getUserHome(request))%>" class="logo">
            <span class="logo-title" style="font-weight: bold;">CIRCLE</span>
            <span class="logo-title">ASSESSMENT</span>
            <%--<img src="images/Logo-Assess.png" height="40">--%>
            <%--<span class="logo-text">Assessment</span>--%>
        </a>
    </div>
</div>



